# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v4.1.0 - 2024-10-18(15:03:47 +0000)

### Other

- [Rlyeh] Refactor to have singular code paths for parsing the library of images
- make default mount options of HostObjects configurable
- ExecutionUnit Resources constraints are not reboot persistent
- initialize gid and uid variables
- [prpl LCM] Execution Unit Resources Constraints values are not persistent after a reboot

## Release v4.0.0 - 2024-10-02(08:29:21 +0000)

### Other

- - [amx] Failing to restart processes with init scripts
- Add support for cgroup v2 controllers
- Implement unprivileged containers
- Add option to keep overlayfs mounted when stopped
- Cthulhu API validation
- Allow unprivileged container without overlayfs
- Allow unprivileged when idmap and acls are not possible with overlayfs
- modify tracezones

## Release v3.9.0 - 2024-07-17(11:15:44 +0000)

### Other

- Create USP Controller instance when a container added
- cthulhu compilation warning

## Release v3.8.0 - 2024-06-19(13:05:29 +0000)

### Other

- [Cthulhu] Use regular expressions for event filters in cthulhu definition odl
- Container private removed

## Release v3.7.12 - 2024-06-18(12:17:17 +0000)

### Other

- Cthulhu network unit tests failing

## Release v3.7.11 - 2024-06-04(16:41:23 +0000)

### Other

- Cthulhu configration not persistent

## Release v3.7.10 - 2024-05-31(15:01:33 +0000)

### Other

- wong error code for disk full
- convert Layers object add_instance to transaction

## Release v3.7.9 - 2024-05-16(14:30:05 +0000)

### Other

- Rework onboarding as plugin

## Release v3.7.8 - 2024-05-06(13:36:57 +0000)

### Other

- do not print password in the logs

## Release v3.7.7 - 2024-05-06(09:49:36 +0000)

### Changes

- Make amxb timeouts configurable

### Other

- Add container execute command capability

## Release v3.7.6 - 2024-03-19(14:06:03 +0000)

### New

- pass stop data to the ctr plugin

### Other

- [TR181-Syslog][LCM] Duplicate syslog configuration of containers leading to syslog-ng not starting

## Release v3.7.5 - 2024-02-13(10:35:32 +0000)

### Other

- Allow lcm containers to log to host dedicated files without a...

## Release v3.7.4 - 2024-02-13(10:25:38 +0000)

### Other

- fix memory leaks

## Release v3.7.3 - 2024-01-15(14:44:14 +0000)

### Other

- stop sandboxes and containers on exit

## Release v3.7.2 - 2023-12-22(16:18:23 +0000)

### Other

- fix tests after previous commit

## Release v3.7.1 - 2023-12-21(15:19:12 +0000)

### Other

- add notification when container state changes

## Release v3.7.0 - 2023-12-15(12:51:40 +0000)

### Other

- Service discovery - Advertisement via DNS.SD

## Release v3.6.0 - 2023-11-16(12:51:42 +0000)

### Other

- remove m4 files

## Release v3.5.2 - 2023-11-08(09:08:24 +0000)

### Other

- update tests

## Release v3.5.1 - 2023-11-06(10:21:28 +0000)

### Other

- - add tests for invalid networkconfig
- Export logs location

## Release v3.5.0 - 2023-10-04(19:31:09 +0000)

### Other

- Implement ModuleVersion parameter

## Release v3.4.2 - 2023-10-04(08:50:06 +0000)

### Other

- Opensource component

## Release v3.4.1 - 2023-09-29(19:27:49 +0000)

### Other

- Cthulhu Device permission

## Release v3.4.0 - 2023-09-25(12:29:48 +0000)

### Other

- make storage location configurable

## Release v3.3.0 - 2023-09-25(08:46:58 +0000)

## Release v3.2.1 - 2023-09-15(11:14:39 +0000)

### Other

- add /dev/tty to allowed devices again

## Release v3.2.0 - 2023-09-08(06:57:29 +0000)

### Other

- define capabilities needed by process

## Release v3.1.3 - 2023-09-07(10:09:07 +0000)

### Other

- Make component available on gitlab.softathome.com
- Use correct SAH license

## Release v3.1.2 - 2023-08-25(09:34:36 +0000)

## Release v3.1.1 - 2023-08-02(15:26:59 +0000)

### Other

- sandboxes were not restored after restart

## Release v3.1.0 - 2023-07-28(07:56:49 +0000)

### Other

- SoftwareModules.ExecutionUnit.{i}.HostObject is missing in datamodel

## Release v3.0.0 - 2023-07-25(14:05:40 +0000)

### Other

- EE ModifyConstraints not aligned with standard

## Release v2.3.3 - 2023-07-25(13:06:17 +0000)

### Other

- [LCM]Container installation failure - "locale" settings not set

## Release v2.3.2 - 2023-07-25(12:49:10 +0000)

## Release v2.3.1 - 2023-07-25(10:38:29 +0000)

### Other

- Load config earlier so plugins can load

## Release v2.3.0 - 2023-07-14(15:00:38 +0000)

### Other

- Creating an active nested EE is not possible

## Release v2.2.0 - 2023-07-12(14:51:48 +0000)

### Other

- LCM containers onboarding solution - runtime

## Release v2.1.2 - 2023-07-12(13:57:47 +0000)

### Other

- HostObjects

## Release v2.1.1 - 2023-06-29(09:52:41 +0000)

### Other

- Modify NetworkConfig

## Release v2.1.0 - 2023-06-16(12:49:13 +0000)

### Other

- NetworkConfig of containers

## Release v2.0.0 - 2023-06-02(11:07:53 +0000)

### Other

- DU/EU Unique Identifier

## Release v1.6.0 - 2023-05-26(08:27:45 +0000)

### Other

- remove deprecated dhcp settings

## Release v1.5.1 - 2023-05-17(11:55:12 +0000)

## Release v1.5.0 - 2023-05-17(06:51:32 +0000)

### Other

- enable port forwarding for containers

## Release v1.4.0 - 2023-05-10(07:44:29 +0000)

### Other

- Fill in interfaces and IP addresses in the DM

## Release v1.3.1 - 2023-05-02(09:06:59 +0000)

### Other

- HostObject support for devices sharing

## Release v1.3.0 - 2023-04-28(09:11:41 +0000)

### Other

- HostObject support for devices sharing

## Release v1.2.1 - 2023-04-07(12:59:35 +0000)

### Other

- revert overlayfs order

## Release v1.2.0 - 2023-04-03(08:27:47 +0000)

### Other

- install containers in parallel
- Implement HostObjects

## Release v1.1.9 - 2023-03-24(11:31:46 +0000)

### Other

- Fix odls

## Release v1.1.8 - 2023-03-24(09:12:56 +0000)

### Other

- Update documentation

## Release v1.1.7 - 2023-03-15(16:16:32 +0000)

### Other

- change location of syslogng config files

## Release v1.1.6 - 2023-03-14(20:16:55 +0000)

### Other

- Crash possible when ctr object is deleted but notif still has a pointer to it
- [LCM][Shutdown] timingila and cthulhu stops script are called...

## Release v1.1.5 - 2023-03-10(15:09:55 +0000)

## Release v1.1.4 - 2023-03-10(14:51:26 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v1.1.3 - 2023-03-01(09:27:58 +0000)

### Other

- [LCM] Expose time for firstInstall and lastupdate

## Release v1.1.2 - 2023-02-22(15:41:55 +0000)

### Other

- do not send remove notice when updating a container

## Release v1.1.1 - 2023-02-20(11:08:57 +0000)

### Other

- add f2fscl and resize2fs as runtime dependencies

## Release v1.1.0 - 2023-02-20(10:43:10 +0000)

### Other

- allow to increase the allocated diskspace of a sandbox

## Release v1.0.0 - 2023-02-14(13:41:38 +0000)

### Other

- move storage to /lcm

## Release v0.21.2 - 2023-02-09(15:44:35 +0000)

### Other

- cthulhu_config.odl was not generated correctly

## Release v0.21.1 - 2023-02-09(12:19:12 +0000)

### Other

- stopping state of container was missing

## Release v0.21.0 - 2023-02-07(14:05:33 +0000)

### Other

- [Timingila-rlyeh] Missing information on reboot

## Release v0.20.1 - 2023-02-01(14:04:44 +0000)

### Other

- Also apply ctr resource limitations on the private sandbox to prevent race conditions

## Release v0.20.0 - 2023-01-10(10:21:31 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.19.11 - 2022-12-23(13:24:49 +0000)

### Other

- Improve plugin api

## Release v0.19.10 - 2022-12-20(17:19:29 +0000)

### Other

- mount /run and /tmp as tmpfs in the container by default

## Release v0.19.9 - 2022-12-08(15:32:33 +0000)

### Other

- Let udhcpc.script work on prplos

## Release v0.19.8 - 2022-12-07(16:01:18 +0000)

## Release v0.19.7 - 2022-12-06(15:46:26 +0000)

### Other

- Do not let uhdcpc overwrite /etc/resolv.conf file

## Release v0.19.6 - 2022-12-05(16:09:24 +0000)

### Other

- Create nested sandboxes in the correct namespace

## Release v0.19.5 - 2022-11-29(16:51:34 +0000)

### Other

- create nested sandboxes in the correct namespaces

## Release v0.19.4 - 2022-11-23(12:57:14 +0000)

### Other

- Cthulhu should send error messages when it fails

## Release v0.19.3 - 2022-11-03(10:10:18 +0000)

### Other

- Add missing dependencies kmod-veth and kmod-fs-ext4

## Release v0.19.2 - 2022-10-24(12:28:54 +0000)

### Fixes

- Enable coredumps

### Other

- allow for several subdirs on repo

## Release v0.19.1 - 2022-09-21(13:22:59 +0000)

## Release v0.19.0 - 2022-08-29(13:25:31 +0000)

### New

- Add plugin system to cthulhu

## Release v0.18.2 - 2022-07-18(12:28:37 +0000)

### Other

- if no network NS is defined, then the parent NS should be inheritted

## Release v0.18.1 - 2022-07-01(18:42:32 +0000)

### Other

- Parse autostart annotations

## Release v0.18.0 - 2022-06-28(12:58:33 +0000)

### New

- allow create and update to override resource limitations

## Release v0.17.3 - 2022-06-24(12:51:45 +0000)

### Other

- Remove unused ParentSandbox network type

## Release v0.17.2 - 2022-06-03(15:54:20 +0000)

### Other

- let syslogng in a container work when restarting the container

## Release v0.17.1 - 2022-05-25(15:56:55 +0000)

### Other

- - Parse resource restrictions from OCI image file

## Release v0.17.0 - 2022-05-23(15:29:54 +0000)

### New

- Parse image Mount annotations

## Release v0.16.0 - 2022-05-19(09:10:36 +0000)

### New

- enable mounting of files and dirs

### Other

- enable mounting of files and dirs

## Release v0.15.0 - 2022-05-17(11:49:55 +0000)

### New

- Issue LCM-136 : Execution Environments: Add

## Release v0.14.2 - 2022-05-12(09:17:10 +0000)

### Other

- Enable generic EE by default
- when creating a container, the command id was not set on the correct notification

## Release v0.14.1 - 2022-05-10(14:51:16 +0000)

### Other

- Logs: Machine Parsable. Execution Unit

## Release v0.14.0 - 2022-05-02(14:51:37 +0000)

### New

- Issue : LCM-54 Logs: Machine Parsable. Execution Unit
- Execution Environments: Modify. Description

## Release v0.13.8 - 2022-04-22(13:04:49 +0000)

### Fixes

- [Cthulhu] Add host deps to recipe

## Release v0.13.7 - 2022-04-21(10:11:30 +0000)

### Fixes

- [Cthulhu] Remove container listing/update from init

## Release v0.13.6 - 2022-04-13(20:33:46 +0000)

### Other

- [Cthulhu] Container created sandbox does not inherit network namespace properly

## Release v0.13.5 - 2022-04-12(14:03:41 +0000)

### Other

- [Cthulhu] Cannot set dm value for AllocatedMemory and AllocatedCPUPercent

## Release v0.13.4 - 2022-04-09(11:57:14 +0000)

### Fixes

- ad CI tests

## Release v0.13.3 - 2022-03-29(07:40:45 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

### Other

- - Add resource management to containers

## Release v0.13.2 - 2022-03-10(02:01:21 +0000)

### Fixes

- [Cthulhu] Use %jd with casting to (intmax_t) for off_t printf format

## Release v0.13.1 - 2022-03-08(10:19:26 +0000)

### Fixes

- print int64 with PRId64

## Release v0.13.0 - 2022-03-07(16:20:25 +0000)

### New

- Issue : LCM-552 Command to modify resource constraints of an Execution Environment

### Fixes

- Issue: print uint64_t platform independent

## Release v0.12.3 - 2022-03-02(17:01:31 +0000)

### Other

- Rework CommandId

## Release v0.12.2 - 2022-03-01(12:53:21 +0000)

### Fixes

- Do not try to restart a container if its sandbox is not up

## Release v0.12.1 - 2022-02-28(18:13:55 +0000)

### Other

- CPU Cgroups limit are not correct in case of multiple CPUs

## Release v0.12.0 - 2022-02-23(18:15:40 +0000)

### New

- Add Created timestamp to Sandbox

## Release v0.11.2 - 2022-02-22(16:22:11 +0000)

### Fixes

- parameter mismatch in default settings

### Other

- - cthulhu:ctr:created event does not contain CommandId if sandbox is disabled

## Release v0.11.1 - 2022-02-22(08:26:39 +0000)

### Fixes

- - actually allocate reserved disk space on disk
- rename AvailableDiskspace to AvailableDiskSpace

## Release v0.11.0 - 2022-02-18(09:22:13 +0000)

### New

- [LCM] Create user roles

## Release v0.10.0 - 2022-02-17(16:57:41 +0000)

### New

- - Introduce a flag to retain or not the data partition when update container

## Release v0.9.2 - 2022-02-17(14:12:57 +0000)

### Fixes

- - memory limitations did not work on some targets

## Release v0.9.1 - 2022-02-16(19:49:26 +0000)

### Other

- retrieve the available mem, diskspace of an EE (sandbox)

## Release v0.9.0 - 2022-02-10(20:43:56 +0000)

### New

- User overlayfs for containers

### Other

- Execution Environments: Restart
- ExecutionUnit: Restart

## Release v0.8.6 - 2022-02-07(10:29:19 +0000)

### Other

- [LCM] Constrain the EE (sandbox) with mem, cpu, diskspace

## Release v0.8.5 - 2022-01-26(16:22:14 +0000)

### Other

- Execution Environments: Restart
- ExecutionUnit: Restart

## Release v0.8.4 - 2022-01-19(17:13:40 +0000)

### Fixes

- add Sandbox.exec to test executing code in namespace

## Release v0.8.3 - 2022-01-12(13:44:03 +0000)

### Other

- - better manage private data of ctr object

## Release v0.8.2 - 2022-01-11(09:54:06 +0000)

### Fixes

- Correct dependencies in baf file

## Release v0.8.1 - 2022-01-10(15:03:04 +0000)

### Fixes

- Removing a failing container that has autorestart enabled can crash cthulhu

### Other

- Removing a failing container that has autorestart enabled can crash cthulhu

## Release v0.8.0 - 2021-12-23(16:33:24 +0000)

### New

- add tr181 error messages

## Release v0.7.0 - 2021-12-13(15:47:09 +0000)

### New

- add descriptor and vendor to DM

## Release v0.6.0 - 2021-12-13(08:37:48 +0000)

### New

- -  add Enable and restart() to Sandboxes

### Fixes

- IOT-000:  update  sandbox events

## Release v0.5.2 - 2021-12-03(02:21:00 +0000)

## Release v0.5.1 - 2021-11-29(08:36:17 +0000)

### Other

- - support kernels without a CFS scheduler

## Release v0.5.0 - 2021-11-26(14:10:25 +0000)

### New

- Implement updating of a container

## Release v0.4.2 - 2021-11-23(20:56:14 +0000)

### Fixes

- Fix for cgroups

## Release v0.4.1 - 2021-11-19(14:03:15 +0000)

### Fixes

- Fix warnings on some compilers

## Release v0.4.0 - 2021-11-19(08:52:47 +0000)

### New

- Create hierarchical sandboxes

### Other

- Create hierarchical sandboxes

## Release v0.3.8 - 2021-11-18(22:44:12 +0000)

### Other

- Create hierarchical sandboxes

## Release v0.3.7 - 2021-11-12(11:51:07 +0000)

## Release v0.3.6 - 2021-11-10(15:26:03 +0000)

### Fixes

- Process return value of sethostname

## Release v0.3.5 - 2021-10-28(09:39:41 +0000)

### Other

- Implement Network NS support

## Release v0.3.4 - 2021-10-19(12:26:16 +0000)

### Other

- support UUID that start with a number

## Release v0.3.3 - 2021-10-15(13:17:15 +0000)

### Fixes

- Fix auto load preprocessor

## Release v0.3.2 - 2021-10-14(09:58:41 +0000)

## Release v0.3.1 - 2021-10-08(14:53:24 +0000)

## Release v0.3.0 - 2021-10-06(18:46:07 +0000)

### New

- support removing of containers

## Release v0.2.1 - 2021-09-22(14:30:11 +0000)

### Fixes

- Auto init backend and make dirs on script startup

## Release v0.2.0 - 2021-09-15(12:59:01 +0000)

### New

- Cgroups support for cthulhu

## Release v0.1.2 - 2021-09-10(09:39:29 +0000)

### Fixes

- save persistent data

### Other

- Enable unit-tests
- Enbale auto opensourcing

## Release v0.1.1 - 2021-06-29(08:33:31 +0000)

### Fixes

- fix compile errors with g++

### Other

- Correct changelog
- Enable g++ Ci job

## Release v0.1.0 - 2020-12-18(10:41:44 +0100)

### New

- Initial release
