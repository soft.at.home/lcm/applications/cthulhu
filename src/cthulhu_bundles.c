/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "cthulhu_priv.h"
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_bundles.h"

#define ME "bundles"

static bool using_bundles = false;

/*
 * Checks whether bundles should be used instead of images
 */
void cthulhu_bundles_init(void) {
    // check the config flag
    amxd_object_t* config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(config) {
        bool use_bundles_config = amxd_object_get_bool(config, CTHULHU_CONFIG_USEBUNDLES, NULL);
        if(!use_bundles_config) {
            SAH_TRACEZ_NOTICE(ME, "Using bundles is disabled in the config");
            return;
        }

        // check if bundles are supported
        amxd_object_t* info = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_INFO);
        if(!info) {
            SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_INFO);
            return;
        }

        bool bundles_supported = amxd_object_get_bool(info, CTHULHU_INFO_BUNDLES_SUPPORTED, NULL);
        if(!bundles_supported) {
            SAH_TRACEZ_NOTICE(ME, "Bundles not supported by the backend");
        }

        using_bundles = use_bundles_config && bundles_supported;
    }
}

bool cthulhu_use_bundles(void) {
    return using_bundles;
}
