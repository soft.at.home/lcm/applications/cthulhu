/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sched.h>
#include <sys/wait.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <linux/if.h>
#include <netlink/netlink.h>
#include <netlink/route/link.h>
#include <netlink/route/link/veth.h>
#include <netlink/route/link/bridge.h>
#include <sys/mman.h>
#include <yajl/yajl_gen.h>
#include <amxc/amxc_variant_type.h>
#include <amxj/amxj_variant.h>
#include <lcm/lcm_serialize.h>

#include "cthulhu_priv.h"

#define ME "ns"

#define CLONE_STACK_SIZE 1048576 // 1024*1024
static amxc_htable_t* ns_proc_data_table = NULL;
static amxc_htable_t* ns_socket_table = NULL;

// store this in a static var so it is also available in the cloned process
static char* sb_host_data_location = NULL;

struct cthulhu_ns_data;
static int cthulhu_link_up(struct nl_sock* sk, struct rtnl_link* link);
static int cthulhu_link_name_up(const char* ifname);
static int cthulhu_ns_create_priv(amxc_var_t* ns_data);
static void cthulhu_ns_destroy_priv(const char* ns_name);
static void ns_proc_data_it_free_in_ns(UNUSED const char* key, amxc_htable_it_t* it);

/**
 * struct to share data with the process that will run in the
 * new namespaces
 *
 */
typedef struct _cthulhu_ns_proc_data {
    amxc_htable_it_t hit;
    int ns_pid;
    char* clone_stack;
    amxc_llist_t veth_interfaces;
} cthulhu_ns_proc_data_t;

typedef struct _cthulhu_ns_clone_data {
    char* sb_name;
} cthulhu_ns_clone_data_t;


typedef struct _cthulhu_ns_socket {
    amxc_htable_it_t hit;
    int fd;
} cthulhu_ns_socket_t;

typedef enum _msg_res
{
    msg_res_ok = 0,
    msg_res_no_more_data,
    msg_res_error,
    msg_res_client_closed,
    msg_res_req_exit
} msg_res_t;

/**
 * Messages to send to the namespace proc
 */
typedef enum _cthulhu_msg_type
{
    cthulhu_msg_none = 0,
    cthulhu_msg_exit,
    cthulhu_msg_ping,
    cthulhu_msg_set_hostname,
    cthulhu_msg_itfup,
    cthulhu_msg_cmd,
    cthulhu_msg_create_ns,
    cthulhu_msg_destroy_ns,
    cthulhu_msg_type_last
} cthulhu_msg_type_t;

static const char* cthulhu_ctr_status_names[] = {
    "None",
    "Exit",
    "Ping",
    "SetHostname",
    "InterfaceUp",
    "Cmd",
    "CreateNamespace",
    "DestroyNamespace"
};

static const char* cthulhu_msg_type_to_string(cthulhu_msg_type_t msg_type) {
    if((msg_type >= 0) && (msg_type < cthulhu_msg_type_last)) {
        return cthulhu_ctr_status_names[msg_type];
    } else {
        return NULL;
    }
}

/**
 * struct to send messages from the host to the newly created
 * namespace
 *
 */
typedef struct _cthulhu_msg {
    cthulhu_msg_type_t msg_type;
    amxc_var_t* data;
} cthulhu_msg_t;

/**
 * @brief Initialize a message.
 * This should be cleaned with @ref cthulhu_msg_clean
 *
 * @param msg
 * @param msg_type
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_msg_init(cthulhu_msg_t* msg, cthulhu_msg_type_t msg_type) {
    msg->msg_type = msg_type;
    return amxc_var_new(&msg->data);
}

/**
 * @brief Clean a message
 *
 * @param msg
 */
static void cthulhu_msg_clean(cthulhu_msg_t* msg) {
    amxc_var_delete(&msg->data);
}

char* cthulhu_get_sb_host_data(const char* ns_name) {
    char* sb_host_data = NULL;
    if(!sb_host_data_location) {
        SAH_TRACEZ_ERROR(ME, "Host data dir not found");
        goto exit;
    }
    if(asprintf(&sb_host_data, "%s/%s", sb_host_data_location, ns_name) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string");
        goto exit;
    }
    if(!cthulhu_isdir(sb_host_data)) {
        if(cthulhu_mkdir(sb_host_data, false) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to create dir [%s]", sb_host_data);
            free(sb_host_data);
            sb_host_data = NULL;
            goto exit;
        }
    }
exit:
    return sb_host_data;
}

static char* cthulhu_get_socket_name(const char* ns_name) {
    char* socket_name = NULL;

    char* sb_host_data = cthulhu_get_sb_host_data(ns_name);
    ASSERT_NOT_NULL(sb_host_data, goto exit);

    if(asprintf(&socket_name, "%s/namespace.sock", sb_host_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string");
        goto exit;
    }
exit:
    free(sb_host_data);
    return socket_name;
}

static int cthulhu_create_socket_to_namespace(const char* ns_name, bool suppress_error) {
    int fd = -1;
    struct sockaddr_un serveraddr;

    char* socket_name = cthulhu_get_socket_name(ns_name);
    ASSERT_NOT_NULL(socket_name, goto exit);

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd == -1) {
        if(!suppress_error) {
            SAH_TRACEZ_ERROR(ME, "Could not create socket (%d: %s)", errno, strerror(errno));
        }
        goto exit;
    }
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sun_family = AF_UNIX;
    strcpy(serveraddr.sun_path, socket_name);

    if(connect(fd, (struct sockaddr*) &serveraddr, sizeof(serveraddr)) < 0) {
        if(!suppress_error) {
            SAH_TRACEZ_ERROR(ME, "Could not connect to socket [%s] (%d: %s)", socket_name, errno, strerror(errno));
        }
        close(fd);
        fd = -1;
        goto exit;
    }
exit:
    free(socket_name);
    return fd;
}

static int cthulhu_get_socket_to_namespace(const char* ns_name, bool suppress_error) {
    int fd = -1;
    cthulhu_ns_socket_t* ns_socket = NULL;
    amxc_htable_it_t* it = NULL;

    it = amxc_htable_get(ns_socket_table, ns_name);
    if(it) {
        ns_socket = amxc_htable_it_get_data(it, cthulhu_ns_socket_t, hit);
        fd = ns_socket->fd;
        goto exit;
    }

    fd = cthulhu_create_socket_to_namespace(ns_name, suppress_error);
    if(fd == -1) {
        if(!suppress_error) {
            SAH_TRACEZ_ERROR(ME, "Could not create socket to namespace [%s]", ns_name);
        }
        goto exit;
    }
    ns_socket = (cthulhu_ns_socket_t*) calloc(1, sizeof(cthulhu_ns_socket_t));
    ns_socket->fd = fd;
    if(amxc_htable_insert(ns_socket_table, ns_name, &ns_socket->hit)) {
        SAH_TRACEZ_ERROR(ME, "Could not add socket to hashtable");
    }

exit:
    return fd;
}



static void ns_socket_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    cthulhu_ns_socket_t* ns_socket = amxc_htable_it_get_data(it, cthulhu_ns_socket_t, hit);
    close(ns_socket->fd);
    free(ns_socket);
}

static void cthulhu_close_socket(const char* ns_name) {
    amxc_htable_it_t* it = amxc_htable_get(ns_socket_table, ns_name);
    if(it) {
        amxc_htable_it_clean(it, ns_socket_it_free);
    }
}

/**
 * Send a message from the host to the namespace process
 *
 * @param pipe the fd of the pipe
 * @param msg message to send
 *
 * @return int reply code, or -1 when failed
 */
static int cthulhu_send_msg(const char* ns_name, cthulhu_msg_t* msg) {
    int res = -1;
    bool suppress_error = false;
    int fd = -1;
    ASSERT_NOT_NULL(msg, goto exit);
    ASSERT_NOT_NULL(ns_name, goto exit);
    SAH_TRACEZ_INFO(ME, "Send msg [%d: %s]", msg->msg_type, cthulhu_msg_type_to_string(msg->msg_type));
    suppress_error = (msg->msg_type == cthulhu_msg_ping);

    fd = cthulhu_get_socket_to_namespace(ns_name, suppress_error);
    if(fd == -1) {
        if(!suppress_error) {
            SAH_TRACEZ_ERROR(ME, "Could not get socket to namespace [%s]", ns_name);
        }
        goto exit;
    }

    if(send(fd, &msg->msg_type, sizeof(cthulhu_msg_type_t), 0) < 0) {
        SAH_TRACEZ_ERROR(ME, "Write error (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    if(lcm_var_serialize(fd, msg->data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not send var (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    if(recv(fd, &res, sizeof(int), 0) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to read the reply (%d: %s)", errno, strerror(errno));
        res = -1;
    }
    SAH_TRACEZ_INFO(ME, "Send msg res: %d", res);
exit:
    return res;
}

/**
 * Receive a message in the namespace process
 *
 * @param fd the fd of the socket
 * @param msg message to fill
 *
 * @return msg_res_t result of the message received
 */
static msg_res_t cthulhu_recv_msg(int fd, cthulhu_msg_t* msg) {
    msg_res_t msg_res = msg_res_ok;
    int res = 0;

    res = recv(fd, &msg->msg_type, sizeof(cthulhu_msg_type_t), 0);
    if(res < 0) {
        if(errno == EWOULDBLOCK) {
            msg_res = msg_res_no_more_data;
            goto exit;
        }
        SAH_TRACEZ_ERROR(ME, "Read error (%d: %s)", errno, strerror(errno));
        msg_res = msg_res_error;
        goto exit;
    }
    if(res == 0) {
        msg_res = msg_res_client_closed;
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Rcv msg [%d: %s]", msg->msg_type, cthulhu_msg_type_to_string(msg->msg_type));
    res = lcm_var_deserialize(fd, msg->data);
    if(res == 0) {
        msg_res = msg_res_client_closed;
    } else if(res < 0) {
        msg_res = msg_res_error;
    }
exit:
    return msg_res;
}

/**
 * @brief Send a reply over a socket
 *
 * @param fd
 * @param reply
 */
static void cthulhu_send_reply(int fd, int reply) {
    if(send(fd, &reply, sizeof(int), 0) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not write reply (%d: %s)", errno, strerror(errno));
    }
}

/**
 * Delete the data
 *
 * @param ns_clone_data_p
 */
static void cthulhu_ns_clone_data_delete(cthulhu_ns_clone_data_t** ns_clone_data_p) {
    if(!ns_clone_data_p) {
        return;
    }
    cthulhu_ns_clone_data_t* ns_clone_data = *ns_clone_data_p;

    if(!ns_clone_data) {
        goto free_ns_clone_data;
    }

    free(ns_clone_data->sb_name);

free_ns_clone_data:
    free(ns_clone_data);
    ns_clone_data_p = NULL;
}

/**
 * @brief Create namespace clone data that will be shared with the cloned process
 *
 * @param ns_clone_data_p
 * @return cthulhu_ns_clone_data_t*
 */
static cthulhu_ns_clone_data_t* cthulhu_ns_clone_data_create(void) {
    cthulhu_ns_clone_data_t* ns_clone_data = (cthulhu_ns_clone_data_t*) calloc(1, sizeof(cthulhu_ns_clone_data_t));
    return ns_clone_data;
}

static msg_res_t namespace_proc_rcv_msg(int client_fd) {
    msg_res_t msg_res = msg_res_ok;
    int res = 0;
    cthulhu_msg_t msg;
    cthulhu_msg_init(&msg, cthulhu_msg_none);
    msg_res = cthulhu_recv_msg(client_fd, &msg);
    if(msg_res != msg_res_ok) {
        SAH_TRACEZ_ERROR(ME, "Error receiving message [%d]", msg_res);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Msg received in namespace [%d: %s]", msg.msg_type, cthulhu_msg_type_to_string(msg.msg_type));
    switch(msg.msg_type) {
    case cthulhu_msg_exit:
        cthulhu_send_reply(client_fd, 0);
        msg_res = msg_res_req_exit;
        break;
    case cthulhu_msg_ping:
        cthulhu_send_reply(client_fd, 0);
        break;
    case cthulhu_msg_set_hostname:
    {
        const char* hostname = GET_CHAR(msg.data, NULL);
        res = sethostname(hostname, strlen(hostname));
        cthulhu_send_reply(client_fd, res);
    }
    break;
    case cthulhu_msg_itfup:
        res = cthulhu_link_name_up(GET_CHAR(msg.data, NULL));
        cthulhu_send_reply(client_fd, res);
        break;
    case cthulhu_msg_cmd:
    {
        const char* cmd = GET_CHAR(msg.data, NULL);
        int exit_status = -1;

        if((res = cthulhu_proc_wait(cmd, &exit_status)) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute %s", cmd);
            cthulhu_send_reply(client_fd, res);
        } else {
            SAH_TRACEZ_INFO(ME, "Cmd: %s", cmd);
            SAH_TRACEZ_INFO(ME, "exit status: %d", exit_status);

            cthulhu_send_reply(client_fd, exit_status);
        }
    }
    break;
    case cthulhu_msg_create_ns:
        res = cthulhu_ns_create_priv(msg.data);
        cthulhu_send_reply(client_fd, res);
        break;
    case cthulhu_msg_destroy_ns:
        cthulhu_ns_destroy_priv(GET_CHAR(msg.data, NULL));
        cthulhu_send_reply(client_fd, 0);
        break;

    default:
        SAH_TRACEZ_ERROR(ME, "Unknown message type %d", msg.msg_type);
    }

exit:
    cthulhu_msg_clean(&msg);
    return msg_res;
}

/**
 * This process will run in the cloned namespaces
 *
 */
static int namespace_proc(void* arg) {
    cthulhu_ns_clone_data_t* ns_clone_data = (cthulhu_ns_clone_data_t*) arg;
    int listen_fd = 0;
    struct sockaddr_un serveraddr;
    bool exit_requested = false;
    char* socket_name = NULL;
    struct pollfd fds[200];
    int res = 0;
    int nfds = 0;
    int current_size = 0;
    int i = 0;
    bool compress_array = false;
    // ignore interrupt in child, the parent will close this  when needed
    signal(SIGINT, SIG_IGN);
    signal(SIGTERM, SIG_IGN);
    signal(SIGABRT, SIG_IGN);

    // delete the htables that are inheritted from the parent
    amxc_htable_delete(&ns_proc_data_table, ns_proc_data_it_free_in_ns);
    amxc_htable_new(&ns_proc_data_table, 10);
    amxc_htable_delete(&ns_socket_table, ns_socket_it_free);
    amxc_htable_new(&ns_socket_table, 10);

    if(!ns_clone_data || string_is_empty(ns_clone_data->sb_name)) {
        SAH_TRACEZ_ERROR(ME, "ns clone data is not complete");
        return -1;
    }

    // set the env variables
    if(ns_clone_data->sb_name) {
        setenv("SB_NAME", ns_clone_data->sb_name, true);
    }
    char* sb_host_data = cthulhu_get_sb_host_data(ns_clone_data->sb_name);
    if(sb_host_data) {
        setenv("SB_HOST_DATA", sb_host_data, true);
        free(sb_host_data);
    }

    socket_name = cthulhu_get_socket_name(ns_clone_data->sb_name);
    if(!socket_name) {
        SAH_TRACEZ_ERROR(ME, "Could not create socket name");
        goto exit;
    }

    // create the listen socket
    listen_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(listen_fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create socket (%d: %s)", errno, strerror(errno));
        goto exit;
    }
    // set non-blocking for poll to work
    int flags = fcntl(listen_fd, F_GETFL, 0);
    fcntl(listen_fd, F_SETFL, flags | O_NONBLOCK);

    unlink(socket_name);
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sun_family = AF_UNIX;
    strcpy(serveraddr.sun_path, socket_name);
    if(bind(listen_fd, (struct sockaddr*) &serveraddr, SUN_LEN(&serveraddr)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not bind to socket [%s] (%d: %s)", socket_name, errno, strerror(errno));
        close(listen_fd);
        goto exit;
    }

    if(listen(listen_fd, 10) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not listen to socket [%s] (%d: %s)", socket_name, errno, strerror(errno));
        close(listen_fd);
        goto exit;
    }

    // initialize the master fd_set
    memset(fds, 0, sizeof(fds));
    fds[0].fd = listen_fd;
    fds[0].events = POLLIN;
    nfds = 1;
    // wait for events
    while(!exit_requested) {
        res = poll(fds, nfds, -1);

        if(res < 0) {
            SAH_TRACEZ_ERROR(ME, "Poll failed (%d: %s)", errno, strerror(errno));
            goto exit_close_socks;
        }

        current_size = nfds;
        for(i = 0; i < current_size; i++) {
            if(fds[i].revents == 0) {
                continue;
            }

            if(fds[i].fd == listen_fd) {
                if(fds[i].revents & POLLHUP) {
                    SAH_TRACEZ_ERROR(ME, "Other side (client) hung up on listen fd [%d:%d]", i, fds[i].fd);
                    exit_requested = true;
                } else if(fds[i].revents != POLLIN) {
                    SAH_TRACEZ_ERROR(ME, "Only POLLIN events expected on listen fd [%d], but revents = %d", fds[i].fd, fds[i].revents);
                    exit_requested = true;
                    break;
                }

                int new_fd = 0;
                do {
                    new_fd = accept(listen_fd, NULL, NULL);
                    if(new_fd < 0) {
                        if(errno != EWOULDBLOCK) {
                            SAH_TRACEZ_ERROR(ME, "Accept failed(%d: %s)", errno, strerror(errno));
                            exit_requested = true;
                        }
                        break;
                    }
                    SAH_TRACEZ_INFO(ME, "New incoming connection %d", new_fd);
                    fds[nfds].fd = new_fd;
                    fds[nfds].events = POLLIN;
                    nfds++;

                } while(new_fd != -1);
            } else {
                bool client_closed = false;
                if(fds[i].revents & POLLHUP) {
                    SAH_TRACEZ_ERROR(ME, "Other side (host!) hung up on fd [%d:%d]", i, fds[i].fd);
                    client_closed = true;
                } else if(fds[i].revents != POLLIN) {
                    SAH_TRACEZ_ERROR(ME, "Only POLLIN events expected on fd [%d:%d], but revents = %d. Closing client.", i, fds[i].fd, fds[i].revents);
                    client_closed = true;
                } else {
                    //data is available on a connection
                    msg_res_t msg_res = msg_res_ok;
                    msg_res = namespace_proc_rcv_msg(fds[i].fd);
                    if(msg_res == msg_res_req_exit) {
                        exit_requested = true;
                    } else if(msg_res == msg_res_client_closed) {
                        client_closed = true;
                    }
                }

                if(client_closed) {
                    close(fds[i].fd);
                    fds[i].fd = -1;
                    compress_array = true;
                }
            }
        } // loop over fds

        if(compress_array) {
            compress_array = false;
            for(i = 0; i < nfds; i++) {
                if(fds[i].fd == -1) {
                    for(int j = i; j < nfds - 1; j++) {
                        fds[j].fd = fds[j + 1].fd;
                    }
                    i--;
                    nfds--;
                }
            }
        }
    } // namespace loop

exit_close_socks:
    // close all open sockets
    for(i = 0; i < nfds; i++) {
        if(fds[i].fd >= 0) {
            close(fds[i].fd);
        }
    }

exit:
    SAH_TRACEZ_INFO(ME, "Exit namespace");
    if(socket_name) {
        unlink(socket_name);
    }
    free(socket_name);
    cthulhu_ns_clone_data_delete(&ns_clone_data);
    amxc_htable_delete(&ns_proc_data_table, NULL);
    amxc_htable_delete(&ns_socket_table, ns_socket_it_free);
    return 0;
}

/**
 * Set the link to UP
 *
 * @param sk netlink socket
 * @param link the link to enable
 *
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_link_up(struct nl_sock* sk, struct rtnl_link* link) {
    struct rtnl_link* change = NULL;
    int res = -1;
    if(!sk) {
        SAH_TRACEZ_ERROR(ME, "Socket is NULL");
        goto exit;
    }
    if(!link) {
        SAH_TRACEZ_ERROR(ME, "Link is NULL");
        goto exit;
    }
    change = rtnl_link_alloc();
    if(!link) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate Change");
        goto exit;
    }

    rtnl_link_set_flags(change, IFF_UP);
    SAH_TRACEZ_INFO(ME, "Set link [%s] up", rtnl_link_get_name(link));

    res = rtnl_link_change(sk, link, change, 0);
    if(res) {
        SAH_TRACEZ_ERROR(ME, "Link [%s] up res: %s", rtnl_link_get_name(link), nl_geterror(res));
    }
exit:
    rtnl_link_put(change);
    return res;
}

/**
 * Set the link to UP
 *
 * @param ifname name of the link
 *
 * @return int 0 on success, -1 on failure
 */
static int cthulhu_link_name_up(const char* ifname) {
    int res = -1;
    struct nl_sock* sk = NULL;
    struct rtnl_link* link = NULL;
    sk = nl_socket_alloc();
    if(!sk) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate socket for netlink");
        goto exit;
    }
    res = nl_connect(sk, NETLINK_ROUTE);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not connect socket for netlink");
        goto exit;
    }
    if(rtnl_link_get_kernel(sk, 0, ifname, &link) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not find link [%s]", ifname);
        goto exit;
    }
    cthulhu_link_up(sk, link);
exit:
    rtnl_link_put(link);
    nl_socket_free(sk);
    return res;
}

/**
 * Create a veth pair, where one veth is on the host, and the
 * other is moved to another network namespace
 *
 * @param ns name of the namespace to move to
 * @param ifname name of the veth interface in the new namespace
 * @param bridge if defined, the veth on the host will be added
 *               to this bridge
 * @param ns_pid the pid of the network namespace to move the
 *               interface to
 * @param comm fd to message the process in the namespace
 * @param sb_data priveate data of the sandbox
 *
 * @return int
 */
static int cthulhu_create_veth(const char* ns_name, const char* ifname, const char* bridge,
                               cthulhu_ns_proc_data_t* ns_data) {
    int res = -1;
    char host_ifname[16];
    uint8_t index = 0;
    struct nl_sock* sk = NULL;
    struct nl_cache* link_cache = NULL;
    struct rtnl_link* link = NULL;
    struct rtnl_link* nl_bridge = NULL;
    cthulhu_msg_t msg;

    cthulhu_msg_init(&msg, cthulhu_msg_itfup);

    sk = nl_socket_alloc();
    if(!sk) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate socket for netlink");
        goto exit;
    }
    res = nl_connect(sk, NETLINK_ROUTE);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not connect socket for netlink");
        goto exit;
    }
    res = rtnl_link_alloc_cache(sk, AF_UNSPEC, &link_cache);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not fetch netlink cache");
        goto exit;
    }

    host_ifname [15] = '\0';
    do {
        snprintf(host_ifname, 15, "veth_%.4s_%.3d", ns_name, index);
        link = rtnl_link_get_by_name(link_cache, host_ifname);
        if(!link) {
            break;
        }
    } while(index++ < 100);
    if(link) {
        SAH_TRACEZ_ERROR(ME, "Could not find a name for the host interface");
        res = -1;
        goto exit;
    }
    res = rtnl_link_veth_add(sk, host_ifname, ifname, ns_data->ns_pid);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create veth. hostif: [%s], ctr_if: [%s], ns_pid: [%d]",
                         host_ifname, ifname, ns_data->ns_pid);
        goto exit;
    }
    llist_append_string(&ns_data->veth_interfaces, host_ifname);
    nl_cache_refill(sk, link_cache);
    link = rtnl_link_get_by_name(link_cache, host_ifname);
    if(!link) {
        SAH_TRACEZ_ERROR(ME, "Cannot find newly created veth [%s]", host_ifname);
        goto exit;
    }

    // add link to bridge
    if(!string_is_empty(bridge)) {
        nl_bridge = rtnl_link_get_by_name(link_cache, bridge);
        if(!nl_bridge) {
            SAH_TRACEZ_WARNING(ME, "Bridge [%s] does not exist", bridge);
            goto exit;
        }
        if(!rtnl_link_is_bridge(nl_bridge)) {
            SAH_TRACEZ_WARNING(ME, "[%s] is not a bridge", bridge);
            goto exit;
        }
        res = rtnl_link_enslave(sk, nl_bridge, link);
        if(res < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not add interface [%s] to bridge [%s]",
                             host_ifname, bridge);
        }
    }

    // bring link up
    cthulhu_link_up(sk, link);
    amxc_var_set_cstring_t(msg.data, ifname);

    cthulhu_send_msg(ns_name, &msg);

exit:
    rtnl_link_put(link);
    rtnl_link_put(nl_bridge);
    nl_cache_free(link_cache);
    nl_socket_free(sk);
    cthulhu_msg_clean(&msg);
    return res;
}

/**
 * Destroy all veth pairs associated with an sandbox
 *
 * @param sb_data pravate data struct of the sandbox
 *
 * @return int
 */
static int cthulhu_destroy_veth(cthulhu_ns_proc_data_t* ns_data) {
    int res = -1;
    amxc_llist_it_t* it = NULL;
    struct nl_sock* sk = NULL;
    struct nl_cache* link_cache = NULL;

    sk = nl_socket_alloc();
    if(!sk) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate socket for netlink");
        goto exit;
    }
    res = nl_connect(sk, NETLINK_ROUTE);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not connect socket for netlink");
        goto exit;
    }
    res = rtnl_link_alloc_cache(sk, AF_UNSPEC, &link_cache);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not fetch netlink cache");
        goto exit;
    }

    while((it = amxc_llist_take_first(&ns_data->veth_interfaces)) != NULL) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* host_if = amxc_var_get_const_cstring_t(var);
        SAH_TRACEZ_INFO(ME, "Remove interface [%s]", host_if);
        struct rtnl_link* link = rtnl_link_get_by_name(link_cache, host_if);
        amxc_var_delete(&var);
        if(!link) {
            SAH_TRACEZ_WARNING(ME, "Link [%s] does not exist", host_if);
            continue;
        }
        if(!rtnl_link_is_veth(link)) {
            SAH_TRACEZ_WARNING(ME, "Link [%s] is not a veth", host_if);
            rtnl_link_veth_release(link);
            continue;
        }
        rtnl_link_veth_release(link);
    }
exit:
    nl_cache_free(link_cache);
    nl_socket_free(sk);
    return res;

}

/**
 * Delete the data
 *
 * @param ns_data_p
 */
static void cthulhu_ns_proc_data_delete(cthulhu_ns_proc_data_t** ns_data_p) {
    if(!ns_data_p) {
        return;
    }
    cthulhu_ns_proc_data_t* ns_data = *ns_data_p;

    if(!ns_data) {
        goto free_ns_data;
    }

    amxc_htable_it_clean(&ns_data->hit, NULL);
    if(ns_data->clone_stack) {
        munmap(ns_data->clone_stack, CLONE_STACK_SIZE);
    }
    amxc_llist_clean(&ns_data->veth_interfaces, variant_list_it_free);
free_ns_data:
    free(ns_data);
    ns_data_p = NULL;
}

/**
 * @brief Create namespace process data and add it to @ref ns_proc_data_table
 *
 * @param ns_name
 * @return cthulhu_ns_proc_data_t*
 */
static cthulhu_ns_proc_data_t* cthulhu_ns_proc_data_create(const char* ns_name) {
    cthulhu_ns_proc_data_t* ns_data = (cthulhu_ns_proc_data_t*) calloc(1, sizeof(cthulhu_ns_proc_data_t));
    char* sb_host_data = NULL;

    if(amxc_llist_init(&ns_data->veth_interfaces) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate veth_interfaces");
        free(ns_data);
        ns_data = NULL;
        goto exit;
    }

    ns_data->clone_stack = (char*) mmap(NULL, CLONE_STACK_SIZE, PROT_READ | PROT_WRITE,
                                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    if(ns_data->clone_stack == MAP_FAILED) {
        SAH_TRACEZ_ERROR(ME, "Could not create memmap for sandbox process");
        ns_data->clone_stack = NULL;
        goto error;
    }
    amxc_htable_insert(ns_proc_data_table, ns_name, &ns_data->hit);
    goto exit;
error:
    cthulhu_ns_proc_data_delete(&ns_data);
    ns_data = NULL;
exit:
    free(sb_host_data);
    return ns_data;
}

/**
 * @brief The htable that is cloned to the namespace should be freed
 * the stack however should not be freed, since it is still in use
 *
 * @param key
 * @param it
 */
static void ns_proc_data_it_free_in_ns(UNUSED const char* key, amxc_htable_it_t* it) {
    cthulhu_ns_proc_data_t* ns_proc_data = amxc_htable_it_get_data(it, cthulhu_ns_proc_data_t, hit);
    free(ns_proc_data);
}

/**
 * @brief Get the namespace process data
 *
 * @param ns_name
 * @return cthulhu_ns_proc_data_t*
 */
static cthulhu_ns_proc_data_t* cthulhu_ns_proc_data_get(const char* ns_name) {
    cthulhu_ns_proc_data_t* proc_data = NULL;
    amxc_htable_it_t* it = NULL;

    if(!ns_proc_data_table) {
        SAH_TRACEZ_ERROR(ME, "ns_proc_data_table is NULL");
        return NULL;
    }

    it = amxc_htable_get(ns_proc_data_table, ns_name);
    if(!it) {
        SAH_TRACEZ_ERROR(ME, "Proc data for namespace [%s] not found", ns_name);
        return NULL;
    }
    proc_data = amxc_htable_it_get_data(it, cthulhu_ns_proc_data_t, hit);
    return proc_data;
}

/**
 * Create namespaces for a sandbox
 *
 * @param sb_obj
 *
 * @return int pid of namespace
 */
static int cthulhu_ns_create_priv(amxc_var_t* ns_data) {
    int res = -1;
    const char* ns_name = NULL;
    amxc_var_t* uts_ns = NULL;
    amxc_var_t* net_ns = NULL;
    const char* net_ns_type = NULL;
    int clone_flags = SIGCHLD;
    cthulhu_msg_t msg;
    cthulhu_ns_proc_data_t* ns_proc_data = NULL;
    cthulhu_ns_clone_data_t* ns_clone_data = NULL;

    ns_name = GET_CHAR(ns_data, CTHULHU_SANDBOX_ID);
    ns_proc_data = cthulhu_ns_proc_data_create(ns_name);
    if(!ns_proc_data) {
        SAH_TRACEZ_ERROR(ME, "[%s] Could not create ns_proc_data", ns_name);
        goto exit;
    }

    ns_clone_data = cthulhu_ns_clone_data_create();
    if(!ns_clone_data) {
        SAH_TRACEZ_ERROR(ME, "[%s] Could not create ns_clone_data", ns_name);
        goto exit;
    }
    ns_clone_data->sb_name = strdup(ns_name);

    // UTS namespace part 1
    uts_ns = amxc_var_get_key(ns_data, CTHULHU_SANDBOX_UTS_NS, AMXC_VAR_FLAG_DEFAULT);
    if(uts_ns && GET_BOOL(uts_ns, CTHULHU_SANDBOX_UTS_NS_ENABLE)) {
        clone_flags |= CLONE_NEWUTS;
    }

    // Network namespace part 1
    net_ns = amxc_var_get_key(ns_data, CTHULHU_SANDBOX_NETWORK_NS, AMXC_VAR_FLAG_DEFAULT);
    net_ns_type = GET_CHAR(net_ns, CTHULHU_SANDBOX_NETWORK_NS_TYPE);
    if(net_ns && net_ns_type && GET_BOOL(net_ns, CTHULHU_SANDBOX_NETWORK_NS_ENABLE)) {
        // if parentSb, the ns is used from the parent
        if((strcmp(net_ns_type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY) == 0) ||
           (strcmp(net_ns_type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH) == 0)) {
            clone_flags |= CLONE_NEWNET;
        }
    }

    // TODO: add clone flags of other namespaces
    ns_proc_data->ns_pid = clone(namespace_proc,
                                 ns_proc_data->clone_stack + CLONE_STACK_SIZE,
                                 clone_flags,
                                 ns_clone_data);
    if(ns_proc_data->ns_pid == -1) {
        SAH_TRACEZ_ERROR(ME, "Could not clone into namespace (%d: %s)", errno, strerror(errno));
        goto exit;
    }

    cthulhu_msg_init(&msg, cthulhu_msg_ping);
    while(cthulhu_send_msg(ns_name, &msg) < 0) {
        SAH_TRACEZ_INFO(ME, "Waiting for sb process");
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 10 * 1000000; // 10ms
        nanosleep(&ts, &ts);
    }
    cthulhu_msg_clean(&msg);

    // UTS namespace part 2
    if(uts_ns && GET_BOOL(uts_ns, CTHULHU_SANDBOX_UTS_NS_ENABLE)) {
        cthulhu_msg_init(&msg, cthulhu_msg_set_hostname);
        const char* hostname = GET_CHAR(uts_ns, CTHULHU_SANDBOX_UTS_NS_HOSTNAME);
        amxc_var_set_cstring_t(msg.data, hostname);
        cthulhu_send_msg(ns_name, &msg);
        cthulhu_msg_clean(&msg);
    }

    // Network namespace part 2
    // if we inherrit the parentSb networkNS, no lo should be created
    if(net_ns && net_ns_type && GET_BOOL(net_ns, CTHULHU_SANDBOX_NETWORK_NS_ENABLE)) {
        // if parentSb, no lo or veth interfaces need to be created, since they
        // should already be there
        if((strcmp(net_ns_type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY) == 0) ||
           (strcmp(net_ns_type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH) == 0)) {
            cthulhu_msg_init(&msg, cthulhu_msg_itfup);
            amxc_var_set_cstring_t(msg.data, "lo");
            cthulhu_send_msg(ns_name, &msg);
            cthulhu_msg_clean(&msg);

            if(net_ns_type && (strcmp(net_ns_type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH) == 0)) {
                amxc_var_t* interface = amxc_var_get_first(GET_ARG(net_ns, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES));
                while(interface) {
                    const char* ifname = GET_CHAR(interface, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE);
                    const char* bridge = GET_CHAR(interface, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_BRIDGE);
                    cthulhu_create_veth(ns_name, ifname, bridge, ns_proc_data);
                    interface = amxc_var_get_next(interface);
                }
            }
        }
    }

    res = ns_proc_data->ns_pid;
    goto exit;

exit:
    cthulhu_ns_clone_data_delete(&ns_clone_data);
    return res;
}

/**
 * @brief Private helperto destroy a namespace
 *
 * @param ns_name
 */
static void cthulhu_ns_destroy_priv(const char* ns_name) {
    cthulhu_ns_proc_data_t* ns_proc_data = NULL;
    ns_proc_data = cthulhu_ns_proc_data_get(ns_name);
    cthulhu_msg_t msg;

    if(!ns_proc_data) {
        SAH_TRACEZ_ERROR(ME, "Could not get proc data for namespace [%s]", ns_name);
        return;
    }

    cthulhu_destroy_veth(ns_proc_data);

    if(ns_proc_data->ns_pid < 1) {
        SAH_TRACEZ_WARNING(ME, "NS PID: %d, nothing to destroy", ns_proc_data->ns_pid);
        return;
    }
    cthulhu_msg_init(&msg, cthulhu_msg_exit);
    cthulhu_send_msg(ns_name, &msg);
    cthulhu_msg_clean(&msg);
    cthulhu_close_socket(ns_name);
    cthulhu_wait_for_pid(ns_proc_data->ns_pid);
    cthulhu_ns_proc_data_delete(&ns_proc_data);
    return;
}

/**
 * @brief Create an @ref amxc_var_t thast contains all necessary data to create a namespace
 *
 * @param sb_obj
 * @return amxc_var_t*
 */
static amxc_var_t* cthulhu_ns_data_create(amxd_object_t* sb_obj) {
    amxc_var_t* ns_data = NULL;
    amxd_object_t* uts_ns = NULL;
    amxd_object_t* net_ns = NULL;

    amxc_var_new(&ns_data);

    amxd_object_get_params(sb_obj, ns_data, amxd_dm_access_private);
    uts_ns = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_UTS_NS);
    if(uts_ns) {
        amxc_var_t* uts_data = amxc_var_add_new_key(ns_data, CTHULHU_SANDBOX_UTS_NS);
        amxd_object_get_params(uts_ns, uts_data, amxd_dm_access_private);
    }
    net_ns = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    if(net_ns) {
        amxc_var_t* net_data = amxc_var_add_new_key(ns_data, CTHULHU_SANDBOX_NETWORK_NS);
        amxd_object_get_params(net_ns, net_data, amxd_dm_access_private);
        amxc_var_t* if_list = amxc_var_add_new_key(net_data, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
        amxc_var_set_type(if_list, AMXC_VAR_ID_LIST);
        amxd_object_t* interfaces = amxd_object_get_child(net_ns, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
        amxd_object_for_each(instance, it, interfaces) {
            amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
            amxc_var_t* if_data = amxc_var_add_new(if_list);
            amxd_object_get_params(instance, if_data, amxd_dm_access_private);
        }
    }
    return ns_data;
}

/**
 * @brief Create a namespace for a sandbox
 *
 * @param sb_obj
 * @param parent_sb The parent sandbox, or NULL if no parent
 * @return int 0 on success, -1 on failure
 */
int cthulhu_namespaces_create(amxd_object_t* sb_obj, const char* parent_sb) {
    int res = -1;
    int ns_pid = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* ns_data = cthulhu_ns_data_create(sb_obj);

    if(!ns_data) {
        SAH_TRACEZ_ERROR(ME, "Could not create ns_data");
        goto exit;
    }

    if(string_is_empty(parent_sb)) {
        ns_pid = cthulhu_ns_create_priv(ns_data);
    } else {
        // create namespace in the namespace of the parent
        cthulhu_msg_t msg;
        cthulhu_msg_init(&msg, cthulhu_msg_create_ns);
        amxc_var_move(msg.data, ns_data);
        ns_pid = cthulhu_send_msg(parent_sb, &msg);
        cthulhu_msg_clean(&msg);
    }
    if(ns_pid > 0) {
        res = 0;
    }

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, sb_obj);
    amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_PID, ns_pid);
    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed: %d", status);
    }
    amxd_trans_clean(&trans);
exit:
    amxc_var_delete(&ns_data);
    return res;
}

/**
 * Destroy namespaces of a sandbox
 *
 * @param sb_obj
 */
int cthulhu_namespaces_destroy(amxd_object_t* sb_obj) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    char* ns_name = NULL;
    char* parent_sb = NULL;

    ns_name = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);
    parent_sb = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);

    if(string_is_empty(parent_sb)) {
        cthulhu_ns_destroy_priv(ns_name);
    } else {
        // destroy namespace in the namespace of the parent
        cthulhu_msg_t msg;
        cthulhu_msg_init(&msg, cthulhu_msg_destroy_ns);
        amxc_var_set_cstring_t(msg.data, ns_name);
        cthulhu_send_msg(parent_sb, &msg);
        cthulhu_msg_clean(&msg);
    }
    // also close a possible socket on the main thread when we have a nested sandbox
    // this socket might have been created by calling an exec on the main thread
    cthulhu_close_socket(ns_name);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, sb_obj);
    amxd_trans_set_int32_t(&trans, CTHULHU_SANDBOX_PID, -1);
    status = amxd_trans_apply(&trans, cthulhu_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed: %d", status);
    }
    amxd_trans_clean(&trans);
    res = 0;

    free(ns_name);
    free(parent_sb);
    return res;
}

int cthulhu_namespaces_exec(amxd_object_t* sb_obj, const char* cmd) {
    int res = -1;
    cthulhu_msg_t msg;
    char* ns_name = NULL;

    ns_name = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);

    cthulhu_msg_init(&msg, cthulhu_msg_cmd);
    amxc_var_set_cstring_t(msg.data, cmd);
    res = cthulhu_send_msg(ns_name, &msg);
    cthulhu_msg_clean(&msg);

    free(ns_name);
    return res;
}

int cthulhu_namespaces_init(void) {
    amxc_htable_new(&ns_proc_data_table, 10);
    amxc_htable_new(&ns_socket_table, 10);
    cthulhu_app_t* app_data = cthulhu_get_app_data();

    sb_host_data_location = strdup(app_data->sb_host_data_location);
    return 0;
}

void cthulhu_namespaces_cleanup(void) {
    amxc_htable_delete(&ns_proc_data_table, ns_proc_data_it_free_in_ns);
    amxc_htable_delete(&ns_socket_table, ns_socket_it_free);
    free(sb_host_data_location);
}
