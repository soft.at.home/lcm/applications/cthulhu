/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/acl.h>
#include <acl/libacl.h>
#include <errno.h>
#include <limits.h>
#include <dirent.h>
#include <sys/stat.h>

#include <lcm/lcm_helpers.h>

#include "cthulhu_priv.h"
#include "cthulhu_acl.h"

#define ME "acl"

static void print_acl(acl_t acl) {
    acl_entry_t entry = NULL;
    acl_permset_t permset;
    amxc_string_t tag;
    amxc_string_init(&tag, 32);
    for(int ent = ACL_FIRST_ENTRY; ; ent = ACL_NEXT_ENTRY) {
        amxc_string_clean(&tag);
        int s = acl_get_entry(acl, ent, &entry);
        if((s == -1) || !entry) {
            break;
        }
        acl_tag_t entryTag;
        ASSERT_NOT_EQUAL(acl_get_tag_type(entry, &entryTag), -1, goto exit);

        amxc_string_appendf(&tag, "TAG: 0x%x", entryTag);
        uid_t* uidp = acl_get_qualifier(entry);
        if(uidp) {
            amxc_string_appendf(&tag, " QUAL: %d", *uidp);
            acl_free(uidp);
        }
        ASSERT_NOT_EQUAL(acl_get_permset(entry, &permset), -1, goto exit);

        if(acl_get_perm(permset, ACL_READ)) {
            amxc_string_appendf(&tag, " R");
        }
        if(acl_get_perm(permset, ACL_WRITE)) {
            amxc_string_appendf(&tag, " W");
        }
        if(acl_get_perm(permset, ACL_EXECUTE)) {
            amxc_string_appendf(&tag, " E");
        }
        SAH_TRACEZ_ERROR(ME, "%s", tag.buffer);
    }
exit:
    return;
}

void print_acl_file(const char* filename) {
    acl_t acl = NULL;
    acl = acl_get_file(filename, ACL_TYPE_ACCESS);
    SAH_TRACEZ_ERROR("ME", "ACL for file %s", filename);
    print_acl(acl);
    if(acl) {
        acl_free(acl);
    }
}

static acl_entry_t cthulhu_acl_find_entry(acl_t acl, acl_tag_t tag, uid_t qual) {
    acl_entry_t entry = NULL;
    acl_tag_t entry_tag;

    for(int ent = ACL_FIRST_ENTRY; ; ent = ACL_NEXT_ENTRY) {
        int res = acl_get_entry(acl, ent, &entry);
        ASSERT_NOT_EQUAL(res, -1, entry = NULL; goto exit, "acl_get_entry failed %d", tag);
        if(res == 0) {
            ASSERT_NOT_EQUAL(ent, ACL_FIRST_ENTRY, goto exit, "ACL does not contain entries");
            // we have reached the last entry
            break;
        }
        ASSERT_NOT_NULL(entry, goto exit);
        ASSERT_NOT_EQUAL(acl_get_tag_type(entry, &entry_tag), -1, entry = NULL; goto exit, "Could not get entry tag type. tag %d", tag);

        if(entry_tag == tag) {
            if((tag != ACL_USER) && (tag != ACL_GROUP)) {
                goto exit;
            }
            // only ACL_USER and ACL_GROUP can have qualifiers
            uid_t* entry_qual = (uid_t*) acl_get_qualifier(entry);
            if(entry_qual && (*entry_qual == qual)) {
                if(entry_qual) {
                    acl_free(entry_qual);
                }
                goto exit;
            }
            if(entry_qual) {
                acl_free(entry_qual);
            }

        }
    }
exit:
    return entry;

}

static int cthulhu_acl_copy_permset(acl_t acl, acl_tag_t src_tag, uid_t src_qual, acl_tag_t dest_tag, uid_t dest_qual, bool overwrite) {
    int res = -1;
    acl_entry_t src_entry = NULL;
    acl_entry_t dest_entry = NULL;
    acl_permset_t permset;

    dest_entry = cthulhu_acl_find_entry(acl, dest_tag, dest_qual);
    if(dest_entry && !overwrite) {
        // do not overwrite
        res = 0;
        goto exit;
    }

    src_entry = cthulhu_acl_find_entry(acl, src_tag, src_qual);
    ASSERT_NOT_NULL(src_entry, goto exit);
    ASSERT_SUCCESS(acl_get_permset(src_entry, &permset), goto exit);

    if(!dest_entry) {
        ASSERT_SUCCESS(acl_create_entry(&acl, &dest_entry), goto exit);
        ASSERT_SUCCESS(acl_set_tag_type(dest_entry, dest_tag), goto exit);
        if((dest_tag == ACL_USER) || (dest_tag == ACL_GROUP)) {
            ASSERT_SUCCESS(acl_set_qualifier(dest_entry, &dest_qual), goto exit);
        }
    }
    ASSERT_SUCCESS(acl_set_permset(dest_entry, permset), goto exit);

    res = 0;
exit:
    return res;
}

static int cthulhu_acl_remove_entry(acl_t acl, acl_tag_t tag, uid_t qual, bool* modified) {
    int res = -1;
    acl_entry_t entry = NULL;
    for(int ent = ACL_FIRST_ENTRY; ; ent = ACL_NEXT_ENTRY) {
        int s = acl_get_entry(acl, ent, &entry);
        if((s == -1) || !entry) {
            break;
        }
        acl_tag_t entryTag;
        ASSERT_NOT_EQUAL(acl_get_tag_type(entry, &entryTag), -1, goto exit);
        if(entryTag != tag) {
            continue;
        }

        uid_t* uidp = acl_get_qualifier(entry);
        if(!uidp || (*uidp != qual)) {
            acl_free(uidp);
            continue;
        }
        acl_free(uidp);
        uidp = NULL;
        ASSERT_NOT_EQUAL(acl_delete_entry(acl, entry), -1, goto exit);
        *modified = true;
        break;
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief set the acl of the file
 *
 * modify the acl, so that a member of the group defined in gid has the same rights
 * as the owner of the file, if the owner is root. This way, any member of this group has the same rights as
 * the root user of the layer.
 *
 * @param filename
 * @param gid
 * @return int
 */
static int cthulhu_acl_set(const char* filename, uid_t uid, gid_t gid) {
    int res = -1;
    acl_t acl = NULL;

    acl = acl_get_file(filename, ACL_TYPE_ACCESS);
    ASSERT_NOT_NULL(acl, goto exit, "No ACL found for %s", filename);

    ASSERT_SUCCESS(cthulhu_acl_copy_permset(acl, ACL_USER_OBJ, 0, ACL_USER, uid, false), goto exit);
    ASSERT_SUCCESS(cthulhu_acl_copy_permset(acl, ACL_GROUP_OBJ, 0, ACL_GROUP, gid, false), goto exit);

    ASSERT_SUCCESS(acl_calc_mask(&acl), goto exit);
    ASSERT_SUCCESS(acl_valid(acl), goto exit);

    ASSERT_SUCCESS(acl_set_file(filename, ACL_TYPE_ACCESS, acl), goto  exit);

    res = 0;
exit:
    if(acl) {
        acl_free(acl);
    }
    return res;
}

/**
 * @brief remove the acl for a specific uid and gid of the file
 *
 * modify the acl, so that the user and group defined in uid and gid do not longer have access
 *
 * @param filename
 * @param gid
 * @return int
 */
static int cthulhu_acl_remove(const char* filename, uid_t uid, gid_t gid) {
    int res = -1;
    acl_t acl = NULL;
    bool modified = false;

    acl = acl_get_file(filename, ACL_TYPE_ACCESS);
    ASSERT_NOT_NULL(acl, goto exit, "No ACL found for %s", filename);

    ASSERT_SUCCESS(cthulhu_acl_remove_entry(acl, ACL_USER, uid, &modified), goto exit);
    ASSERT_SUCCESS(cthulhu_acl_remove_entry(acl, ACL_GROUP, gid, &modified), goto exit);

    if(!modified) {
        res = 0;
        goto exit;
    }
    ASSERT_SUCCESS(acl_calc_mask(&acl), goto exit);
    ASSERT_SUCCESS(acl_valid(acl), goto exit);

    ASSERT_SUCCESS(acl_set_file(filename, ACL_TYPE_ACCESS, acl), goto  exit);

    res = 0;
exit:
    if(acl) {
        acl_free(acl);
    }
    return res;
}

int cthulhu_acl_set_recursive(const char* filename, uid_t uid, gid_t gid) {
    int res = -1;
    DIR* dir = NULL;
    struct dirent* entry;
    char path[PATH_MAX] = {0};

    lcm_file_type_t type = lcm_filetype(filename);
    if((type == FILE_TYPE_SYMLINK) || (type == FILE_TYPE_UNKNOWN)) {
        res = 0;
        goto exit;
    }

    CHECK_FAILURE_LOG(WARNING, cthulhu_acl_set(filename, uid, gid), , "cthulhu_acl_set failed for file %s", filename);

    // recurse over all entries if the filename is a dir
    if(type == FILE_TYPE_DIRECTORY) {
        dir = opendir(filename);
        ASSERT_NOT_NULL(dir, goto exit, "Could not open %s (%d: %s)", filename, errno, strerror(errno));

        while((entry = readdir(dir))) {
            if((strcmp(entry->d_name, ".") == 0) ||
               (strcmp(entry->d_name, "..") == 0)) {
                continue;
            }
            int written = snprintf(path, sizeof(path), "%s/%s", filename, entry->d_name);
            if((written < 0) || (written >= (int) sizeof(path))) {
                SAH_TRACEZ_ERROR(ME, "Failed to construct full filename for %s, written: %d", filename, written);
                goto exit;
            }
            ASSERT_SUCCESS(cthulhu_acl_set_recursive(path, uid, gid), goto exit, "cthulhu_acl_set_recursive failed for path %s", path);
        }
    }

    res = 0;
exit:
    if(dir) {
        closedir(dir);
    }
    return res;
}

int cthulhu_acl_remove_recursive(const char* filename, uid_t uid, gid_t gid) {
    int res = -1;
    DIR* dir = NULL;
    struct dirent* entry;
    char path[PATH_MAX] = {0};

    lcm_file_type_t type = lcm_filetype(filename);
    if((type == FILE_TYPE_SYMLINK) || (type == FILE_TYPE_UNKNOWN)) {
        res = 0;
        goto exit;
    }

    CHECK_FAILURE_LOG(WARNING, cthulhu_acl_remove(filename, uid, gid), , "cthulhu_acl_remove failed for file %s", filename);

    // recurse over all entries if the filename is a dir
    if(type == FILE_TYPE_DIRECTORY) {
        dir = opendir(filename);
        ASSERT_NOT_NULL(dir, goto exit, "Could not open %s (%d: %s)", filename, errno, strerror(errno));

        while((entry = readdir(dir))) {
            if((strcmp(entry->d_name, ".") == 0) ||
               (strcmp(entry->d_name, "..") == 0)) {
                continue;
            }
            int written = snprintf(path, sizeof(path), "%s/%s", filename, entry->d_name);
            if((written < 0) || (written >= (int) sizeof(path))) {
                SAH_TRACEZ_ERROR(ME, "Failed to construct full filename for %s, written: %d", filename, written);
                goto exit;
            }
            ASSERT_SUCCESS(cthulhu_acl_remove_recursive(path, uid, gid), goto exit, "cthulhu_acl_remove_recursive failed for path %s", path);
        }
    }

    res = 0;
exit:
    if(dir) {
        closedir(dir);
    }
    return res;
}
