/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_ctr_info.h>

#include "cthulhu_priv.h"

#define ME "events"

void _container_autorestart_enabled(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    amxd_object_t* autostart = amxd_dm_signal_get_object(cthulhu_get_dm(), data);
    amxd_object_t* ctr_obj = NULL;
    char* status = NULL;
    char* ctr_id = NULL;

    if(!autostart) {
        SAH_TRACEZ_ERROR(ME, "Object not found");
        return;
    }
    ctr_obj = amxd_object_get_parent(autostart);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "Object not found");
        return;
    }
    ctr_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_ID, NULL);
    status = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_STATUS, NULL);
    SAH_TRACEZ_INFO(ME, CTHULHU_CONTAINER_STATUS " %s", status);

    if(cthulhu_string_to_ctr_status(status) != cthulhu_ctr_status_running) {
        SAH_TRACEZ_INFO(ME, "Start container %s", ctr_id);
        cthulhu_ctr_start(ctr_id, NULL);
    }
//exit:
    free(ctr_id);
    free(status);
}


void _print_event(UNUSED const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    SAH_TRACEZ_ERROR(ME, "Signal received - %s\n", sig_name);
    SAH_TRACEZ_ERROR(ME, "Signal data = \n");
    fflush(stdout);
    if(!amxc_var_is_null(data)) {
        amxc_var_dump(data, STDOUT_FILENO);
    }
}

#define AMXB_PARAMETERS "parameters"
#define AMXB_PARAMETERS_TO "to"

void _sandbox_updated(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    amxd_object_t* sb_obj = amxd_dm_signal_get_object(cthulhu_get_dm(), data);
    amxc_var_t* parameters = amxc_var_get_key(data, AMXB_PARAMETERS, AMXC_VAR_FLAG_DEFAULT);
    cthulhu_notif_data_t* notif_data = NULL;
    cthulhu_notif_data_new(&notif_data, sb_obj, cthulhu_notif_cmd_none, NULL);

    amxc_var_t* var_enable = amxc_var_get_key(parameters, CTHULHU_SANDBOX_ENABLE, AMXC_VAR_FLAG_DEFAULT);
    if(var_enable) {
        bool enable = GET_BOOL(var_enable, AMXB_PARAMETERS_TO);
        char* sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);

        if(enable) {
            cthulhu_sb_start(sb_id, notif_data);
        } else {
            cthulhu_sb_stop(sb_id, notif_data);
        }
        free(sb_id);
    }

    // event updated sandbox
    cthulhu_notif_sb_updated(notif_data, parameters);
    cthulhu_notif_data_delete(&notif_data);
}

void _container_updated(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_object_t* ctr_obj = amxd_dm_signal_get_object(cthulhu_get_dm(), data);
    if(!ctr_obj) {
        return;
    }
    amxc_var_t* parameters = amxc_var_get_key(data, AMXB_PARAMETERS, AMXC_VAR_FLAG_DEFAULT);
    cthulhu_notif_data_t* notif_data = NULL;

    cthulhu_notif_data_new(&notif_data, ctr_obj, cthulhu_notif_cmd_none, NULL);

    // event updated container
    cthulhu_notif_ctr_updated(notif_data, parameters);
    cthulhu_notif_data_delete(&notif_data);
}

void cthulhu_ctr_priv_init(cthulhu_ctr_priv_t* ctr_priv) {
    if(!ctr_priv) {
        return;
    }

    ctr_priv->real_status = cthulhu_ctr_status_ghost;
}

void cthulhu_ctr_priv_clean(cthulhu_ctr_priv_t* ctr_priv) {
    if(!ctr_priv) {
        return;
    }
    if(ctr_priv->retry_timer) {
        amxp_timer_delete(&(ctr_priv->retry_timer));
    }
    if(ctr_priv->retry_reset_timer) {
        amxp_timer_delete(&(ctr_priv->retry_reset_timer));
    }
}


