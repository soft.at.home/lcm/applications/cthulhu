/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <stdio.h>
#include <sys/mount.h>
#include <limits.h>
#include <dirent.h>

#include "cthulhu_priv.h"
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>
#include <lcm/lcm_helpers.h>

#include "cthulhu_unprivileged.h"

#define ME "unpriv"

#define ID_START 0x3000
#define ID_RANGE 0x5000

typedef struct _range {
    amxc_llist_it_t it;
    uint32_t start;
    uint32_t end;
} range_t;

static bool idmapping_is_supported = false;

char* cthulhu_unpriv_get_username(const char* ctr_id);


/**
 * @brief check if the container is unprivileged
 *
 *
 * @param ctr_id
 * @return bool true if inprivileged
 */
static int cthulhu_ctr_is_unprivileged(const char* ctr_id) {
    amxd_object_t* unpriv_config = NULL;
    amxd_object_t* ctr_obj = NULL;
    bool ctr_is_unpriv = false;
    amxd_status_t s;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    unpriv_config = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    ASSERT_NOT_NULL(unpriv_config, goto exit, "CTR[%s]: Could not get Unprivileged object", ctr_id);
    ctr_is_unpriv = amxd_object_get_bool(unpriv_config, CTHULHU_CONTAINER_UNPRIVILEGED_ENABLED, &s);
    ASSERT_EQUAL(s, amxd_status_ok, goto exit);

exit:
    return ctr_is_unpriv;
}

/**
 * @brief get info on whether ownership should be changed and to which ods
 *
 * @param ctr_id hte container to check
 * @param change_ownership filled in with true if ownership should be changed
 * @param uid the uid of the new owner
 * @param gid the gid of the new owner
 * @return int
 */
int cthulhu_unpriv_get_settings(const char* ctr_id, bool* change_ownership, uid_t* uid, gid_t* gid) {
    int res = -1;
    char* username = NULL;
    char* groupname = NULL;

    *change_ownership = cthulhu_ctr_is_unprivileged(ctr_id);

    if(*change_ownership && (uid || gid)) {
        username = cthulhu_unpriv_get_username(ctr_id);
        ASSERT_STR_NOT_EMPTY(username, goto exit, "CTR[%s]: could not get username", ctr_id);
        struct passwd* usrpwd = getpwnam(username);
        ASSERT_NOT_NULL(usrpwd, goto exit, "Could not define uid for user [%s] (%d: %s)", username, errno, strerror(errno));
        if(uid) {
            *uid = usrpwd->pw_uid;
        }
        if(gid) {
            *gid = usrpwd->pw_gid;
        }
    }
    res = 0;
exit:
    free(username);
    free(groupname);
    return res;
}

/**
 * @brief check if the uid+subuid is already present in the file, if not, add it.
 *
 * @param filename
 * @param uid
 * @param subuid
 * @param range
 * @return int
 */
static int cthulhu_unpriv_add_range(const char* filename, int uid, int subuid, int range) {
    int res = -1;
    char line[100] = {0};
    int cur_uid = 0;
    int cur_subuid = 0;
    int cur_range = 0;

    FILE* file = fopen(filename, "a+");
    ASSERT_NOT_NULL(file, goto exit, "Could not open file [%s] (%d: %s)", filename, errno, strerror(errno));

    while(fgets(line, sizeof(line), file) != NULL) {
        sscanf(line, "%d:%d:%d", &cur_uid, &cur_subuid, &cur_range);
        if((cur_uid == uid) && (cur_subuid == subuid)) {
            goto exit_success;
        }
    }
    // entry not found, add it
    if(fprintf(file, "%d:%d:%d\n", uid, subuid, range) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add entry to %s", filename);
        goto exit;
    }

exit_success:
    res = 0;
exit:
    if(file) {
        fclose(file);
    }
    return res;
}

static void cthulhu_unpriv_check_capabilities(void) {
    // check is if overlayfs is requested by the config, and supported by the kernel
    char* idmap_src_dir = NULL;
    char* idmap_dest_dir = NULL;
    char idmap_layer[] = "idmap_testdir";
    char idmap_ctr[] = "idmap_ctr";
    char* overlayfs_dir = NULL;
    char upperdir[PATH_MAX] = {0};
    char workdir[PATH_MAX] = {0};
    char dest_base_path[PATH_MAX] = {0};
    amxc_llist_t usermappings;
    amxc_llist_init(&usermappings);


    bool overlayfs_supported = cthulhu_overlayfs_supported();
    bool idmap_supported = cthulhu_idmap_supported(overlayfs_supported);
    if(!idmap_supported) {
        SAH_TRACEZ_WARNING(ME, "ID mapping is not supported%s", overlayfs_supported? " with overlayfs": "");
        idmapping_is_supported = false;
        goto exit;
    }
    // check if idmapping with overlayfs is supported on the filesystem
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    ASSERT_NOT_NULL(app_data, goto exit);
    idmap_src_dir = cthulhu_layer_get_dir_from_name(app_data->layer_location, idmap_layer);
    ASSERT_NOT_NULL(idmap_src_dir, goto exit);
    ASSERT_SUCCESS(cthulhu_mkdir(idmap_src_dir, true), goto exit, "Could not create idmap test dir [%s]", idmap_src_dir);
    if(umount(idmap_src_dir) == 0) {
        SAH_TRACEZ_INFO(ME, "idmap_src_dir [%s] was still mounted. unmount successful", idmap_src_dir);
    }
    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_user, 0, 1000, 1);
    cthulhu_usermapping_data_add_to_list(&usermappings, cthulhu_usermapping_group, 0, 1000, 1);

    idmap_dest_dir = cthulhu_idmap_layer(idmap_layer, idmap_ctr, &usermappings);
    if(!idmap_dest_dir) {
        SAH_TRACEZ_WARNING(ME, "Idmapping is not supported on this board");
        idmapping_is_supported = false;
        goto exit;
    }
    if(cthulhu_overlayfs_supported()) {
        overlayfs_dir = cthulu_overlayfs_get_rootfs(idmap_ctr);
        ASSERT_SUCCESS(cthulhu_mkdir(overlayfs_dir, true), goto exit);
        ASSERT_TRUE(snprintf(upperdir, sizeof(upperdir) - 1, "%s/%s/upper", app_data->data_location, idmap_ctr) > 0,
                    goto exit, "Could not generate upperdir string");
        ASSERT_TRUE(snprintf(workdir, sizeof(workdir) - 1, "%s/%s/work", app_data->data_location, idmap_ctr) > 0,
                    goto exit, "Could not generate workdir string");
        ASSERT_SUCCESS(cthulhu_mkdir(upperdir, true), goto exit);
        ASSERT_SUCCESS(cthulhu_mkdir(workdir, true), goto exit);
        if(umount(overlayfs_dir) == 0) {
            SAH_TRACEZ_INFO(ME, "overlayfs_dir [%s] was still mounted. unmount successful", overlayfs_dir);
        }

        if(cthulhu_overlayfs_mount(idmap_dest_dir, upperdir, workdir, overlayfs_dir) < 0) {
            SAH_TRACEZ_WARNING(ME, "Overlayfs of a idmapped lower layer is not supported on this board");
            idmapping_is_supported = false;
            goto exit;
        }
    }
    SAH_TRACEZ_NOTICE(ME, "Idmapping is supported on this board");
    idmapping_is_supported = true;
    goto exit;

exit:
    amxc_llist_clean(&usermappings, cthulhu_usermapping_data_it_free);
    if(overlayfs_dir) {
        umount(overlayfs_dir);
        cthulhu_rmdir(upperdir);
        cthulhu_rmdir(workdir);
        cthulhu_rmdir(overlayfs_dir);
        free(overlayfs_dir);
    }
    if(idmap_dest_dir) {
        umount(idmap_dest_dir);
        cthulhu_rmdir(idmap_dest_dir);

        if(snprintf(dest_base_path, PATH_MAX, "%s/idmap/%s", app_data->layer_location, idmap_ctr) !=
           (int) (strlen(app_data->layer_location) + strlen(idmap_ctr) + strlen("/idmap/"))) {
            SAH_TRACEZ_WARNING(ME, "Could not generate idmap_ctr string. idmap_ctr dir won't be removed");
        } else {
            cthulhu_rmdir(dest_base_path);

            // Check and remove the idmap dir if empty
            if(snprintf(dest_base_path, PATH_MAX, "%s/idmap", app_data->layer_location) !=
               (int) (strlen(app_data->layer_location) + strlen("/idmap"))) {
                SAH_TRACEZ_WARNING(ME, "Could not generate idmap layers upper dir. idmap layers won't be cleaned");
            } else {
                if(lcm_dir_empty(dest_base_path) == true) {
                    cthulhu_rmdir(dest_base_path);
                }
            }
        }

        free(idmap_dest_dir);
    }
    if(idmap_src_dir) {
        cthulhu_rmdir(idmap_src_dir);
        free(idmap_src_dir);
    }
    cthulhu_rmdir(upperdir);
    cthulhu_rmdir(workdir);
}

/**
 * @brief add uid to the /etc/subuid file
 *
 * @param uid
 * @param subuid
 * @param range
 * @return int
 */
int cthulhu_unpriv_add_subuid(int uid, int subuid, int range) {
    return cthulhu_unpriv_add_range("/etc/subuid", uid, subuid, range);
}

/**
 * @brief addd gid to the /etc/subgid file
 *
 * @param gid
 * @param subgid
 * @param range
 * @return int
 */
int cthulhu_unpriv_add_subgid(int gid, int subgid, int range) {
    return cthulhu_unpriv_add_range("/etc/subgid", gid, subgid, range);
}


int cthulhu_unpriv_init(void) {
    int res = -1;
    ASSERT_SUCCESS(cthulhu_unpriv_add_range("/etc/subuid", 0, ID_START, ID_RANGE), goto exit);
    ASSERT_SUCCESS(cthulhu_unpriv_add_range("/etc/subgid", 0, ID_START, ID_RANGE), goto exit);
    cthulhu_unpriv_check_capabilities();

    res = 0;
exit:
    return res;
}

static void range_list_it_free(amxc_llist_it_t* it) {
    range_t* range = amxc_llist_it_get_data(it, range_t, it);
    ASSERT_NOT_NULL(range, return );
    amxc_llist_it_take(&(range->it));
    free(range);
}

static int compare_ranges(amxc_llist_it_t* it1, amxc_llist_it_t* it2) {
    range_t* r1 = amxc_llist_it_get_data(it1, range_t, it);
    range_t* r2 = amxc_llist_it_get_data(it2, range_t, it);

    if(r1->start == r2->start) {
        return 0;
    } else if(r1->start < r2->start) {
        return -1;
    } else {
        return 1;
    }
}

/**
 * @brief Find the next free range of uids or guids
 *
 * @param uid this should be initialized with the first id of the global range.
 * @param range the requested range of ids
 * @param param_id the parameter of the ID in the DM (uid or gid)
 * @param param_range the parameter of the range in the DM (uid or gid)
 * @return int
 */
static int cthulhu_unpriv_get_next_free_range(uid_t* uid, uint32_t range, const char* param_id, const char* param_range) {
    int res = -1;
    amxc_llist_t ranges;
    amxd_object_t* containers = NULL;

    amxc_llist_init(&ranges);
    // add all existing ranges
    containers = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES);
    ASSERT_NOT_NULL(containers, goto exit);

    // go over all containers and ad the range to the list
    amxd_object_for_each(instance, it_ctr, containers) {
        amxd_object_t* ctr_obj = amxc_llist_it_get_data(it_ctr, amxd_object_t, it);
        amxd_object_t* unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
        ASSERT_NOT_NULL(unpriv_obj, goto exit, "Could not get Unprivileged object");
        uint32_t start = amxd_object_get_uint32_t(unpriv_obj, param_id, NULL);
        if(start == 0) {
            // the container does not have ids assigned
            continue;
        }
        uint32_t ctr_range = amxd_object_get_uint32_t(unpriv_obj, param_range, NULL);
        CHECK_EQUAL_LOG(WARNING, ctr_range, 0, continue, "container has a range of 0, which should not be possible");
        range_t* r = calloc(1, sizeof(range_t));
        r->start = start;
        r->end = start + ctr_range - 1;
        amxc_llist_append(&ranges, &(r->it));
    }
    // if no ranges are occupied yet, then this range is good
    if(amxc_llist_is_empty(&ranges)) {
        goto exit_success;
    }
    // sort the ranges
    amxc_llist_sort(&ranges, compare_ranges);

    // go over all ranges, and check if the requested range fits in the gap
    amxc_llist_it_t* ranges_it = amxc_llist_get_first(&ranges);
    while(ranges_it) {
        range_t* r = amxc_llist_it_get_data(ranges_it, range_t, it);
        if((*uid + range - 1) < r->start) {
            // range found
            break;
        }
        *uid = r->end + 1;

        ranges_it = amxc_llist_it_get_next(ranges_it);
    }

exit_success:
    res = 0;
exit:
    amxc_llist_clean(&ranges, range_list_it_free);
    return res;
}


static int cthulhu_unpriv_get_subids(const char* ctr_id, uid_t* uid, uint32_t* range, const char* param_id, const char* param_range) {
    int res = -1;
    // first check if it is defined in the datamodel, otherwise get new values
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    ASSERT_NOT_NULL(unpriv_obj, goto exit, "CTR[%s]: Could not get Unprivileged object", ctr_id);

    *uid = amxd_object_get_uint32_t(unpriv_obj, param_id, NULL);
    *range = amxd_object_get_uint32_t(unpriv_obj, param_range, NULL);
    if(*uid > 0) {
        goto exit_success;
    }
    if(*range == 0) {
        goto exit_success;
    }

    *uid = ID_START;

    ASSERT_SUCCESS(cthulhu_unpriv_get_next_free_range(uid, *range, param_id, param_range),
                   goto exit, "Failed to get a free range of IDs for %s", param_id);

    amxd_object_set_uint32_t(unpriv_obj, param_id, *uid);
    amxd_object_set_uint32_t(unpriv_obj, param_range, *range);

exit_success:
    res = 0;
exit:
    return res;
}

int cthulhu_unpriv_get_user_subids(const char* ctr_id, uid_t* uid, uint32_t* range) {
    return cthulhu_unpriv_get_subids(ctr_id, uid, range, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGRANGE);
}
int cthulhu_unpriv_get_group_subids(const char* ctr_id, gid_t* gid, uint32_t* range) {
    return cthulhu_unpriv_get_subids(ctr_id, gid, range, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGRANGE);
}

/**
 * @brief get the username associated with an unprivileged container
 *
 * @param username
 * @return char* username. Should be freed.
 */
char* cthulhu_unpriv_get_username(const char* ctr_id) {
    char* username = NULL;
    amxd_object_t* ctr_obj = NULL;

    ASSERT_TRUE(cthulhu_ctr_is_unprivileged(ctr_id), goto exit);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    uint32_t index = amxd_object_get_index(ctr_obj);

    int res = asprintf(&username, "cthulhu_%u", index);
    ASSERT_NOT_EQUAL(res, -1, goto exit, "CTR[%s]: Failed to create username string", ctr_id);

exit:
    return username;

}

static int cthulhu_unpriv_store_user_in_dm(const char* ctr_id, const char* username) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;
    bool change_ownership = false;
    uid_t uid = 0;
    gid_t gid = 0;


    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    ASSERT_NOT_NULL(unpriv_obj, goto exit, "CTR[%s]: Could not get Unprivileged object", ctr_id);

    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &uid, &gid), goto exit, "Could not retreive unpriv settings");

    cthulhu_trans_new(&trans, unpriv_obj);

    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_UID, uid);
    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GID, gid);
    amxd_trans_set_cstring_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_USERNAME, username);
    amxd_trans_set_cstring_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GROUPNAME, username);

    status = cthulhu_trans_send(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }

exit:
    return res;
}

static int cthulhu_unpriv_remove_user_from_dm(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    ASSERT_NOT_NULL(unpriv_obj, goto exit, "CTR[%s]: Could not get Unprivileged object", ctr_id);

    cthulhu_trans_new(&trans, unpriv_obj);

    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, 0);
    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, 0);
    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_UID, 0);
    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GID, 0);
    amxd_trans_set_cstring_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_USERNAME, "");
    amxd_trans_set_cstring_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GROUPNAME, "");

    status = cthulhu_trans_send(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }
    res = 0;
exit:
    return res;
}

int cthulhu_unpriv_ctr_dm_reset_mappings(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* unpriv_obj = NULL;
    amxd_trans_t* trans = NULL;
    amxd_status_t status = amxd_status_ok;

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    unpriv_obj = amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_UNPRIVILEGED);
    ASSERT_NOT_NULL(unpriv_obj, goto exit, "CTR[%s]: Could not get Unprivileged object", ctr_id);

    cthulhu_trans_new(&trans, unpriv_obj);

    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_UIDMAPPINGSTART, 0);
    amxd_trans_set_uint32_t(trans, CTHULHU_CONTAINER_UNPRIVILEGED_GIDMAPPINGSTART, 0);

    status = cthulhu_trans_send(&trans);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
    }

exit:
    return res;
}

int cthulhu_unpriv_create_user(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;

    char* username = NULL;
    char cmd[255] = {0};
    int exit_code = 0;

    CHECK_FALSE_LOG(INFO, cthulhu_ctr_is_unprivileged(ctr_id), res = 0; goto exit, "CTR[%s] container is privileged", ctr_id);

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit, "CTR[%s]: Could not get DM object", ctr_id);

    username = cthulhu_unpriv_get_username(ctr_id);
    ASSERT_STR_NOT_EMPTY(username, goto exit, "CTR[%s]: Could not get username", ctr_id);

    struct passwd* usrpwd = getpwnam(username);
    CHECK_NOT_NULL_LOG(INFO, usrpwd, res = 0; goto exit_success, "User [%s] already exists", username);

    snprintf(cmd, sizeof(cmd), "useradd -r %s", username);
    ASSERT_SUCCESS(cthulhu_proc_wait(cmd, &exit_code), goto exit, "Adding user [%s] failed", username);
    ASSERT_EQUAL(exit_code, 0, goto exit, "Adding user [%s] failed with code %d", username, exit_code);

exit_success:
    // add info to dm
    cthulhu_unpriv_store_user_in_dm(ctr_id, username);
    res = 0;
exit:
    free(username);
    return res;
}

int cthulhu_unpriv_remove_user(const char* ctr_id) {
    char cmd[255] = {0};
    char* username = NULL;
    int res = -1;
    int exit_code = 0;
    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);

    CHECK_FALSE_LOG(INFO, cthulhu_ctr_is_unprivileged(ctr_id), res = 0; goto exit, "CTR[%s] container is privileged", ctr_id);

    username = cthulhu_unpriv_get_username(ctr_id);
    ASSERT_STR_NOT_EMPTY(username, goto exit, "CTR[%s]: Could not get username", ctr_id);


    snprintf(cmd, sizeof(cmd), "userdel %s", username);
    ASSERT_SUCCESS(cthulhu_proc_wait(cmd, &exit_code), goto exit, "Removing user [%s] failed", username);
    ASSERT_EQUAL(exit_code, 0, goto exit, "Removing user [%s] failed with code %d", username, exit_code);

    ASSERT_SUCCESS(cthulhu_unpriv_remove_user_from_dm(ctr_id), goto exit);

    res = 0;
exit:
    free(username);
    return res;
}

/**
 * @brief this trigger is called when the layer is extracted.
 *
 * if idmapping is not supported, then the acl of the layer will be modified
 * so the container user has access to the layer
 *
 * @param layerdir location of the layer
 * @param uid
 * @param gid
 * @return int
 */
int cthulhu_unpriv_layer_post_extract(const char* layer_dir, uid_t uid, gid_t gid) {
    int res = 0;

    if(idmapping_is_supported == false) {
        SAH_TRACEZ_INFO(ME, "idmapping not supported: Trying Posix Acls (if supported by lower filesystem)");
        res = cthulhu_acl_set_recursive(layer_dir, uid, gid);
        CHECK_FAILURE_LOG(WARNING, res, NO_INSTRUCTION, "idmapping and Posix Acls not possible");
    }

    return res;
}

static int cthulhu_unpriv_get_usermapping_for_ctr(amxc_llist_t* usermappings, const char* ctr_id) {
    int res = -1;
    bool change_ownership = false;
    uid_t uid = 0;
    gid_t gid = 0;
    uid_t uid_map = 0;
    gid_t gid_map = 0;
    uint32_t range = 0;

    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &uid, &gid), goto exit);
    CHECK_FALSE_LOG(INFO, change_ownership, return 0, "Changing ownership is not requested.");

    ASSERT_SUCCESS(cthulhu_usermapping_data_add_to_list(usermappings, cthulhu_usermapping_user, 0, uid, 1), goto exit);
    ASSERT_SUCCESS(cthulhu_usermapping_data_add_to_list(usermappings, cthulhu_usermapping_group, 0, gid, 1), goto exit);

    cthulhu_unpriv_get_user_subids(ctr_id, &uid_map, &range);
    ASSERT_SUCCESS(cthulhu_usermapping_data_add_to_list(usermappings, cthulhu_usermapping_user, 1, uid_map, range), goto exit);
    cthulhu_unpriv_get_group_subids(ctr_id, &gid_map, &range);
    ASSERT_SUCCESS(cthulhu_usermapping_data_add_to_list(usermappings, cthulhu_usermapping_group, 1, gid_map, range), goto exit);

    res = 0;
exit:
    return res;
}

/**
 * @brief prepare the container layers for unprivileged access if needed and supported
 *
 * if unprivileged is requested, and idmapping is supported, then each layer will be
 * remounted with idmapping settings so the access rights of the files of the layers
 * are correct.
 *
 * @param ctr_id
 * @return int
 */
int cthulhu_unpriv_ctr_pre_mount(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* layers = NULL;
    bool change_ownership = false;
    amxc_llist_t usermappings;
    amxc_llist_init(&usermappings);

    CHECK_FALSE_LOG(INFO, cthulhu_overlayfs_supported(), res = 0; goto exit, "Overlayfs is not supported.");
    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, NULL, NULL), goto exit);
    CHECK_FALSE_LOG(INFO, change_ownership, res = 0; goto exit, "Changing ownership is not requested.");
    CHECK_FALSE_LOG(INFO, idmapping_is_supported, res = 0; goto exit, "Idmapping is not supported.");
    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    cthulhu_unpriv_get_usermapping_for_ctr(&usermappings, ctr_id);

    layers = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    ASSERT_NOT_NULL(layers, goto exit, "Could not find layers for container [%s]", ctr_id);
    amxd_object_for_each(instance, it, layers) {
        amxd_object_t* layer_obj = amxc_llist_it_get_data(it, amxd_object_t, it);

        char* mountedlayer = amxd_object_get_cstring_t(layer_obj, CTHULHU_CONTAINER_LAYERS_MOUNTEDLAYER, NULL);
        if(!string_is_empty(mountedlayer)) {
            SAH_TRACEZ_INFO(ME, "Layer is already mounted [%s]", mountedlayer);
            free(mountedlayer);
            continue;
        }
        free(mountedlayer);
        char* layer = amxd_object_get_cstring_t(layer_obj, CTHULHU_CONTAINER_LAYERS_LAYER, NULL);
        ASSERT_STR_NOT_EMPTY(layer, free(layer); goto exit);
        mountedlayer = cthulhu_idmap_layer(layer, ctr_id, &usermappings);
        ASSERT_STR_NOT_EMPTY(mountedlayer, free(layer); free(mountedlayer); goto exit, "Idmapping of layer [%s] failed", layer);
        amxd_status_t trans_status = amxd_object_set_trans(cstring_t, layer_obj, CTHULHU_CONTAINER_LAYERS_MOUNTEDLAYER, mountedlayer);
        free(layer);
        free(mountedlayer);
        ASSERT_SUCCESS(trans_status, goto exit, "Failed to save mountedlayer in the datamodel. status [%d]", trans_status);
    }
    res = 0;
exit:
    amxc_llist_clean(&usermappings, cthulhu_usermapping_data_it_free);
    return res;
}

int cthulhu_unpriv_ctr_post_umount(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* layers = NULL;

    CHECK_FALSE_LOG(INFO, cthulhu_overlayfs_supported(), res = 0; goto exit, "Overlayfs is not supported.");

    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);
    layers = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    ASSERT_NOT_NULL(layers, goto exit, "Could not find layers for container [%s]", ctr_id);
    amxd_object_for_each(instance, it, layers) {
        amxd_object_t* layer_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* mountedlayer = amxd_object_get_cstring_t(layer_obj, CTHULHU_CONTAINER_LAYERS_MOUNTEDLAYER, NULL);
        if(string_is_empty(mountedlayer)) {
            free(mountedlayer);
            continue;
        }
        if(umount(mountedlayer) != 0) {
            if(errno == EINVAL) {
                SAH_TRACEZ_INFO(ME, "Layer [%s] was not mounted", mountedlayer);
            }
            SAH_TRACEZ_WARNING(ME, "Layer [%s] could not be unmounted (%d: %s)", mountedlayer, errno, strerror(errno));
            free(mountedlayer);
            continue;
        }
        amxd_status_t trans_status = amxd_object_set_trans(cstring_t, layer_obj, CTHULHU_CONTAINER_LAYERS_MOUNTEDLAYER, "");
        if(trans_status) {
            SAH_TRACEZ_ERROR(ME, "Failed to clear mountedlayer in the datamodel. status [%d]", trans_status);
        }
        free(mountedlayer);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief remove acls from layers after container is removed
 *
 * @param ctr_id
 * @return int
 */
int cthulhu_unpriv_ctr_post_remove(const char* ctr_id) {
    int res = -1;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* layers = NULL;
    bool change_ownership = false;
    amxc_llist_t usermappings;
    cthulhu_app_t* app_data = NULL;
    uid_t uid = 0;
    gid_t gid = 0;

    amxc_llist_init(&usermappings);

    app_data = cthulhu_get_app_data();
    ASSERT_NOT_NULL(app_data, goto exit);

    ASSERT_SUCCESS(cthulhu_unpriv_get_settings(ctr_id, &change_ownership, &uid, &gid), goto exit);
    CHECK_FALSE_LOG(INFO, change_ownership, res = 0; goto exit, "Changing ownership is not requested.");
    if(idmapping_is_supported) {
        // no need to modify acls, since they are only set when idmapping is not supported
        res = 0;
        goto exit;
    }
    ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_obj, goto exit);

    layers = amxd_object_get(ctr_obj, CTHULHU_CONTAINER_LAYERS);
    ASSERT_NOT_NULL(layers, goto exit, "Could not find layers for container [%s]", ctr_id);
    amxd_object_for_each(instance, it, layers) {
        amxd_object_t* layer_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* layer = amxd_object_get_cstring_t(layer_obj, CTHULHU_CONTAINER_LAYERS_LAYER, NULL);
        ASSERT_STR_NOT_EMPTY(layer, free(layer); goto exit);
        char* layerdir = cthulhu_layer_get_dir_from_name(app_data->layer_location, layer);
        free(layer);
        ASSERT_STR_NOT_EMPTY(layerdir, free(layerdir); goto exit);
        ASSERT_SUCCESS(cthulhu_acl_remove_recursive(layerdir, uid, gid), , "Could not reset ACL for layer [%s]", layerdir);
        free(layerdir);
    }
    res = 0;
exit:
    amxc_llist_clean(&usermappings, cthulhu_usermapping_data_it_free);
    return res;
}

int cthulhu_chown_recursive(const char* filename, uid_t uid, gid_t gid) {
    int res = -1;
    DIR* dir = NULL;
    struct dirent* entry;
    char path[PATH_MAX] = {0};

    lcm_file_type_t type = lcm_filetype(filename);
    if((type == FILE_TYPE_SYMLINK) || (type == FILE_TYPE_UNKNOWN)) {
        res = 0;
        goto exit;
    }

    CHECK_FAILURE_LOG(WARNING, chown(filename, uid, gid), , "chown failed for file [%s]", filename);

    // recurse over all entries if the filename is a dir
    if(type == FILE_TYPE_DIRECTORY) {
        dir = opendir(filename);
        ASSERT_NOT_NULL(dir, goto exit, "Could not open %s (%d: %s)", filename, errno, strerror(errno));

        while((entry = readdir(dir))) {
            if((strcmp(entry->d_name, ".") == 0) ||
               (strcmp(entry->d_name, "..") == 0)) {
                continue;
            }
            int written = snprintf(path, sizeof(path), "%s/%s", filename, entry->d_name);
            if((written < 0) || (written >= (int) sizeof(path))) {
                SAH_TRACEZ_ERROR(ME, "Failed to construct full filename for %s, written: %d", filename, written);
                goto exit;
            }
            ASSERT_SUCCESS(cthulhu_chown_recursive(path, uid, gid), goto exit, "cthulhu_chown_recursive failed for path %s", path);
        }
    }

    res = 0;
exit:
    if(dir) {
        closedir(dir);
    }
    return res;
}
