/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "cthulhu_priv.h"

#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"

#define ME "rpc"

static void cthulhu_stats_fill(amxc_var_t* retval, cthulhu_resource_stats_t* stats) {
    amxc_var_t* var = NULL;

    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_TOTAL, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->total);
    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_USED, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->used);
    var = amxc_var_get_key(retval, CTHULHU_STATS_ELEM_FREE, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_int64_t(var, stats->free);
}

amxd_status_t _sb_stats_read(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_resource_stats_t cthulhu_stats;
    amxd_object_t* sb_obj = NULL;
    char* sb_id = NULL;

    cthulhu_stats.total = -1;
    cthulhu_stats.free = -1;
    cthulhu_stats.used = -1;

    status = amxd_action_object_read(object, param, reason, args, retval, priv);
    ASSERT_FALSE(status != amxd_status_ok && status != amxd_status_parameter_not_found, goto exit, "Status: %d", status);

    sb_obj = amxd_object_get_parent(amxd_object_get_parent(object));
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        goto exit;
    }
    if(cthulhu_sb_get_status_real(sb_obj) == cthulhu_sb_status_up) {
        sb_id = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_ID, NULL);

        const char* object_name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
        if(strcmp(object_name, CTHULHU_STATS_DISKSPACE) == 0) {
            cthulhu_sb_diskspace_resources_get(sb_id, &cthulhu_stats);
        } else if(strcmp(object_name, CTHULHU_STATS_MEMORY) == 0) {
            cthulhu_cgroup_memory_resources_get(sb_id, &cthulhu_stats);
        } else {
            SAH_TRACEZ_ERROR(ME, "Unknown stat [%s] ", object_name);
        }
    }
    cthulhu_stats_fill(retval, &cthulhu_stats);
exit:
    free(sb_id);
    return status;
}

amxd_status_t _ctr_stats_read(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_object_t* stat_obj = NULL;
    amxd_object_t* resource_obj = NULL;
    char* priv_sb_id = NULL;

    ctr_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object)));
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "Parent object is NULL");
        goto exit;
    }
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        goto exit;
    }
    sb_obj = cthulhu_sb_get(priv_sb_id);
    stat_obj = amxd_object_get_child(sb_obj, CTHULHU_STATS);
    resource_obj = amxd_object_get_child(stat_obj, amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    status = _sb_stats_read(resource_obj, param, reason, args, retval, priv);

exit:
    free(priv_sb_id);
    return status;
}

static int cthulhu_sb_modify_metadata_fill(amxc_var_t* sb_metadata, amxc_var_t* args) {
    const char* metadata_params[] = {
        CTHULHU_SANDBOX_AVAILABLE_ROLES
    };
    return cthulhu_metadata_fill(sb_metadata, args, metadata_params, cthulhu_metadata_count(metadata_params));
}

amxd_status_t _Sandbox_modify(UNUSED amxd_object_t* obj,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_object_t* sb_obj = NULL;
    amxd_trans_t* trans = NULL;
    bool modified_resource = false;

    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* sb_id = GET_CHAR(args, CTHULHU_SANDBOX_ID);
    amxc_var_t* var = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_modify, command_id);

    // check if there is already an Sandbox with this sb_id
    sb_obj = cthulhu_sb_get(sb_id);
    if(sb_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_unknown_exec_env, CTHULHU_SANDBOX " %s does not exist", sb_id);
        goto exit;
    }

    cthulhu_trans_new(&trans, sb_obj);
    if((var = GET_ARG(args, CTHULHU_SANDBOX_CPU)) != NULL) {
        int32_t cpu = amxc_var_dyncast(int32_t, var);
        modified_resource = true;
        if(cpu > 100) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Sandbox %s CPU should be between -1 and 100, not %d", sb_id, cpu);
            goto exit;
        }
        amxd_trans_set_value(int32_t, trans, CTHULHU_SANDBOX_CPU, cpu);
    }
    if((var = GET_ARG(args, CTHULHU_SANDBOX_MEMORY)) != NULL) {
        amxd_trans_set_value(int32_t, trans, CTHULHU_SANDBOX_MEMORY, amxc_var_dyncast(int32_t, var));
        modified_resource = true;
    }
    if((var = GET_ARG(args, CTHULHU_SANDBOX_DISKSPACE)) != NULL) {
        int32_t diskspace = amxc_var_dyncast(int32_t, var);
        int32_t cur_diskspace = amxd_object_get_int32_t(sb_obj, CTHULHU_SANDBOX_DISKSPACE, NULL);
        if((diskspace > 0) && (diskspace < SB_MIN_DISKSIZE)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Sandbox [%s] Diskspace should be bigger then %d KB", sb_id, SB_MIN_DISKSIZE);
            goto exit;
        }
        if(diskspace < cur_diskspace) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Sandbox [%s] Diskspace should be bigger then the current diskspace (Current: %d KB new: %d KB)", sb_id, cur_diskspace, diskspace);
            goto exit;
        }
        amxd_trans_set_value(int32_t, trans, CTHULHU_SANDBOX_DISKSPACE, diskspace);
        modified_resource = true;
    }

    {
        amxc_var_t* sb_metadata = NULL;
        amxc_var_new(&sb_metadata);
        amxc_var_set_type(sb_metadata, AMXC_VAR_ID_HTABLE);
        if(cthulhu_sb_modify_metadata_fill(sb_metadata, args) > 0) {
            if(cthulhu_plugin_sb_validate(args, ret) > 0) {
                int32_t error_type = GET_INT32(ret, CTHULHU_NOTIF_ERROR_TYPE);
                const char* reason = GET_CHAR(ret, CTHULHU_NOTIF_REASON);
                cthulhu_notif_error(notif_data, error_type, reason);
                status = amxd_status_invalid_arg;
            } else {
                cthulhu_plugin_sb_modify(sb_id, sb_metadata);
                status = amxd_status_ok;
            }
        }
        amxc_var_delete(&sb_metadata);
    }

    if(modified_resource) {
        status = cthulhu_trans_send(&trans);
        if(status != amxd_status_ok) {
            NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
            goto exit;
        }
    }

    cthulhu_notif_sb_updated(notif_data, NULL);
exit:
    amxd_trans_delete(&trans);
    cthulhu_notif_data_delete(&notif_data);
    return status;
}

amxd_status_t _Container_modify(UNUSED amxd_object_t* obj,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_notif_data_t* notif_data = NULL;
    amxd_object_t* ctr_obj = NULL;
    amxd_object_t* res_obj = NULL;
    amxd_trans_t* ctr_trans = NULL;
    amxd_trans_t* sb_trans = NULL;
    char* priv_sb_id = NULL;
    amxd_object_t* sb_obj = NULL;
    int32_t cpu = -1;
    int32_t diskspace = -1;

    const char* command_id = GET_CHAR(args, CTHULHU_NOTIF_COMMAND_ID);
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    amxc_var_t* var = NULL;

    cthulhu_notif_data_new(&notif_data, obj, cthulhu_notif_cmd_sb_modify, command_id);

    // check if there is already an Sandbox with this sb_id
    ctr_obj = cthulhu_ctr_get(ctr_id);
    if(ctr_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container %s does not exist", ctr_id);
        goto exit;
    }

    res_obj = amxd_object_get_child(ctr_obj, CTHULHU_CONTAINER_RESOURCES);
    if(res_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Could not get resources");
        goto exit;
    }

    cthulhu_trans_new(&ctr_trans, res_obj);
    cthulhu_trans_new(&sb_trans, sb_obj);
    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_CPU)) != NULL) {
        cpu = amxc_var_dyncast(int32_t, var);
        if(cpu > 100) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container [%s] CPU should be between 0 and 100, not %d", ctr_id, cpu);
            goto exit;
        }
        amxd_trans_set_value(int32_t, ctr_trans, CTHULHU_CONTAINER_RESOURCES_CPU, cpu);
    }

    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_MEMORY)) != NULL) {
        amxd_trans_set_value(int32_t, ctr_trans, CTHULHU_CONTAINER_RESOURCES_MEMORY, amxc_var_dyncast(int32_t, var));
    }
    if((var = GET_ARG(args, CTHULHU_CONTAINER_RESOURCES_DISKSPACE)) != NULL) {
        diskspace = amxc_var_dyncast(int32_t, var);
        if((diskspace > 0) && (diskspace < SB_MIN_DISKSIZE)) {
            NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Container [%s] Diskspace should be bigger then %d KB", ctr_id, SB_MIN_DISKSIZE);
            goto exit;
        }
        amxd_trans_set_value(int32_t, ctr_trans, CTHULHU_CONTAINER_RESOURCES_DISKSPACE, diskspace);
    }
    status = cthulhu_trans_send(&ctr_trans);
    if(status != amxd_status_ok) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
        goto exit;
    }


    // Apply also on the private sandbox
    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(priv_sb_id == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Associated sandbox missing");
        goto exit;
    }
    sb_obj = cthulhu_sb_get(priv_sb_id);
    if(sb_obj == NULL) {
        NOTIF_FAILED(notif_data, tr181_fault_invalid_arguments, "Associated sandbox missing");
        goto exit;
    }

    amxd_trans_set_value(int32_t, sb_trans, CTHULHU_CONTAINER_RESOURCES_CPU, cpu);
    amxd_trans_set_value(int32_t, sb_trans, CTHULHU_CONTAINER_RESOURCES_MEMORY, amxc_var_dyncast(int32_t, var));
    amxd_trans_set_value(int32_t, sb_trans, CTHULHU_CONTAINER_RESOURCES_DISKSPACE, diskspace);

    status = cthulhu_trans_send(&sb_trans);
    if(status != amxd_status_ok) {
        NOTIF_FAILED(notif_data, tr181_fault_request_denied, "Transaction failed. Status: %d", status);
        goto exit;
    }

exit:
    amxd_trans_delete(&ctr_trans);
    amxd_trans_delete(&sb_trans);
    cthulhu_notif_data_delete(&notif_data);
    amxc_var_set(bool, ret, (status == amxd_status_ok));
    return status;
}

amxd_status_t _appdata_utilization_read(amxd_object_t* object,
                                        UNUSED amxd_param_t* param,
                                        UNUSED amxd_action_t reason,
                                        UNUSED const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    cthulhu_resource_stats_t cthulhu_stats;
    char* mountpoint = NULL;
    uint32_t usage = 0;

    mountpoint = amxd_object_get_cstring_t(object, CTHULHU_SANDBOX_APPLICATIONDATA_MOUNTPOINT, NULL);
    if(!string_is_empty(mountpoint)) {
        if(cthulhu_storage_get_stats(mountpoint, &cthulhu_stats) == 0) {
            // TR-181 defines the usage as MiB, be we get it in KiB
            usage = cthulhu_stats.used / 1024;
        }
    }
    amxc_var_set_uint32_t(retval, usage);
    status = amxd_status_ok;
//exit:
    return status;
}
