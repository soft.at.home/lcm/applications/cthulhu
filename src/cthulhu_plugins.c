/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <string.h>
#include <dirent.h>

#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_defines_plugin.h>
#include "cthulhu_priv.h"
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>
#include <amxc/amxc_variant.h>
#include <lcm/amxc_data.h>

#define ME "plugins"

typedef void (* cthulhu_plugin_return_handler) (const char* const plugin, const char* const command, amxc_var_t* ret);

static amxc_llist_t plugins;

typedef struct _cthulhu_plugin_data {
    char* so_file;
    amxm_shared_object_t* plugin;
    amxc_llist_it_t lit;
} cthulhu_plugin_data_t;


static int cthulhu_plugin_data_init(cthulhu_plugin_data_t* const data) {
    int retval = -1;
    ASSERT_NOT_NULL(data, goto exit);
    ASSERT_SUCCESS(amxc_llist_it_init(&data->lit), goto exit);
    data->so_file = NULL;
    data->plugin = NULL;

    retval = 0;
exit:
    return retval;
}

static void cthulhu_plugin_data_clean(cthulhu_plugin_data_t* const data) {
    ASSERT_NOT_NULL(data, goto exit);
    free_null(data->so_file);
    data->plugin = NULL;
exit:
    return;
}

static int cthulhu_plugin_data_new(cthulhu_plugin_data_t** data) {
    int retval = -1;
    ASSERT_NOT_NULL(data, goto exit);

    *data = (cthulhu_plugin_data_t*) calloc(1, sizeof(cthulhu_plugin_data_t));
    ASSERT_NOT_NULL(*data, goto exit);

    if(cthulhu_plugin_data_init(*data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to init data");
        goto exit;
    }
    retval = 0;

exit:
    return retval;
}

static void cthulhu_plugin_data_delete(cthulhu_plugin_data_t** data) {
    ASSERT_NOT_NULL(data, goto exit);

    cthulhu_plugin_data_clean(*data);

    free_null(*data);
exit:
    return;
}

static void cthulhu_plugin_data_it_free(amxc_llist_it_t* it) {
    cthulhu_plugin_data_t* data = amxc_llist_it_get_data(it, cthulhu_plugin_data_t, lit);
    cthulhu_plugin_data_delete(&data);
}

static void cthulhu_plugin_exec(const char* command, amxc_var_t* args, cthulhu_plugin_return_handler return_handler) {
    cthulhu_plugin_data_t* data = NULL;
    amxc_llist_for_each(it, (&plugins)) {
        amxc_var_t ret;
        amxc_var_init(&ret);

        data = amxc_llist_it_get_data(it, cthulhu_plugin_data_t, lit);
        if(!amxm_so_has_function(data->plugin, CTHULHU_PLUGIN, command)) {
            continue;
        }
        if(amxm_so_execute_function(data->plugin, CTHULHU_PLUGIN, command, args, &ret) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute %s on %s", command, data->so_file);
        }
        if(return_handler) {
            return_handler(data->so_file, command, &ret);
        }
        amxc_var_clean(&ret);
    }
}

static void cthulhu_plugin_default_return_handler(const char* const plugin, const char* const command, amxc_var_t* ret) {
    int res = amxc_var_get_int32_t(ret);
    if(res < 0) {
        SAH_TRACEZ_WARNING(ME, "[%s] command [%s] returned %d", plugin, command, res);
    }
}

static void cthulhu_plugin_init_return_handler(UNUSED const char* const plugin, UNUSED const char* const command, amxc_var_t* ret) {
    const char* mib_general = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_GENERAL);
    const char* mib_container = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_CONTAINER);
    const char* mib_sandbox = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_SANDBOX);
    const char* mib_general_private = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_GENERAL_PRIVATE);
    const char* mib_container_private = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_CONTAINER_PRIVATE);
    const char* mib_sandbox_private = GET_CHAR(ret, CTHULHU_PLUGIN_ARG_MIB_SANDBOX_PRIVATE);

    if(mib_general) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM "." CTHULHU_PLUGINS);
        if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_general) < 0) {
            SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s]", plugin, mib_general);
        }
    }
    if(mib_general_private) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM "." CTHULHU_PLUGINS_PRIVATE);
        if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_general_private) < 0) {
            SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s]", plugin, mib_general_private);
        }
    }
    if(mib_container) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES "." CTHULHU_PLUGINS);
        if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_container) < 0) {
            SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s] on Container", plugin, mib_container);
            goto exit;
        }
    }
    if(mib_container_private) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES "." CTHULHU_PLUGINS_PRIVATE);
        if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_container_private) < 0) {
            SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s] on Container", plugin, mib_container_private);
            goto exit;
        }
    }
    if(mib_sandbox) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES "." CTHULHU_PLUGINS);
        if(!amxd_object_has_mib(plugin_obj, mib_sandbox)) {
            if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_sandbox) < 0) {
                SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s] on Sandbox", plugin, mib_sandbox);
                goto exit;
            }
        }
    }
    if(mib_sandbox_private) {
        amxd_object_t* plugin_obj = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES "." CTHULHU_PLUGINS_PRIVATE);
        if(!amxd_object_has_mib(plugin_obj, mib_sandbox_private)) {
            if(amxo_parser_apply_mib(cthulhu_get_parser(), plugin_obj, mib_sandbox_private) < 0) {
                SAH_TRACEZ_ERROR(ME, "[%s] failed to apply mib [%s] on Sandbox", plugin, mib_sandbox_private);
                goto exit;
            }
        }
    }


exit:
    return;

}

void cthulhu_plugin_init(void) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_data(&args, CTHULHU_PLUGIN_ARG_DATAMODEL, cthulhu_get_dm());
    amxc_var_add_new_key_data(&args, CTHULHU_PLUGIN_ARG_PRESTART_CONFIG, cthulhu_get_prestartup_config());

    cthulhu_plugin_exec(CTHULHU_PLUGIN_INIT, &args, cthulhu_plugin_init_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_cleanup(void) {
    amxc_var_t args;
    amxc_var_init(&args);
    cthulhu_plugin_exec(CTHULHU_PLUGIN_CLEANUP, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_odl_loaded(void) {
    amxc_var_t args;
    amxc_var_init(&args);
    cthulhu_plugin_exec(CTHULHU_PLUGIN_ODL_LOADED, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_create(const char* ctr_id, const amxc_var_t* data, image_spec_schema_image_manifest_schema* image_manifest, cthulhu_config_t* cthulhu_config) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    if(image_manifest) {
        amxc_var_add_new_key_data(&args, CTHULHU_PLUGIN_ARG_IMAGE_MANIFEST, image_manifest);
    }
    if(data) {
        amxc_var_t* var_data = amxc_var_add_new_key(&args, CTHULHU_PLUGIN_ARG_CREATE_DATA);
        amxc_var_copy(var_data, data);
    }
    if(cthulhu_config) {
        amxc_var_add_new_key_data(&args, CTHULHU_PLUGIN_ARG_CTHULHU_CONFIG, cthulhu_config);
    }

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_CREATE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_modify(const char* ctr_id, const amxc_var_t* data) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    if(data) {
        amxc_var_t* var_data = amxc_var_add_new_key(&args, CTHULHU_PLUGIN_ARG_CREATE_DATA);
        amxc_var_copy(var_data, data);
    }

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_MODIFY, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_postcreate(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_POSTCREATE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_prestart(const char* ctr_id, const char* rootfs, cthulhu_ctr_start_data_t* start_data) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_ROOTFS, rootfs);
    if(start_data) {
        amxc_var_add_new_key_data(&args, CTHULHU_PLUGIN_ARG_START_DATA, start_data);
    }

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_PRESTART, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_poststart(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_POSTSTART, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_statechanged(const char* ctr_id, cthulhu_ctr_status_t old_state, cthulhu_ctr_status_t new_state) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_OLD_STATE, cthulhu_ctr_status_to_string(old_state));
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_NEW_STATE, cthulhu_ctr_status_to_string(new_state));

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_STATECHANGED, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_prestop(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_PRESTOP, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_poststop(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_POSTSTOP, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_preremove(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_PREREMOVE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_ctr_postremove(const char* ctr_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_CTR_ID, ctr_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_CTR_POSTREMOVE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

int cthulhu_plugin_ctr_validate(amxc_var_t* argsvar,
                                amxc_var_t* retvar) {
    int ret = -1;
    cthulhu_plugin_data_t* data = NULL;

    amxc_llist_for_each(it, (&plugins)) {
        data = amxc_llist_it_get_data(it, cthulhu_plugin_data_t, lit);
        if(!amxm_so_has_function(data->plugin, CTHULHU_PLUGIN, CTHULHU_PLUGIN_CTR_VALIDATE)) {
            continue;
        }

        amxc_var_set_type(retvar, AMXC_VAR_ID_HTABLE);

        ret = amxm_so_execute_function(data->plugin, CTHULHU_PLUGIN, CTHULHU_PLUGIN_CTR_VALIDATE, argsvar, retvar);
        if(ret == 0) {
            SAH_TRACEZ_INFO(ME, "validated by '%s'", data->so_file);
            ret = 0;
        } else if(ret < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute " CTHULHU_PLUGIN_CTR_VALIDATE " on '%s'", data->so_file);
        } else {
            SAH_TRACEZ_WARNING(ME, "invalidated by '%s'", data->so_file);
            ret = +1;
            break;
        }
        amxc_var_clean(retvar);
    }

    return ret;
}

int cthulhu_plugin_sb_validate(amxc_var_t* argsvar,
                               amxc_var_t* retvar) {
    int ret = -1;
    cthulhu_plugin_data_t* data = NULL;

    amxc_llist_for_each(it, (&plugins)) {
        data = amxc_llist_it_get_data(it, cthulhu_plugin_data_t, lit);
        if(!amxm_so_has_function(data->plugin, CTHULHU_PLUGIN, CTHULHU_PLUGIN_SB_VALIDATE)) {
            continue;
        }

        amxc_var_set_type(retvar, AMXC_VAR_ID_HTABLE);

        ret = amxm_so_execute_function(data->plugin, CTHULHU_PLUGIN, CTHULHU_PLUGIN_SB_VALIDATE, argsvar, retvar);
        if(ret == 0) {
            SAH_TRACEZ_INFO(ME, "validated by '%s'", data->so_file);
            ret = 0;
        } else if(ret < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute " CTHULHU_PLUGIN_SB_VALIDATE " on '%s'", data->so_file);
        } else {
            SAH_TRACEZ_WARNING(ME, "invalidated by '%s'", data->so_file);
            ret = +1;
            break;
        }
        amxc_var_clean(retvar);
    }

    return ret;
}

void cthulhu_plugin_sb_create(const char* sb_id, const char* sb_host_data_dir, const amxc_var_t* data) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_HOST_DATA_DIR, sb_host_data_dir);

    if(data) {
        amxc_var_t* var_data = amxc_var_add_new_key(&args, CTHULHU_PLUGIN_ARG_CREATE_DATA);
        amxc_var_copy(var_data, data);
    }

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_CREATE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_modify(const char* sb_id, const amxc_var_t* data) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    if(data) {
        amxc_var_t* var_data = amxc_var_add_new_key(&args, CTHULHU_PLUGIN_ARG_CREATE_DATA);
        amxc_var_copy(var_data, data);
    }

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_MODIFY, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_prestart(const char* sb_id, const char* sb_host_data_dir) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_HOST_DATA_DIR, sb_host_data_dir);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_PRESTART, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_poststart(const char* sb_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_POSTSTART, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_statechanged(const char* sb_id, cthulhu_sb_status_t old_state, cthulhu_sb_status_t new_state) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_OLD_STATE, cthulhu_sb_status_to_string(old_state));
    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_NEW_STATE, cthulhu_sb_status_to_string(new_state));

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_STATECHANGED, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_prestop(const char* sb_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_PRESTOP, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_poststop(const char* sb_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_POSTSTOP, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_preremove(const char* sb_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_PREREMOVE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

void cthulhu_plugin_sb_postremove(const char* sb_id) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_PLUGIN_ARG_SB_ID, sb_id);

    cthulhu_plugin_exec(CTHULHU_PLUGIN_SB_POSTREMOVE, &args, cthulhu_plugin_default_return_handler);
    amxc_var_clean(&args);
}

static int cthulhu_plugin_load(const char* plugin_so, const char* plugin_fullpath) {
    int res = -1;
    cthulhu_plugin_data_t* plugin_data = NULL;
    SAH_TRACEZ_INFO(ME, "Load plugin [%s]", plugin_so);
    if(cthulhu_plugin_data_new(&plugin_data) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create plugin_data");
        goto error;
    }
    plugin_data->so_file = strdup(plugin_so);

    res = amxm_so_open(&plugin_data->plugin, plugin_so, plugin_fullpath);
    if(res) {
        const char* err_msg = amxm_so_error();
        SAH_TRACEZ_ERROR(ME, "Can not load plugin [%s]: (%d) %s", plugin_so, res, (err_msg ? err_msg : "???"));
        goto error;
    }

    if(amxm_so_get_module(plugin_data->plugin, CTHULHU_PLUGIN) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", CTHULHU_PLUGIN);
        amxm_so_close(&plugin_data->plugin);
        plugin_data->plugin = NULL;
        goto error;
    }
    amxc_llist_append(&plugins, &plugin_data->lit);

    res = 0;
    goto exit;
error:
    cthulhu_plugin_data_delete(&plugin_data);
exit:
    return res;
}

int cthulhu_plugins_init(void) {
    int res = -1;
    amxd_object_t* config = NULL;
    char* plugin_location = NULL;
    char* plugin_fullpath = NULL;
    char* mibs_dir = NULL;
    DIR* plugin_dir = NULL;
    struct dirent* entry;

    if(amxc_llist_init(&plugins) < 0) {
        goto exit;
    }

    config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_CONFIG);
    if(!config) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_DM_CONFIG " not found in the datamodel");
        goto exit;
    }
    plugin_location = amxd_object_get_cstring_t(config, CTHULHU_CONFIG_PLUGIN_LOCATION, NULL);
    if(string_is_empty(plugin_location)) {
        SAH_TRACEZ_ERROR(ME, "No plugin location is defined");
        goto exit;
    }
    if(!cthulhu_isdir(plugin_location)) {
        SAH_TRACEZ_INFO(ME, "Plugin dir [%s] does not exist", plugin_location);
        goto exit;
    }
    if(asprintf(&mibs_dir, "%s/mibs", plugin_location) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate memory");
    }
    if(cthulhu_isdir(mibs_dir)) {
        SAH_TRACEZ_INFO(ME, "Load plugin mibs");
        if(amxo_parser_scan_mib_dir(cthulhu_get_parser(), mibs_dir) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to parse mibs in %s", mibs_dir);
        }
    }

    if((plugin_dir = opendir(plugin_location)) == NULL) {
        SAH_TRACEZ_INFO(ME, "Plugin dir [%s] can not be opened", plugin_location);
        goto exit;
    }

    while((entry = readdir(plugin_dir))) {
        bool isfile = false;
        if((entry->d_name[0] == '.') && ((entry->d_name[1] == '\0') || ((entry->d_name[1] == '.') && (entry->d_name[2] == '\0')))) {
            continue;
        }
        if(asprintf(&plugin_fullpath, "%s/%s", plugin_location, entry->d_name) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate string");
            goto exit;
        }

        switch(entry->d_type) {
        case DT_LNK:
        case DT_UNKNOWN:
            if(cthulhu_isfile(plugin_fullpath)) {
                isfile = true;
            }
            break;
        case DT_REG:
            isfile = true;
            break;
        default:
            break;
        }

        if(isfile) {
            cthulhu_plugin_load(entry->d_name, plugin_fullpath);
        } else {
            SAH_TRACEZ_NOTICE(ME, "Plugin is not a file [%s]", plugin_fullpath);
        }

        free(plugin_fullpath);
    }

    // send init command to all plugins
    cthulhu_plugin_init();

    res = 0;
exit:
    if(plugin_dir) {
        closedir(plugin_dir);
    }
    free(plugin_location);
    free(mibs_dir);
    return res;
}

void cthulhu_plugins_cleanup(void) {
    cthulhu_plugin_data_t* data = NULL;
    // send init command to all plugins
    cthulhu_plugin_cleanup();

    // close all plugins
    amxc_llist_for_each(it, (&plugins)) {
        data = amxc_llist_it_get_data(it, cthulhu_plugin_data_t, lit);
        if(data->plugin) {
            amxm_so_close(&data->plugin);
            free_null(data->so_file);
            data->plugin = NULL;
        }
    }

    amxc_llist_clean(&plugins, cthulhu_plugin_data_it_free);
}
