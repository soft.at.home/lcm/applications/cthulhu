/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "cthulhu_priv.h"
#include "cthulhu_backend.h"
#include <cthulhu/cthulhu_defines.h>
#define ME "backend"

static amxm_module_t* mod_backend = NULL;

static int cthulhu_backend_cmd(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    amxc_var_t* original_args = amxc_var_get_key(args, "original_args", AMXC_VAR_FLAG_DEFAULT);
    const char* command = GET_CHAR(args, "command_name");

    return amxm_so_execute_function(cthulhu_backend_get(), MOD_CTHULHU, command, original_args, ret);
}

int cthulhu_backend_add_function(const char* func_name, amxm_callback_t cb) {
    int retval = -1;

    ASSERT_STR_NOT_EMPTY(func_name, return retval);
    ASSERT_NOT_NULL(cb, return retval);

    SAH_TRACEZ_INFO(ME, "Adding function: %s, %s, %s", AMXM_SELF, CTHULHU_MOD_BACKEND_COMMAND, func_name);
    retval = amxm_module_add_function(mod_backend, func_name, cb);
    ASSERT_SUCCESS(retval, return retval, "Couldn't add function: '%s'", func_name);

    return retval;
}

int cthulhu_backend_init_functions(void) {
    int retval = -1;

    amxm_shared_object_t* so = amxm_get_so(AMXM_SELF);
    ASSERT_NOT_NULL(so, return retval, "Cannot get " AMXM_SELF);

    retval = amxm_module_register(&mod_backend, so, CTHULHU_MOD_BACKEND);
    ASSERT_SUCCESS(retval, return retval, "Cannot register module");

    retval = cthulhu_backend_add_function(CTHULHU_MOD_BACKEND_COMMAND, cthulhu_backend_cmd);
    ASSERT_SUCCESS(retval, return retval);

    return 0;
}

