/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <cthulhu/cthulhu_helpers.h>
#include <linux/limits.h>
#include <locale.h>

#include <archive.h>
#include <archive_entry.h>

#include "cthulhu_priv.h"

#define ME "archive"

// code used from https://github.com/libarchive/libarchive/blob/master/examples/untar.c
static int cthulhu_copy_data(struct archive* ar, struct archive* aw) {
    int r;
    const void* buff;
    size_t size;
#if ARCHIVE_VERSION_NUMBER >= 3000000
    int64_t offset;
#else
    off_t offset;
#endif

    while(true) {
        r = archive_read_data_block(ar, &buff, &size, &offset);
        if(r == ARCHIVE_EOF) {
            return (ARCHIVE_OK);
        }
        if(r != ARCHIVE_OK) {
            return (r);
        }
        r = archive_write_data_block(aw, buff, size, offset);
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "archive_write_data_block() error: %s",
                             archive_error_string(aw));
            return (r);
        }
    }
}

// extract code borrowed from https://github.com/libarchive/libarchive/blob/master/examples/untar.c
static int cthulhu_extract(const char* filename, const char* out_location) {
    struct archive* a = NULL;
    struct archive* ext = NULL;
    struct archive_entry* entry;
    int r = ARCHIVE_FAILED;
    char out_path[PATH_MAX];
    // flag definitions:
    // https://www.freebsd.org/cgi/man.cgi?query=archive_write_disk&sektion=3&format=html
    int flags = ARCHIVE_EXTRACT_TIME |
        ARCHIVE_EXTRACT_PERM |
        ARCHIVE_EXTRACT_OWNER |
        ARCHIVE_EXTRACT_ACL |
        ARCHIVE_EXTRACT_FFLAGS |
        ARCHIVE_EXTRACT_SECURE_NODOTDOT |
        ARCHIVE_EXTRACT_SECURE_SYMLINKS |
        ARCHIVE_EXTRACT_XATTR;

    a = archive_read_new();
    ext = archive_write_disk_new();
    archive_write_disk_set_options(ext, flags);
    archive_read_support_filter_gzip(a);
    archive_read_support_format_tar(a);

    if((filename != NULL) && (strcmp(filename, "-") == 0)) {
        filename = NULL;
    }

    r = archive_read_open_filename(a, filename, 10240);
    if(r != 0) {
        SAH_TRACEZ_ERROR(ME, "archive_read_open_filename() error: %s", archive_error_string(a));
        goto exit;
    }
    while(true) {
        r = archive_read_next_header(a, &entry);
        if(r == ARCHIVE_EOF) {
            break;
        }
        if(r == ARCHIVE_WARN) {
            /* operation succeeded but a non-critical error was encountered => continue processing */
            SAH_TRACEZ_WARNING(ME, "archive_read_next_header() warning: %s", archive_error_string(a));
        } else if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "archive_read_next_header() error: %s", archive_error_string(a));
            goto exit;
        }

        sprintf(out_path, "%s/%s", out_location, archive_entry_pathname(entry));
        archive_entry_set_pathname(entry, out_path);
        /* A hardlink must point to a real file, so set its output directory too. */
        const char* original_hardlink = archive_entry_hardlink(entry);
        if(original_hardlink) {
            char new_hardlink[PATH_MAX];
            sprintf(new_hardlink, "%s/%s", out_location, original_hardlink);
            archive_entry_set_hardlink(entry, new_hardlink);
        }
        r = archive_write_header(ext, entry);
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME,
                             "archive_write_header() file: %s error: %s",
                             out_path, archive_error_string(ext));
            goto exit;
        } else {
            cthulhu_copy_data(a, ext);
            r = archive_write_finish_entry(ext);
            if(r != ARCHIVE_OK) {
                SAH_TRACEZ_ERROR(ME,
                                 "archive_write_finish_entry() file: %s error: %s",
                                 out_path, archive_error_string(ext));
            }
        }
    }
    r = ARCHIVE_OK;

exit:
    if(a) {
        archive_read_close(a);
        archive_read_free(a);
    }
    if(ext) {
        archive_write_close(ext);
        archive_write_free(ext);
    }
    return r;
}

int cthulhu_extract_bundle(const char* bundle_name, const char* storage_location, const char* bundle_location) {
    int retval = -1;
    amxc_string_t* extract_location = NULL;
    amxc_string_t* bundle_path = NULL;

    amxc_string_new(&extract_location, 0);
    amxc_string_setf(extract_location, "%s/%s", storage_location, bundle_name);

    amxc_string_new(&bundle_path, 0);
    amxc_string_setf(bundle_path, "%s/%s.tar", bundle_location, bundle_name);

    if(cthulhu_extract(bundle_path->buffer, extract_location->buffer) != 0) {
        SAH_TRACEZ_ERROR(ME,
                         "Failed to extract %s to %s",
                         bundle_location, extract_location->buffer);
        cthulhu_rmdir(extract_location->buffer);
        goto exit;
    }

    retval = 0;
exit:
    amxc_string_delete(&extract_location);
    amxc_string_delete(&bundle_path);
    return retval;
}

int cthulhu_extract_layer(const char* layer_digest,
                          const char* storage_location,
                          const char* layer_location) {
    int retval = -1;
    amxc_string_t* digest_file = NULL;
    amxc_string_t* layer_dir = NULL;

    ASSERT_STR_NOT_EMPTY(layer_digest, goto exit);
    ASSERT_STR_NOT_EMPTY(storage_location, goto exit);
    ASSERT_STR_NOT_EMPTY(layer_location, goto exit);

    ASSERT_TRUE(cthulhu_isdir(layer_location), goto exit, "Layer dir [%s] does not exist", layer_location)

    digest_file = cthulhu_get_digest_file(layer_digest, storage_location);
    ASSERT_NOT_NULL(digest_file, goto exit, "Failed to construct digest file string for %s", layer_digest);
    layer_dir = cthulhu_get_digest_file(layer_digest, layer_location);
    ASSERT_NOT_NULL(layer_dir, goto exit, "Failed to construct layer dir string for %s", layer_digest);

    SAH_TRACEZ_INFO(ME, "Extracting layer %s", digest_file->buffer);

    if(cthulhu_isdir(layer_dir->buffer)) {
        SAH_TRACEZ_INFO(ME, "Layer %s is already extracted", layer_dir->buffer);
        retval = 0;
        goto exit;
    }

    ASSERT_SUCCESS(cthulhu_mkdir(layer_dir->buffer, true), goto exit, "Failed to create dir [%s]", layer_dir->buffer);

    if(cthulhu_extract(digest_file->buffer, layer_dir->buffer) != 0) {
        SAH_TRACEZ_ERROR(ME,
                         "Failed to extract %s to %s",
                         digest_file->buffer, layer_dir->buffer);
        // delete the created directory
        cthulhu_rmdir(layer_dir->buffer);
        goto exit;
    }

    retval = 0;
exit:
    amxc_string_delete(&digest_file);
    amxc_string_delete(&layer_dir);

    return retval;
}

int cthulhu_extract_layer_to_rootfs(const char* layer_digest,
                                    const char* storage_location,
                                    const char* rootfs) {
    int retval = -1;
    amxc_string_t* digest_file = NULL;
    amxc_string_t* digest_hash = NULL;
    amxc_string_t output_location;

    amxc_string_init(&output_location, 0);

    ASSERT_STR_NOT_EMPTY(layer_digest, goto exit);
    ASSERT_STR_NOT_EMPTY(storage_location, goto exit);
    ASSERT_STR_NOT_EMPTY(rootfs, goto exit);

    digest_file = cthulhu_get_digest_file(layer_digest, storage_location);
    ASSERT_NOT_NULL(digest_file, goto exit, "Failed to construct digest file string for %s", layer_digest);
    digest_hash = cthulhu_get_digest_hash(layer_digest);
    ASSERT_NOT_NULL(digest_hash, goto exit, "Failed to get digest hash string for %s", layer_digest);


    SAH_TRACEZ_INFO(ME, "Extracting layer %s", digest_file->buffer);

    if(cthulhu_extract(digest_file->buffer, rootfs) != 0) {
        SAH_TRACEZ_ERROR(ME,
                         "Failed to extract %s to %s",
                         digest_file->buffer, rootfs);
        // delete the created directory
        cthulhu_rmdir(rootfs);
        goto exit;
    }

    retval = 0;
exit:
    amxc_string_delete(&digest_file);
    amxc_string_delete(&digest_hash);
    amxc_string_clean(&output_location);
    return retval;
}

