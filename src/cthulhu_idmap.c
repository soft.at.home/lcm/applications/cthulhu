/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/socket.h>
#include <linux/version.h>

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sched.h>

#include <cthulhu/cthulhu_helpers.h>

#include "cthulhu_priv.h"
#include "cthulhu_acl.h"


#define ME "idmap"

#ifdef SYS_mount_setattr // this define is needed to do idmapping
#include <linux/mount.h>

static inline long mount_setattr(int dirfd, const char* pathname, unsigned int flags,
                                 struct mount_attr* attr, size_t size) {
    return syscall(SYS_mount_setattr, dirfd, pathname, flags,
                   attr, size);
}
static inline long open_tree(int dirfd, const char* filename, unsigned int flags) {
    return syscall(SYS_open_tree, dirfd, filename, flags);
}
static inline long move_mount(int from_dirfd, const char* from_pathname,
                              int to_dirfd, const char* to_pathname, unsigned int flags) {
    return syscall(SYS_move_mount, from_dirfd, from_pathname,
                   to_dirfd, to_pathname, flags);
}

static inline int write_all(int fd, const void* buf, size_t count) {
    while(count) {
        ssize_t tmp;

        errno = 0;
        tmp = write(fd, buf, count);
        if(tmp > 0) {
            count -= tmp;
            if(count) {
                buf = (const void*) ((const char*) buf + tmp);
            }
        } else if((errno != EINTR) && (errno != EAGAIN)) {
            return -1;
        }
        if(errno == EAGAIN) {
            struct timespec ts;
            ts.tv_sec = 0;
            ts.tv_nsec = 250;
            nanosleep(&ts, &ts);
        }
    }
    return 0;
}

static inline ssize_t read_all(int fd, char* buf, size_t count) {
    ssize_t ret;
    ssize_t c = 0;
    int tries = 0;

    memset(buf, 0, count);
    while(count > 0) {
        ret = read(fd, buf, count);
        if(ret < 0) {
            if(((errno == EAGAIN) || (errno == EINTR)) && (tries++ < 5)) {
                struct timespec ts;
                ts.tv_sec = 0;
                ts.tv_nsec = 250;
                nanosleep(&ts, &ts);
                continue;
            }
            return c ? c : -1;
        }
        if(ret == 0) {
            return c;
        }
        tries = 0;
        count -= ret;
        buf += ret;
        c += ret;
    }
    return c;
}

/**
 * original code from https://github.com/util-linux/util-linux/blob/master/libmount/src/hook_idmap.c
 */
static int write_id_mapping(cthulhu_usermapping_type_t map_type, pid_t pid, const char* buf,
                            size_t buf_size) {
    int fd = -1, rc = -1, setgroups_fd = -1;
    char path[PATH_MAX];

    if((geteuid() != 0) && (map_type == cthulhu_usermapping_group)) {
        snprintf(path, sizeof(path), "/proc/%d/setgroups", pid);

        setgroups_fd = open(path, O_WRONLY | O_CLOEXEC | O_NOCTTY);
        if((setgroups_fd < 0) && (errno != ENOENT)) {
            goto exit;
        }

        if(setgroups_fd >= 0) {
            rc = write_all(setgroups_fd, "deny\n", strlen("deny\n"));
            if(rc) {
                goto exit;
            }
        }
    }

    snprintf(path, sizeof(path), "/proc/%d/%cid_map", pid,
             map_type == cthulhu_usermapping_user ? 'u' : 'g');
    fd = open(path, O_WRONLY | O_CLOEXEC | O_NOCTTY);
    if(fd < 0) {
        goto exit;
    }
    rc = write_all(fd, buf, buf_size);
exit:
    if(fd >= 0) {
        close(fd);
    }
    if(setgroups_fd >= 0) {
        close(setgroups_fd);
    }

    return rc;
}

/**
 * original code from https://github.com/util-linux/util-linux/blob/master/libmount/src/hook_idmap.c
 */
static int map_ids(amxc_llist_t* usermappings, pid_t pid) {
    int fill, left;
    char* pos;
    int rc = 0;
    char mapbuf[4096] = {};


    for(cthulhu_usermapping_type_t type = cthulhu_usermapping_user; type <= cthulhu_usermapping_group; type++) {
        bool had_entry = false;

        pos = mapbuf;
        amxc_llist_for_each(it, usermappings) {
            cthulhu_usermapping_data_t* usermapping_data = amxc_llist_it_get_data(it, cthulhu_usermapping_data_t, lit);
            if(usermapping_data->type != type) {
                continue;
            }
            had_entry = true;

            left = sizeof(mapbuf) - (pos - mapbuf);
            fill = snprintf(pos, left,
                            "%" PRIu32 " %" PRIu32 " %" PRIu32 "\n",
                            usermapping_data->ctr_id, usermapping_data->host_id, usermapping_data->range);
            /*
             * The kernel only takes <= 4k for writes to
             * /proc/<pid>/{g,u}id_map
             */
            if(fill <= 0) {
                return errno = EINVAL, -1;
            }

            pos += fill;
        }

        if(!had_entry) {
            continue;
        }

        rc = write_id_mapping(type, pid, mapbuf, pos - mapbuf);
        if(rc < 0) {
            return -1;
        }

        memset(mapbuf, 0, sizeof(mapbuf));
    }

    return 0;
}

/**
 * original code from https://github.com/util-linux/util-linux/blob/master/libmount/src/hook_idmap.c
 */
static int get_userns_fd(amxc_llist_t* usermappings) {
    int fd_userns = -1;
    ssize_t rc = -1;
    char c = '1';
    pid_t pid;
    int sock_fds[2];
    char path[PATH_MAX];

    rc = socketpair(PF_LOCAL, SOCK_STREAM | SOCK_CLOEXEC, 0, sock_fds);
    if(rc < 0) {
        return -errno;
    }

    pid = fork();
    ASSERT_FALSE(pid < 0, goto err_close_sock, "Unsharing user namespace failed");


    if(pid == 0) {
        close(sock_fds[1]);

        rc = unshare(CLONE_NEWUSER);
        if(rc < 0) {
            _exit(EXIT_FAILURE);
        }

        /* Let parent know we're ready to have the idmapping written. */
        rc = write_all(sock_fds[0], &c, 1);
        if(rc) {
            _exit(EXIT_FAILURE);
        }

        /* Hang around until the parent has persisted our namespace. */
        rc = read_all(sock_fds[0], &c, 1);
        if(rc != 1) {
            _exit(EXIT_FAILURE);
        }

        close(sock_fds[0]);
        _exit(EXIT_SUCCESS);
    }
    close(sock_fds[0]);
    sock_fds[0] = -1;

    /* Wait for child to set up a new namespace. */
    rc = read_all(sock_fds[1], &c, 1);
    if(rc != 1) {
        SAH_TRACEZ_ERROR(ME, "Unsharing user namespace failed");
        kill(pid, SIGKILL);
        goto err_wait;
    }

    rc = map_ids(usermappings, pid);
    if(rc < 0) {
        SAH_TRACEZ_ERROR(ME, "Mapping ids failed");
        kill(pid, SIGKILL);
        goto err_wait;
    }

    snprintf(path, sizeof(path), "/proc/%d/ns/user", pid);
    fd_userns = open(path, O_RDONLY | O_CLOEXEC | O_NOCTTY);
    ASSERT_TRUE(fd_userns > 0, , "Failed to open %s", path);

    /* Let child know we've persisted its namespace. */
    (void) write_all(sock_fds[1], &c, 1);

err_wait:
    rc = cthulhu_wait_for_pid(pid);

err_close_sock:
    if(sock_fds[0] > 0) {
        close(sock_fds[0]);
    }
    close(sock_fds[1]);

    if((rc < 0) && (fd_userns >= 0)) {
        close(fd_userns);
        fd_userns = -1;
    }

    return fd_userns;
}

/**
 * @brief idmap a directory
 *
 * @param src source directory
 * @param dest destination directory
 * @param usermappings idmapping that should be done. a list of cthulhu_usermapping_data_t
 * @return int
 */
int cthulhu_idmap(char* src, char* dest, amxc_llist_t* usermappings) {
    int res = -1;
    long ret = -1;
    int fd_tree = 0;
    int userns_fd = 0;
    SAH_TRACEZ_INFO(ME, "src: %s dest %s", src, dest);

    ret = open_tree(-EBADF, src,
                    OPEN_TREE_CLONE |
                    OPEN_TREE_CLOEXEC |
                    AT_EMPTY_PATH);
    ASSERT_FALSE(ret <= 0, goto exit, "Failed to open_tree %s", src);
    fd_tree = (int) ret;

    if(!amxc_llist_is_empty(usermappings)) {
        userns_fd = get_userns_fd(usermappings);
        ASSERT_TRUE(userns_fd > 0, goto exit, "Could not get userns_fd");

        struct mount_attr attr = {
            .attr_set = MOUNT_ATTR_IDMAP,
        };
        attr.userns_fd = userns_fd;
        ASSERT_SUCCESS(mount_setattr(fd_tree, "", AT_EMPTY_PATH | AT_RECURSIVE, &attr, sizeof(attr)),
                       goto exit, "failed to set idmap to mount (%d: %s)", errno, strerror(errno));

    }

    ASSERT_SUCCESS(move_mount(fd_tree, "", -EBADF, dest, MOVE_MOUNT_F_EMPTY_PATH), goto exit, "failed to move mount to %s", dest);

    res = 0;
exit:
    if(fd_tree > 0) {
        close(fd_tree);
    }
    if(userns_fd > 0) {
        close(userns_fd);
    }
    return res;
}

/**
 * @brief helper to idmap a layer of a certain container
 *
 * a directory will be made under <layer_location>/idmap/<ctr_id>/<layer>
 *
 * @param layer
 * @param ctr_id
 * @param usermappings
 * @return the layer dir, or NULL if failed. return string should be freed.
 */
char* cthulhu_idmap_layer(const char* layer, const char* ctr_id, amxc_llist_t* usermappings) {
    char* src = NULL;
    char* dest_base = NULL;
    char* dest = NULL;
    cthulhu_app_t* app_data = cthulhu_get_app_data();
    ASSERT_NOT_NULL(app_data, goto exit);
    src = cthulhu_layer_get_dir_from_name(app_data->layer_location, layer);
    ASSERT_NOT_NULL(src, goto exit, "Could not create layer dir string");
    ASSERT_TRUE(cthulhu_isdir(src), goto exit, "Layer does not exist [%s]", src);
    if(asprintf(&dest_base, "%s/idmap/%s", app_data->layer_location, ctr_id) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to assign dest_base string");
        goto exit;
    }
    dest = cthulhu_layer_get_dir_from_name(dest_base, layer);
    cthulhu_mkdir(dest, true);

    int res = cthulhu_idmap(src, dest, usermappings);
    if(res < 0) {
        SAH_TRACEZ_ERROR(ME, "Idmapping of %s failed", src);
        free_null(dest);
    }

exit:
    free(src);
    free(dest_base);
    return dest;
}

/**
 * @brief checks if idmapping is supported on this board, depending on defines and kernel version
 *
 * @param with_overlayfs
 * @return int
 */
int cthulhu_idmap_supported(bool with_overlayfs) {
    // overlayfs supported since 5.19
    // https://man.archlinux.org/man/mount_setattr.2.en#NOTES
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 19, 0)
    static bool overlayfs_supported = false;
#else
    static bool overlayfs_supported = true;
#endif
    if(with_overlayfs) {
        return overlayfs_supported;
    }
    return true;
}

#else // SYS_mount_setattr
int cthulhu_idmap(UNUSED char* src, UNUSED char* dest, UNUSED amxc_llist_t* usermappings) {
    SAH_TRACEZ_WARNING(ME, "idmapping is not supported on this kernel");
    return -1;
}
int cthulhu_idmap_supported(UNUSED bool with_overlayfs) {
    return false;
}

char* cthulhu_idmap_layer(UNUSED const char* layer, UNUSED const char* ctr_id, UNUSED amxc_llist_t* usermappings) {
    return NULL;
}
#endif // SYS_mount_setattr

