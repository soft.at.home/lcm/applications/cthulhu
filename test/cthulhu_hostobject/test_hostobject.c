/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>
#include <lcm/lcm_assert.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_hostobject.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

const char* sb_name = "sb_for_ctr";


static int hostobjects_add(amxc_llist_t* hostobjects, const char* source, const char* destintation, const char* options) {
    int res = -1;
    amxc_var_t* hostobject = NULL;

    ASSERT_NOT_NULL(hostobjects, goto exit);


    ASSERT_SUCCESS(amxc_var_new(&hostobject), goto exit);
    ASSERT_SUCCESS(amxc_var_set_type(hostobject, AMXC_VAR_ID_HTABLE), goto exit);

    if(source) {
        amxc_var_add_new_key_cstring_t(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_SOURCE, source);
    }
    if(destintation) {
        amxc_var_add_new_key_cstring_t(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_DESTINATION, destintation);
    }
    if(options) {
        amxc_var_add_new_key_cstring_t(hostobject, CTHULHU_CONTAINER_HOSTOBJECT_OPTIONS, options);
    }
    ASSERT_SUCCESS(amxc_llist_append(hostobjects, &(hostobject)->lit), goto exit);

    res = 0;
exit:
    return res;
}

static amxd_object_t* test_get_mount_objects(const char* ctr_id) {
    amxd_object_t* ctr_obj = testhelper_get_ctr(ctr_id);
    assert_non_null(ctr_obj);
    return amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_MOUNTS);
}

static bool test_ctr_contains_mount(const char* ctr_id, const char* source, const char* destination, const char* type, const char* options) {
    bool res = false;
    amxd_object_t* mounts = test_get_mount_objects(ctr_id);
    amxd_object_for_each(instance, it_mnt, mounts) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_mnt, amxd_object_t, it);
        char* inst_source = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_SOURCE, NULL);
        char* inst_destination = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_DESTINATION, NULL);
        char* inst_type = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_TYPE, NULL);
        char* inst_options = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_MOUNTS_OPTIONS, NULL);

        printf("Source: %s Dest: %s Type: %s Options: %s \n", inst_source, inst_destination, inst_type, inst_options);
        if((!source || (strcmp(source, inst_source) == 0)) &&
           (!destination || (strcmp(destination, inst_destination) == 0)) &&
           (!type || (strcmp(type, inst_type) == 0)) &&
           (!options || (strcmp(options, inst_options) == 0))) {
            res = true;
            free(inst_source);
            free(inst_destination);
            free(inst_type);
            free(inst_options);
            break;
        }
        free(inst_source);
        free(inst_destination);
        free(inst_type);
        free(inst_options);
    }
    return res;
}

static amxd_object_t* test_get_device_objects(const char* ctr_id) {
    amxd_object_t* ctr_obj = testhelper_get_ctr(ctr_id);
    assert_non_null(ctr_obj);
    char* priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    assert_non_null(priv_sb_id);
    amxd_object_t* sb_obj = cthulhu_sb_get(priv_sb_id);
    assert_non_null(sb_obj);
    free(priv_sb_id);
    return amxd_object_findf(sb_obj, CTHULHU_SANDBOX_DEVICES);
}

static bool test_ctr_contains_device(const char* ctr_id,
                                     const char* device,
                                     const char* devicetype,
                                     const char* access,
                                     uint32_t major,
                                     uint32_t minor,
                                     bool create) {
    bool res = false;
    amxd_object_t* devices = test_get_device_objects(ctr_id);
    amxd_object_for_each(instance, it_dev, devices) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_dev, amxd_object_t, it);
        char* inst_device = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_DEVICE, NULL);
        char* inst_devicetype = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_TYPE, NULL);
        uint32_t inst_major = amxd_object_get_uint32_t(instance, CTHULHU_SANDBOX_DEVICES_MAJOR, NULL);
        uint32_t inst_minor = amxd_object_get_uint32_t(instance, CTHULHU_SANDBOX_DEVICES_MINOR, NULL);
        char* inst_access = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_DEVICES_ACCESS, NULL);
        bool inst_create = amxd_object_get_bool(instance, CTHULHU_SANDBOX_DEVICES_CREATE, NULL);


        printf("Device: %s Type: %s Permission: %s, Major: %u Minor: %u, Create: %d\n",
               inst_device, inst_devicetype, inst_access, inst_major, inst_minor, inst_create);
        if(((strcmp(device, inst_device) == 0)) &&
           ((strcmp(devicetype, inst_devicetype) == 0)) &&
           ((strcmp(access, inst_access) == 0)) &&
           (inst_major == major) &&
           (inst_minor == minor) &&
           (inst_create == create)) {
            res = true;
            free(inst_device);
            free(inst_devicetype);
            free(inst_access);
            break;
        }
        free(inst_device);
        free(inst_devicetype);
        free(inst_access);
    }
    return res;
}


int test_cthulhu_ho_setup (UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_ho_teardown (UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown();
    LOG_EXIT
}

void test_cthulhu_ho_options (UNUSED void** state) {
    LOG_ENTRY
    amxc_var_t* options_var = NULL;

    // test NULL
    options_var = cthulhu_ctr_options_string_to_var(NULL);
    assert_non_null(options_var);
    assert_int_equal(amxc_var_type_of(options_var), AMXC_VAR_ID_HTABLE);
    amxc_var_delete(&options_var);

    // test good options
    options_var = cthulhu_ctr_options_string_to_var("type=mount,rw,nodev,nosuid,noatime,create=dir");
    assert_non_null(options_var);
    assert_int_equal(amxc_var_type_of(options_var), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(options_var, "type"), "mount");
    assert_string_equal(GET_CHAR(options_var, "create"), "dir");
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "rw")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "nodev")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "nosuid")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "noatime")), AMXC_VAR_ID_NULL);
    amxc_var_delete(&options_var);

    // test good options with spaces
    options_var = cthulhu_ctr_options_string_to_var("  type = mount  ,rw,nodev   ,   nosuid ,  noatime, create  =  dir   ");
    assert_non_null(options_var);
    assert_int_equal(amxc_var_type_of(options_var), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(options_var, "type"), "mount");
    assert_string_equal(GET_CHAR(options_var, "create"), "dir");
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "rw")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "nodev")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "nosuid")), AMXC_VAR_ID_NULL);
    assert_int_equal(amxc_var_type_of(GET_ARG(options_var, "noatime")), AMXC_VAR_ID_NULL);

    // convert var to string
    amxc_string_t* options_str = cthulhu_ctr_options_var_to_string(options_var);
    assert_string_equal(options_str->buffer, "type=mount,noatime,rw,create=dir,nosuid,nodev");
    amxc_var_delete(&options_var);
    amxc_string_delete(&options_str);
    LOG_EXIT
}

void test_cthulhu_ho_mount_invalid (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    // hostobject is not a list
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_HOSTOBJECT, "not_a_list");
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_var_clean(&args);
    // hostobject does not contain source
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, NULL, "/tmp/destination", "type=mount"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain destination
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "/tmp/source", NULL, "type=mount"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain options
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "/tmp/source", "/tmp/destination", NULL), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    // hostobject does not contain correct type
    // amxc_var_init(&args);
    // amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    // amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    // amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    // amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    // amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    // amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    // assert_int_equal(amxc_llist_new(&hostobjects), 0);
    // assert_int_equal(hostobjects_add(hostobjects, "/tmp/source", "/tmp/destination", "type=unknown"), 0);

    // amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    // assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    // amxc_llist_delete(&hostobjects, variant_list_it_free);
    // amxc_var_clean(&args);

    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_ho_mount_default_options (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "/tmp/source", "/tmp/destination", "type=mount"), 0);
    assert_int_equal(hostobjects_add(hostobjects, "tmpfs", "/tmp/tmpfs", "type=mount"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_SOURCE, "/tmp/source", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/destination", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_TYPE, "none", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "bind,ro,nodev,nosuid,relatime,noexec", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_SOURCE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_TYPE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "rw,nodev,nosuid,relatime", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // defaults
    assert_true(test_ctr_contains_mount(ctr_id, "tmpfs", "tmp", "tmpfs", "rw,nodev,nosuid,noatime,create=dir"));
    assert_true(test_ctr_contains_mount(ctr_id, "tmpfs", "run", "tmpfs", "rw,nodev,nosuid,noatime,create=dir"));
    // newly added
    assert_true(test_ctr_contains_mount(ctr_id, "/tmp/source", "/tmp/destination", "none", "bind,ro,nodev,nosuid,relatime,noexec"));
    assert_true(test_ctr_contains_mount(ctr_id, "tmpfs", "/tmp/tmpfs", "tmpfs", "rw,nodev,nosuid,relatime"));

    testhelper_remove_ctr(ctr_id, true);
    LOG_EXIT
}

void test_cthulhu_ho_mount_custom_options (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "/tmp/source", "/tmp/destination", "type=mount,option1"), 0);
    assert_int_equal(hostobjects_add(hostobjects, "tmpfs", "/tmp/tmpfs", "type=mount,option2"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_SOURCE, "/tmp/source", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/destination", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_TYPE, "none", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "option1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_SOURCE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_TYPE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "option2", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_true(test_ctr_contains_mount(ctr_id, "/tmp/source", "/tmp/destination", "none", "option1"));
    assert_true(test_ctr_contains_mount(ctr_id, "tmpfs", "/tmp/tmpfs", "tmpfs", "option2"));
    LOG_EXIT
}

void test_cthulhu_ho_mount_update (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_SOURCE, "/tmp/source", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/destination", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_TYPE, "none", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".5."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "option1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_SOURCE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_DESTINATION, "/tmp/tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_TYPE, "tmpfs", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_MOUNTS ".6."CTHULHU_CONTAINER_MOUNTS_OPTIONS, "option2", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_true(test_ctr_contains_mount(ctr_id, "/tmp/source", "/tmp/destination", "none", "option1"));
    assert_true(test_ctr_contains_mount(ctr_id, "tmpfs", "/tmp/tmpfs", "tmpfs", "option2"));
    LOG_EXIT
}

void test_cthulhu_ho_mount_remove (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    assert_int_equal(amxc_llist_new(&hostobjects), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&hostobjects, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_false(test_ctr_contains_mount(ctr_id, "/tmp/source", "/tmp/destination", "none", "option1"));
    assert_false(test_ctr_contains_mount(ctr_id, "tmpfs", "/tmp/tmpfs", "tmpfs", "option2"));

    testhelper_remove_ctr(ctr_id, true);
    LOG_EXIT
}

void test_cthulhu_ho_device (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "", "/dev/tty60", "type=device,devicetype=c,access=rwm,major=4,minor=60,create=1"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // check existence of devices
    assert_true(test_ctr_contains_device(ctr_id, "/dev/tty60", "c", "rwm", 4, 60, true));

    testhelper_remove_ctr(ctr_id, true);
    LOG_EXIT
}

void test_cthulhu_ho_device_wrong_options (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_hostobject";
    amxc_llist_t* hostobjects = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&hostobjects), 0);
    assert_int_equal(hostobjects_add(hostobjects, "", "/dev/tty60", "type=device,devicetype=c,access=rwm,major=4,create=1"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_HOSTOBJECT, hostobjects);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&hostobjects, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // check existence of devices
    assert_false(test_ctr_contains_device(ctr_id, "/dev/tty60", "c", "rwm", 4, 60, true));

    testhelper_remove_ctr(ctr_id, true);
    LOG_EXIT
}
