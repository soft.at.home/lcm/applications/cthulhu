/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"
#include "test_issues.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

const char* sb_name = "sb_for_ctr";

int test_cthulhu_issues_setup (UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_issues_teardown (UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown();
    LOG_EXIT
}

void test_cthulhu_lcm_626 (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t value;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&value);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_STOP, &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);

    // wait 5s and eat all signals
    expected_sig_init(&expected_sig, "lalala", false, false);
    expected_sig_wait(&expected_sig, 5);
    expected_sig_clean(&expected_sig);

    amxd_object_t* ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES ".1");
    assert_non_null(ctr);

    assert_int_equal(amxd_object_get_param(ctr, CTHULHU_CONTAINER_STATUS, &value), amxd_status_ok);
    assert_string_equal(amxc_var_get_const_cstring_t(&value), CTHULHU_CONTAINER_STATUS_STOPPED);



    amxc_var_clean(&value);
    amxc_var_clean(&ret);
    LOG_EXIT
}
