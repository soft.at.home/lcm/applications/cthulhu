/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_ctr.h"
#include "expected_signal.h"
#include "test_setup.h"
#include "cthulhu_module.h"
#include "cthulhu_cgroup.h"

#undef ME
#define ME "test"

const char* sb_name = "sb_for_ctr";
static bool execute_deferred_done = false;
static int sig_eat_timeout = 3;

int test_cthulhu_ctr_setup(UNUSED void** state) {
    LOG_ENTRY
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_ctr_teardown(UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    expected_sig_eat(sig_eat_timeout);
    return test_cthulhu_teardown();
    LOG_EXIT
}

void test_cthulhu_create_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_create_ctr_cmd(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";
    char* command_id = "cid_1";

    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_CTR_CREATE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(params, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_start_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

static void test_deferred_execution_returns(const amxc_var_t* const data,
                                            void* const priv) {
    LOG_ENTRY
    amxc_var_t* ret = (amxc_var_t*) priv;
    assert_non_null(data);
    assert_non_null(ret);

    amxc_var_copy(ret, data);
    execute_deferred_done = true;
    LOG_EXIT
}

static void handle_events(void) {
    int cnt = 1;
    printf("Waiting for CTR execution RPC to terminate \n");
    while(amxp_signal_read() == 0 || execute_deferred_done == false) {
        cnt % 5 == 0 ? printf("*") : printf(".");
        cnt++;
        usleep(100 * 1000);
    }
    printf("\n");
}

static void loop_ready(void) {
    fd_set read_fds;
    struct timeval timeout;
    int cnt = 1;

    // get last added amxo listener (added by liblcm worker)
    amxc_llist_t* listeners = amxo_parser_get_listeners(cthulhu_get_parser());
    amxo_connection_t* inst = NULL;
    inst = amxc_llist_it_get_data(amxc_llist_get_last(listeners), amxo_connection_t, it);

    while(1) {
        FD_ZERO(&read_fds);
        FD_SET(inst->fd, &read_fds);

        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;

        int ready = select(inst->fd + 1, &read_fds, NULL, NULL, &timeout);
        if(ready > 0) {
            // data available in the pipe
            //printf("Data is available on the file descriptor.\n");
            break;
        } else if(ready == 0) {
            if(cnt % 3 == 0) {
                printf(".");
            }
            if(cnt % 20 == 0) {
                printf("*");
            }
            cnt++;
        }
    }

    // trigger the callback
    inst->reader(inst->fd, inst->priv);
}

static void test_cthulhu_rpc_execute_config(amxd_object_t* dm_ctr, const char* ctr_id, const char* cmd,
                                            const char* cmd_args, uint32_t timeout, amxd_status_t expected_status,
                                            const char* expected_retval) {
    LOG_ENTRY
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t exec_ret;
    uint64_t call_id = 0;
    amxc_var_t args_var;

    amxc_var_init(&args_var);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&exec_ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CMD_CTR_EXECUTE_CMD, cmd);
    if(timeout) {
        amxc_var_add_key(uint32_t, &args, CTHULHU_CMD_CTR_EXECUTE_TIMEOUT, timeout);
    }

    amxc_var_set(cstring_t, &args_var, cmd_args);
    amxc_llist_t* args_list = amxc_var_dyncast(amxc_llist_t, &args_var);
    amxc_var_add_key(amxc_llist_t, &args, CTHULHU_CMD_CTR_EXECUTE_ARGUMENTS, args_list);
    amxc_llist_delete(&args_list, variant_list_it_free);

    execute_deferred_done = false;
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_EXECUTE, &args, &ret), amxd_status_deferred);
    call_id = amxc_var_constcast(uint64_t, &ret);
    assert_int_equal(amxd_function_set_deferred_cb(call_id, test_deferred_execution_returns, &exec_ret), amxd_status_ok);

    loop_ready();
    handle_events();

    assert_int_equal(GET_UINT32(&exec_ret, "status"), expected_status);
    if(expected_retval) {
        assert_string_equal(GET_CHAR(&exec_ret, "retval"), expected_retval);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&exec_ret);
    amxc_var_clean(&args_var);
    LOG_EXIT
}

void test_cthulhu_rpc_execute(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    const char* ctr_id = "ctr_1";
    const char* fake_ctr_id = "fake_id";
    const char* cmd_args = "3, 2";
    const char* cmd_valid = "/bin/sleep";
    const char* cmd_invalid = "/bin/invalid_sleep";

    assert_non_null(dm_ctr);

    // Check non-existing container
    test_cthulhu_rpc_execute_config(dm_ctr, fake_ctr_id, cmd_valid, cmd_args, 4,
                                    amxd_status_unknown_error, "CONTAINER_NOT_FOUND");

    // Check command not exceeding timeout (cmd 5 seconds, timeout 10)
    test_cthulhu_rpc_execute_config(dm_ctr, ctr_id, cmd_valid, cmd_args, 10,
                                    amxd_status_ok, "CONTAINER_CMD_OK");

    // Check command exceeding timeout (cmd 5 seconds, timeout 2)
    test_cthulhu_rpc_execute_config(dm_ctr, ctr_id, cmd_valid, cmd_args, 2,
                                    amxd_status_unknown_error, "CONTAINER_CMD_TIMEOUT");

    // Check command not exceeding default timeoutt (cmd 5 seconds, default timeout 60)
    test_cthulhu_rpc_execute_config(dm_ctr, ctr_id, cmd_valid, cmd_args, 0,
                                    amxd_status_ok, "CONTAINER_CMD_OK");

    // Check invalid command
    test_cthulhu_rpc_execute_config(dm_ctr, ctr_id, cmd_invalid, cmd_args, 5,
                                    amxd_status_unknown_error, "CONTAINER_CMD_KO");
    LOG_EXIT
}

static void test_cthulhu_backend_command_execute_config(const char* ctr_id, const char* cmd,
                                                        amxc_var_t* cmd_args, uint32_t timeout,
                                                        int expected_status, const char* expected_retval) {
    LOG_ENTRY
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_COMMAND, cmd);
    amxc_var_set_key(&args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_ARGUMENTS, cmd_args, AMXC_VAR_FLAG_COPY);
    if(timeout) {
        amxc_var_add_key(uint32_t, &args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT, timeout);
    }
    assert_int_equal(cthulhu_backend_command(CTHULHU_BACKEND_CMD_CTR_EXECUTE, &args, &ret), expected_status);
    assert_string_equal(GET_CHAR(&ret, NULL), expected_retval);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_backend_command_execute(UNUSED void** state) {
    LOG_ENTRY
    amxc_var_t arguments;
    amxc_var_t* arg = NULL;

    const char* ctr_id = "ctr_1";
    const char* cmd_valid = "/bin/sleep";
    const char* cmd_invalid = "/bin/invalid_sleep";
    const char* cmd_hang = "/bin/hang";

    amxc_var_init(&arguments);
    amxc_var_set_type(&arguments, AMXC_VAR_ID_LIST);
    arg = amxc_var_add(amxc_llist_t, &arguments, NULL);
    amxc_var_set_type(arg, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, arg, "2");
    arg = amxc_var_add(amxc_llist_t, &arguments, NULL);
    amxc_var_set_type(arg, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, arg, "3");

    test_cthulhu_backend_command_execute_config("fake_id", cmd_valid, &arguments, 2, 2, "CONTAINER_NOT_FOUND");
    test_cthulhu_backend_command_execute_config(ctr_id, cmd_valid, &arguments, 7, 0, "CONTAINER_CMD_OK");
    test_cthulhu_backend_command_execute_config(ctr_id, cmd_valid, &arguments, 2, 1, "CONTAINER_CMD_TIMEOUT");
    test_cthulhu_backend_command_execute_config(ctr_id, cmd_valid, &arguments, 0, 0, "CONTAINER_CMD_OK");
    test_cthulhu_backend_command_execute_config(ctr_id, cmd_hang, &arguments, 0, 1, "CONTAINER_CMD_TIMEOUT");
    test_cthulhu_backend_command_execute_config(ctr_id, cmd_invalid, &arguments, 1, 1, "CONTAINER_CMD_KO");

    amxc_var_clean(&arguments);
    LOG_EXIT
}

void test_cthulhu_stop_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STOPPING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);

    expected_sig_eat(sig_eat_timeout);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_restart_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_RESTART, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RESTARTING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_RESTARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_list_ctr(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:list", false, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_update_ctr_keepdata(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.15", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);

    expected_sig_eat(sig_eat_timeout);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_update_ctr_removedata(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, true);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.14", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);

    expected_sig_eat(sig_eat_timeout);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_remove_ctr(UNUSED void** state) {
    LOG_ENTRY
    testhelper_remove_ctr("ctr_1", true);
    LOG_EXIT
}

void test_cthulhu_remove_ctr_cmd(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";
    char* command_id = "cid_2";

    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_CTR_REMOVE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_CONTAINER_ID, ctr_id);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:removed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_create_ctr_disklocation(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_disklocation";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "disklocation");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.20");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_DISKLOCATION, "subdir1/subdir2/disklocation");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_update_ctr_disklocation(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_disklocation";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "disklocationupdate");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.21");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_DISKLOCATION, "subdir1/subdir3/disklocationupdate");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "disklocationupdate", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.21", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_remove_ctr_disklocation(UNUSED void** state) {
    LOG_ENTRY
    testhelper_remove_ctr("ctr_disklocation", true);
    LOG_EXIT
}

void test_cthulhu_create_ctr_resources(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_resources";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, true);
    amxc_var_add_key(int32_t, &args, CTHULHU_CONTAINER_RESOURCES_MEMORY, 12345);
    amxc_var_add_key(int32_t, &args, CTHULHU_CONTAINER_RESOURCES_CPU, 15);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);

    char* limit_in_bytes = cthulhu_cgroup_setting_get("cthulhu/sb_for_ctr/__sb_for_ctr_ctr_resources__", cthulhu_cgroup_get_controller(controller_mem), "memory.limit_in_bytes");
    assert_non_null(limit_in_bytes);
    assert_string_equal(limit_in_bytes, "12641280");
    char* cfs_quota_us = cthulhu_cgroup_setting_get("cthulhu/sb_for_ctr/__sb_for_ctr_ctr_resources__", cthulhu_cgroup_get_controller(controller_cpu), "cpu.cfs_quota_us");
    assert_non_null(cfs_quota_us);
    assert_string_equal(cfs_quota_us, "60000");
    char* cfs_period_us = cthulhu_cgroup_setting_get("cthulhu/sb_for_ctr/__sb_for_ctr_ctr_resources__", cthulhu_cgroup_get_controller(controller_cpu), "cpu.cfs_period_us");
    assert_non_null(cfs_period_us);
    assert_string_equal(cfs_period_us, "100000");

    free(limit_in_bytes);
    free(cfs_quota_us);
    free(cfs_period_us);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_remove_ctr_resources(UNUSED void** state) {
    LOG_ENTRY
    testhelper_remove_ctr("ctr_resources", true);
    LOG_EXIT
}
