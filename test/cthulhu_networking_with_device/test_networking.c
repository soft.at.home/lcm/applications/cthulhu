/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_networking.h"
#include "expected_signal.h"
#include "test_setup.h"
#include "dummy_be.h"



#define ME "test"

#define NC "NetworkConfig"
#define NC_ACCESSINTERFACES "AccessInterfaces"
#define NC_ACCESSINTERFACES_REFERENCE "Reference"
#define NC_PORTFORWARDING "PortForwarding"
#define NC_PORTFORWARDING_INTERFACE "Interface"
#define NC_PORTFORWARDING_EXTERNALPORT "ExternalPort"
#define NC_PORTFORWARDING_INTERNALPORT "InternalPort"
#define NC_PORTFORWARDING_PROTOCOL "Protocol"
#define NC_PORTFORWARDING_PROTOCOL_TCP "TCP"
#define NC_PORTFORWARDING_PROTOCOL_UDP "UDP"
#define NC_PORTFORWARDING_PROTOCOL_BOTH "Both"
#define NC_PORTFORWARDING_STATUS "Status"
#define NC_PORTFORWARDING_STATUS_OFF "Off"
#define NC_PORTFORWARDING_STATUS_OK "Ok"
#define NC_PORTFORWARDING_STATUS_ERROR "Error"
#define NC_PORTFORWARDING_PATH "Path"

#define NC_INTERFACES "Interfaces"
#define NC_INTERFACES_NAME "Name"
#define NC_INTERFACES_REFERENCE "Reference"
#define NC_BRIDGES "Bridges"
#define NC_BRIDGES_INTERFACES "Interfaces"
#define NC_BRIDGES_BRIDGE "Bridge"
#define NC_DEFAULTINTERFACENAME "DefaultInterfaceName"

#define NC_DNSSD "DNSSD"
#define NC_DNSSD_REFERENCE "Reference"
#define NC_DNSSD_ENABLE "Enable"
#define NC_DNSSD_INTERFACE "Interface"
#define NC_DNSSD_INSTANCENAME "InstanceName"
#define NC_DNSSD_APPLICATIONPROTOCOL "ApplicationProtocol"
#define NC_DNSSD_TRANSPORTPROTOCOL "TransportProtocol"
#define NC_DNSSD_DOMAIN "Domain"
#define NC_DNSSD_PORT "Port"
#define NC_DNSSD_TEXTRECORD "TextRecord"
#define NC_DNSSD_TEXTRECORD_KEY "Key"
#define NC_DNSSD_TEXTRECORD_VALUE "Value"
#define NC_DNSSD_STATUS "Status"
#define NC_DNSSD_STATUS_OFF "Off"
#define NC_DNSSD_STATUS_OK "Ok"
#define NC_DNSSD_STATUS_ERROR "Error"
#define NC_DNSSD_PATH "Path"
#define NC_DNSSD_PORTFORWARDINGPATH "PortForwardingPath"

const char* sb_name = "sb_for_ctr";
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_dm_t dummy_dm;


static amxd_object_t* test_get_pf_instances(amxd_object_t* ctr_obj) {
    return amxd_object_findf(ctr_obj, CTHULHU_PLUGINS_PRIVATE "." NC "." NC_PORTFORWARDING);
}

static amxd_object_t* test_get_dnssd_instances(amxd_object_t* ctr_obj) {
    return amxd_object_findf(ctr_obj, CTHULHU_PLUGINS_PRIVATE "." NC "." NC_DNSSD);
}

static bool test_ctr_contains_pf(amxd_object_t* ctr_obj,
                                 const char* ext_itf,
                                 uint32_t ext_port,
                                 uint32_t int_port,
                                 const char* protocol,
                                 const char* status,
                                 const char* path) {
    printf("Look for ExtItf: %s ExtPort: %u IntPort: %d Protocol: %s Status: %s \n", ext_itf, ext_port, int_port, protocol, status);
    bool res = false;
    amxd_object_t* pfs = test_get_pf_instances(ctr_obj);
    amxd_object_for_each(instance, it_pf, pfs) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_pf, amxd_object_t, it);
        char* inst_ext_itf = amxd_object_get_cstring_t(instance, NC_PORTFORWARDING_INTERFACE, NULL);
        uint32_t inst_ext_port = amxd_object_get_uint32_t(instance, NC_PORTFORWARDING_EXTERNALPORT, NULL);
        uint32_t inst_int_port = amxd_object_get_uint32_t(instance, NC_PORTFORWARDING_INTERNALPORT, NULL);
        char* inst_status = amxd_object_get_cstring_t(instance, NC_PORTFORWARDING_STATUS, NULL);
        char* inst_path = amxd_object_get_cstring_t(instance, NC_PORTFORWARDING_PATH, NULL);
        char* inst_protocol = amxd_object_get_cstring_t(instance, NC_PORTFORWARDING_PROTOCOL, NULL);

        printf("ExtItf: %s ExtPort: %u IntPort: %d Protocol: %s Status: %s PATH: %s\n",
               inst_ext_itf, inst_ext_port, inst_int_port, inst_protocol, inst_status, inst_path);
        if((strcmp(ext_itf, inst_ext_itf) == 0) &&
           (ext_port == inst_ext_port) &&
           (int_port == inst_int_port) &&
           (strcmp(status, inst_status) == 0) &&
           (strcmp(path, inst_path) == 0) &&
           (strcmp(protocol, inst_protocol) == 0)
           ) {
            res = true;
            free(inst_ext_itf);
            free(inst_path);
            free(inst_status);
            free(inst_protocol);
            break;
        }
        free(inst_ext_itf);
        free(inst_path);
        free(inst_status);
        free(inst_protocol);
    }
    return res;
}

static bool test_ctr_contains_dnssd(const char* ctr_id,
                                    bool enable,
                                    const char* interface,
                                    const char* instance_name,
                                    const char* application_protocol,
                                    const char* transport_protocol,
                                    const char* domain,
                                    uint16_t port) {
    printf("Look for Itf: %s InstanceName: %s ApplicationProtocol: %s TransportProtocol: %s Domain: %s Port: %u\n",
           interface, instance_name, application_protocol, transport_protocol, domain, port);
    bool res = false;
    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);

    amxd_object_t* dnssds = test_get_dnssd_instances(ctr_obj);
    amxd_object_for_each(instance, it_dnssd, dnssds) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_dnssd, amxd_object_t, it);

        bool inst_enable = amxd_object_get_bool(instance, NC_DNSSD_ENABLE, NULL);
        char* inst_interface = amxd_object_get_cstring_t(instance, NC_DNSSD_INTERFACE, NULL);
        char* inst_instance_name = amxd_object_get_cstring_t(instance, NC_DNSSD_INSTANCENAME, NULL);
        char* inst_application_protocol = amxd_object_get_cstring_t(instance, NC_DNSSD_APPLICATIONPROTOCOL, NULL);
        char* inst_transport_protocol = amxd_object_get_cstring_t(instance, NC_DNSSD_TRANSPORTPROTOCOL, NULL);
        char* inst_domain = amxd_object_get_cstring_t(instance, NC_DNSSD_DOMAIN, NULL);
        uint16_t inst_port = amxd_object_get_uint16_t(instance, NC_DNSSD_PORT, NULL);

        printf("Found Itf: %s InstanceName: %s ApplicationProtocol: %s TransportProtocol: %s Domain: %s Port: %u\n",
               inst_interface, inst_instance_name, inst_application_protocol, inst_transport_protocol, inst_domain, inst_port);

        if((enable == inst_enable) &&
           (strcmp(interface, inst_interface) == 0) &&
           (strcmp(instance_name, inst_instance_name) == 0) &&
           (strcmp(application_protocol, inst_application_protocol) == 0) &&
           (strcmp(transport_protocol, inst_transport_protocol) == 0) &&
           (strcmp(domain, inst_domain) == 0) &&
           (port == inst_port)
           ) {
            res = true;
            free(inst_interface);
            free(inst_instance_name);
            free(inst_application_protocol);
            free(inst_transport_protocol);
            free(inst_domain);
            break;
        }
        free(inst_interface);
        free(inst_instance_name);
        free(inst_application_protocol);
        free(inst_transport_protocol);
        free(inst_domain);
    }
    return res;
}


static bool test_sb_network(const char* const ctr_id,
                            bool enabled,
                            const char* const type,
                            const char* const bridge,
                            const char* const itf) {
    bool res = false;
    char* priv_sb_id = NULL;
    char* tmpstr = NULL;
    amxd_object_t* priv_sb_obj = NULL;
    amxd_object_t* netns_obj = NULL;
    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    ASSERT_NOT_NULL(ctr_id, goto exit);

    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    ASSERT_STR_NOT_EMPTY(priv_sb_id, goto exit);

    priv_sb_obj = cthulhu_sb_get(priv_sb_id);
    ASSERT_NOT_NULL(priv_sb_obj, goto exit);

    netns_obj = amxd_object_get(priv_sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    ASSERT_NOT_NULL(priv_sb_obj, goto exit);

    ASSERT_TRUE((enabled == amxd_object_get_bool(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, NULL)), goto exit);
    tmpstr = amxd_object_get_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, NULL);
    ASSERT_TRUE((strcmp(type, tmpstr) == 0), goto exit);
    free(tmpstr);
    if(bridge || itf) {
        amxd_object_t* itfs_obj = amxd_object_get(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
        const amxc_llist_it_t* it = amxd_object_first_instance(itfs_obj);
        amxd_object_t* itf_obj = amxc_llist_it_get_data(it, amxd_object_t, it);

        ASSERT_NOT_NULL(itf_obj, goto exit);

        if(bridge) {
            tmpstr = amxd_object_get_cstring_t(itf_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_BRIDGE, NULL);
            ASSERT_TRUE((strcmp(bridge, tmpstr) == 0), goto exit);
            free(tmpstr);
        }
        if(itf) {
            tmpstr = amxd_object_get_cstring_t(itf_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE, NULL);
            ASSERT_TRUE((strcmp(itf, tmpstr) == 0), goto exit);
            free(tmpstr);
        }
    }
    tmpstr = NULL;
    res = true;
exit:
    free(tmpstr);
    free(priv_sb_id);
    return res;
}


// static void test_portmapping_contains(const char* path, uint32_t ext_port, const char* int_ip, uint32_t int_port, const char* protocol) {

//     amxb_bus_ctx_t* device_ctx = amxb_be_who_has(path);
//     assert_non_null(device_ctx);
//     amxc_var_t result;
//     amxc_var_init(&result);
//     amxb_get(device_ctx, path, 0, &result, 5);
//     // amxc_var_dump(&result, STDOUT_FILENO);

//     amxc_var_t* pf = GETP_ARG(&result, "0.0");
//     // amxc_var_dump(pf, STDOUT_FILENO);

//     assert_int_equal(GET_UINT32(pf, "ExternalPort"), ext_port);
//     assert_string_equal(GET_CHAR(pf, "InternalClient"), int_ip);
//     assert_int_equal(GET_UINT32(pf, "InternalPort"), int_port);
//     assert_string_equal(GET_CHAR(pf, "Protocol"), protocol);
//     amxc_var_clean(&result);
// }

static void test_load_device(void) {
    test_register_dummy_be();
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(test_load_dummy_remote("../config/device.odl"), 0);
    assert_int_equal(amxb_register(bus_ctx, &dummy_dm), 0);
}

static int networkconfig_add_access_itf(amxc_htable_t* networkconfig, const char* const itf) {
    amxc_var_t* access_itfs = NULL;
    amxc_htable_it_t* it = amxc_htable_get(networkconfig, NC_ACCESSINTERFACES);
    if(it) {
        access_itfs = amxc_htable_it_get_data(it, amxc_var_t, hit);
    }
    if(!access_itfs) {
        amxc_var_new(&access_itfs);
        amxc_var_set_type(access_itfs, AMXC_VAR_ID_LIST);
        amxc_htable_insert(networkconfig, NC_ACCESSINTERFACES, &access_itfs->hit);
    }
    amxc_var_t* access_itf = amxc_var_add_new(access_itfs);
    amxc_var_set_type(access_itf, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(access_itf, NC_ACCESSINTERFACES_REFERENCE, itf);
    return 0;
}

static int networkconfig_add_portforwarding(amxc_htable_t* networkconfig, const char* const itf,
                                            uint32_t ext_port, uint32_t int_port, const char* const protocol) {
    amxc_var_t* pfs = NULL;
    amxc_htable_it_t* it = amxc_htable_get(networkconfig, NC_PORTFORWARDING);
    if(it) {
        pfs = amxc_htable_it_get_data(it, amxc_var_t, hit);
    }
    if(!pfs) {
        amxc_var_new(&pfs);
        amxc_var_set_type(pfs, AMXC_VAR_ID_LIST);
        amxc_htable_insert(networkconfig, NC_PORTFORWARDING, &pfs->hit);
    }
    amxc_var_t* pf = amxc_var_add_new(pfs);
    amxc_var_set_type(pf, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(pf, NC_PORTFORWARDING_INTERFACE, itf);
    amxc_var_add_new_key_cstring_t(pf, NC_PORTFORWARDING_PROTOCOL, protocol);
    amxc_var_add_new_key_uint32_t(pf, NC_PORTFORWARDING_EXTERNALPORT, ext_port);
    amxc_var_add_new_key_uint32_t(pf, NC_PORTFORWARDING_INTERNALPORT, int_port);
    return 0;
}

static int networking_add_ref_interface(const char* const name, const char* const reference) {
    int res = -1;
    amxd_object_t* itf_template = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_NETWORKCONFIG ".Interfaces");
    ASSERT_NOT_NULL(itf_template, goto exit);

    amxc_var_t params;
    amxd_object_t* itf_obj = NULL;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, "Name", name);
    amxc_var_add_new_key_cstring_t(&params, "Reference", reference);
    amxd_object_add_instance(&itf_obj, itf_template, NULL, 0, &params);
    amxc_var_clean(&params);

    res = 0;
exit:
    return res;
}

static int networking_add_fw_rules(const char* const interfaces) {
    int res = -1;
    amxd_object_t* fw_template = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_NETWORKCONFIG ".FirewallRules");
    ASSERT_NOT_NULL(fw_template, goto exit);

    amxc_var_t params;
    amxd_object_t* itf_obj = NULL;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, "Interfaces", interfaces);
    amxd_object_add_instance(&itf_obj, fw_template, NULL, 0, &params);
    amxc_var_clean(&params);
    ASSERT_NOT_NULL(itf_obj, goto exit);

    amxd_object_t* rules_obj = amxd_object_get(itf_obj, "Rules");
    ASSERT_NOT_NULL(rules_obj, goto exit);
    amxd_object_t* rule_obj = NULL;
    amxd_object_add_instance(&rule_obj, rules_obj, NULL, 0, NULL);
    ASSERT_NOT_NULL(rule_obj, goto exit);

    res = 0;
exit:
    return res;
}

static int networkconfig_add_dnssd(amxc_htable_t* networkconfig,
                                   bool enable,
                                   const char* interface,
                                   const char* instance_name,
                                   const char* application_protocol,
                                   const char* transport_protocol,
                                   const char* domain,
                                   uint16_t port) {
    amxc_var_t* dnssds = NULL;
    amxc_htable_it_t* it = amxc_htable_get(networkconfig, NC_DNSSD);
    if(it) {
        dnssds = amxc_htable_it_get_data(it, amxc_var_t, hit);
    }
    if(!dnssds) {
        amxc_var_new(&dnssds);
        amxc_var_set_type(dnssds, AMXC_VAR_ID_LIST);
        amxc_htable_insert(networkconfig, NC_DNSSD, &dnssds->hit);
    }

    amxc_var_t* dnssd = amxc_var_add_new(dnssds);
    amxc_var_set_type(dnssd, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_bool(dnssd, NC_DNSSD_ENABLE, enable);
    amxc_var_add_new_key_cstring_t(dnssd, NC_DNSSD_INTERFACE, interface);
    amxc_var_add_new_key_cstring_t(dnssd, NC_DNSSD_INSTANCENAME, instance_name);
    amxc_var_add_new_key_cstring_t(dnssd, NC_DNSSD_APPLICATIONPROTOCOL, application_protocol);
    amxc_var_add_new_key_cstring_t(dnssd, NC_DNSSD_TRANSPORTPROTOCOL, transport_protocol);
    amxc_var_add_new_key_cstring_t(dnssd, NC_DNSSD_DOMAIN, domain);
    amxc_var_add_new_key_uint16_t(dnssd, NC_DNSSD_PORT, port);

    return 0;
}

static int networking_set_default_fw_chain(void) {
    int res = -1;
    amxd_object_t* config = amxd_dm_findf(cthulhu_get_dm(), CTHULHU_DM_NETWORKCONFIG);
    ASSERT_NOT_NULL(config, goto exit);

    amxd_object_set_csv_string_t(config, "DefaultFirewallChain", "Device.Firewall.Chain.[Alias==\"LCM\"].");

    res = 0;
exit:
    return res;
}

int test_cthulhu_networking_setup_wo_device(UNUSED void** state) {
    int res = test_cthulhu_setup();
    networking_add_ref_interface("Wan", "Device.Logical.Interface.1.");
    networking_add_ref_interface("Lan", "Device.Logical.Interface.2.");
    networking_add_fw_rules("Lan");
    networking_add_fw_rules("Lan,Wan");
    networking_set_default_fw_chain();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_networking_teardown_wo_device (UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown();
    LOG_EXIT
}

int test_cthulhu_networking_setup_with_device (UNUSED void** state) {
    LOG_ENTRY
    test_cthulhu_init_dm();
    test_load_device();
    int res = test_cthulhu_setup_wo_dm();
    networking_add_ref_interface("Wan", "Device.Logical.Interface.1.");
    networking_add_ref_interface("Lan", "Device.Logical.Interface.2.");
    networking_add_fw_rules("Lan");
    networking_add_fw_rules("Lan,Wan");
    networking_set_default_fw_chain();
    sleep(5);
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
    LOG_EXIT
}

int test_cthulhu_networking_teardown_with_device (UNUSED void** state) {
    LOG_ENTRY
    testhelper_cthulhu_stop_sandbox(sb_name);

    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    return test_cthulhu_teardown();
    test_unregister_dummy_be();
    LOG_EXIT
}

void test_cthulhu_pf_create_ctr (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);

    LOG_EXIT
}

void test_cthulhu_pf_add_ports (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);
    amxd_object_t* pf_instances_obj = test_get_pf_instances(ctr_obj);
    assert_non_null(pf_instances_obj);

    amxc_var_t params;
    amxd_object_t* pf_obj = NULL;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_EXTERNALPORT, 1000);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_INTERNALPORT, 2000);
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_INTERFACE, "Wan");
    amxd_object_add_instance(&pf_obj, pf_instances_obj, NULL, 0, &params);
    amxc_var_clean(&params);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_EXTERNALPORT, 1001);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_INTERNALPORT, 2001);
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_INTERFACE, "Lan");
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_PROTOCOL, "UDP");
    amxd_object_add_instance(&pf_obj, pf_instances_obj, NULL, 0, &params);
    amxc_var_clean(&params);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_EXTERNALPORT, 1002);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_INTERNALPORT, 2002);
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_INTERFACE, "Lan");
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_PROTOCOL, "TCP");
    amxd_object_add_instance(&pf_obj, pf_instances_obj, NULL, 0, &params);
    amxc_var_clean(&params);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_EXTERNALPORT, 1003);
    amxc_var_add_new_key_uint32_t(&params, NC_PORTFORWARDING_INTERNALPORT, 2003);
    amxc_var_add_new_key_cstring_t(&params, NC_PORTFORWARDING_INTERFACE, "Lan");
    amxd_object_add_instance(&pf_obj, pf_instances_obj, NULL, 0, &params);
    amxc_var_clean(&params);


    assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    LOG_EXIT
}

void test_cthulhu_pf_start_ctr_wo_devices (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", CTHULHU_CONTAINER_STATUS_STARTING, false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);

    const char* err_state = NC_PORTFORWARDING_STATUS_OFF;
    // should be error, but callbacks do not seem to work
    //const char* err_state = NC_PORTFORWARDING_STATUS_ERROR;

    assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", err_state, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", err_state, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", err_state, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    LOG_EXIT
}

void test_cthulhu_pf_stop_ctr (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);


    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);

    assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    LOG_EXIT
}

void test_cthulhu_pf_start_ctr_with_devices (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_START, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Container.Instances.cpe-ctr_1.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", CTHULHU_CONTAINER_STATUS_RUNNING, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);


    // the plugin does not receive events in the test setup :(
    // assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", NC_PORTFORWARDING_STATUS_OK, "Device.NAT.PortMapping.2."));
    // assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", NC_PORTFORWARDING_STATUS_OK, "Device.NAT.PortMapping.3."));
    // assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", NC_PORTFORWARDING_STATUS_OK, "Device.NAT.PortMapping.4."));
    // assert_true(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));

    // test_portmapping_contains("Device.NAT.PortMapping.2.", 1000, "192.168.2.1", 2000, "TCP");
    // test_portmapping_contains("Device.NAT.PortMapping.3.", 1001, "192.168.1.1", 2001, "UDP");
    // test_portmapping_contains("Device.NAT.PortMapping.4.", 1002, "192.168.2.1", 2002, "TCP");
    LOG_EXIT
}

/**
 * @brief update the container, but the update has no networking settings
 *
 * @param state
 */
void test_cthulhu_pf_update_wo_devices_no_pf (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.15", false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // wait 5s and eat all signals
    expected_sig_eat(5);

    assert_false(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    LOG_EXIT
}

/**
 * @brief update the container, but the update has different networking settings
 *
 * @param state
 */
void test_cthulhu_pf_update_wo_devices_with_pf (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";
    amxc_htable_t* networkconfig = NULL;

    amxd_object_t* ctr_obj = cthulhu_ctr_get(ctr_id);
    assert_non_null(ctr_obj);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Wan"), 0);
    assert_int_equal(networkconfig_add_portforwarding(networkconfig, "Wan", 4000, 5000, "Both"), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);


    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLE, "alpine", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_BUNDLEVERSION, "3.15", false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // wait 5s and eat all signals
    expected_sig_eat(5);

    assert_false(test_ctr_contains_pf(ctr_obj, "Wan", 1000, 2000, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1001, 2001, "UDP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1002, 2002, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_false(test_ctr_contains_pf(ctr_obj, "Lan", 1003, 2003, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 4000, 5000, "TCP", NC_PORTFORWARDING_STATUS_OFF, ""));
    assert_true(test_ctr_contains_pf(ctr_obj, "Wan", 4000, 5000, "UDP", NC_PORTFORWARDING_STATUS_OFF, ""));

    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    LOG_EXIT
}

void test_cthulhu_pf_remove_ctr (UNUSED void** state) {
    LOG_ENTRY
    const char* ctr_id = "ctr_1";
    expected_signal_t expected_sig;
    // wait 5s and eat all signals
    expected_sig_eat(2);

    testhelper_remove_ctr(ctr_id, true);
    sig_read_all();
    LOG_EXIT
}

void test_cthulhu_pf_valid_accessitfs(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Lan"), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH, "br-lcm", "lcm0"));
    LOG_EXIT
}

void test_cthulhu_pf_valid_accessitfs_2(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Lan"), 0);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Wan"), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH, "br-lcm", "lcm0"));
    LOG_EXIT
}

void test_cthulhu_pf_invalid_accessitfs(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Wan"), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY, NULL, NULL));
    LOG_EXIT
}

void test_cthulhu_pf_invalid_accessitfs_2(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Pan"), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY, NULL, NULL));
    LOG_EXIT
}

void test_cthulhu_pf_no_accessitfs(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH, "br-lcm", "lcm0"));
    LOG_EXIT
}

void test_cthulhu_pf_no_networkconfig(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    assert_true(test_sb_network(ctr_id, true, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY, NULL, NULL));
    LOG_EXIT
}

/**
 * DNSSD tests
 *
 */

void test_cthulhu_dnssd(UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    amxc_htable_t* networkconfig = NULL;
    const char* ctr_id = "ctr_1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxc_htable_new(&networkconfig, 2), 0);

    assert_non_null(dm_ctr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(networkconfig_add_access_itf(networkconfig, "Lan"), 0);
    assert_int_equal(networkconfig_add_dnssd(networkconfig, true, "Device.IP.Interface.2.", "nyDnssd", "daap", "TCP", "local", 3452), 0);
    amxc_var_add_new_key_amxc_htable_t(&args, CTHULHU_CONTAINER_NETWORKCONFIG, networkconfig);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_htable_delete(&networkconfig, variant_htable_it_free);
    assert_true(test_ctr_contains_dnssd(ctr_id, true, "Device.IP.Interface.2.", "nyDnssd", "daap", "TCP", "local", 3452));
    LOG_EXIT
}
