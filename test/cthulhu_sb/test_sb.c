/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_priv.h"
#include "test_sb.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

static void test_cthulhu_stop_sandbox_with_name(const char* name);

int test_cthulhu_sb_setup (UNUSED void** state) {
    LOG_ENTRY
    return test_cthulhu_setup();
    LOG_EXIT
}

int test_cthulhu_sb_teardown (UNUSED void** state) {
    LOG_ENTRY
    test_cthulhu_stop_sandbox_with_name("my_sandbox");
    return test_cthulhu_teardown();
    LOG_EXIT
}

void test_cthulhu_create_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, false);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_start_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

static void test_cthulhu_stop_sandbox_with_name(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_stop_sandbox (UNUSED void** state) {
    LOG_ENTRY
    test_cthulhu_stop_sandbox_with_name("my_sandbox");
    LOG_EXIT
}

void test_cthulhu_restart_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    SAH_TRACEZ_INFO(ME, "Restart");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_RESTART, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Sandbox.Instances.cpe-my_sandbox.", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.from", "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "parameters.Status.to", "Restarting", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_list_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:list", false, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    amxc_var_add_new_key_bool(&args, CTHULHU_CMD_CTR_LS_QUIET, true);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_exec_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CMD_SB_EXEC_CMD, "ls");
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_EXEC, &args, &ret), amxd_status_ok);
    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_remove_sandbox (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_REMOVE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:removed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    sleep(1);
    amxp_timers_calculate();
    amxp_timers_check();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_create_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_1";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_CREATE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_SANDBOX_ENABLE, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_start_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_1";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_START);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_stop_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_2";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_STOP);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_restart_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_3";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_RESTART);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_list_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_4";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_LS);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_LS, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:list", false, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_remove_sandbox_cmd (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* command_id = "cid_5";
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_REMOVE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMANDID, command_id);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:removed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    expected_sig_add_match_cstring_t(&expected_sig, CTHULHU_NOTIF_COMMAND_ID, command_id, true);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    sleep(1);
    amxp_timers_calculate();
    amxp_timers_check();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_create_sandbox_cmd_empty (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox_cmd";
    amxc_var_t* params;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_COMMAND_COMMAND, CTHULHU_CMD_SB_CREATE);
    params = amxc_var_add_key(amxc_htable_t, &args, CTHULHU_COMMAND_PARAMETERS, NULL);
    assert_non_null(params);
    amxc_var_add_key(cstring_t, params, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, params, CTHULHU_SANDBOX_ENABLE, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_COMMAND, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_create_sandbox_enabled (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    char* sb_name = "my_sandbox";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, true);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}
