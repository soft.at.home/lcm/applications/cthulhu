/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>
#include <lcm/lcm_assert.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_envvariable.h"
#include "expected_signal.h"
#include "test_setup.h"

#define ME "test"

const char* sb_name = "sb_for_ctr";


static int envvars_add(amxc_llist_t* envvars, const char* key, const char* value) {
    int res = -1;
    amxc_var_t* envvar = NULL;
    ASSERT_NOT_NULL(envvars, goto exit);

    ASSERT_SUCCESS(amxc_var_new(&envvar), goto exit);
    ASSERT_SUCCESS(amxc_var_set_type(envvar, AMXC_VAR_ID_HTABLE), goto exit);

    if(key) {
        amxc_var_add_new_key_cstring_t(envvar, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, key);
    }
    if(value) {
        amxc_var_add_new_key_cstring_t(envvar, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, value);
    }
    ASSERT_SUCCESS(amxc_llist_append(envvars, &(envvar)->lit), goto exit);

    res = 0;
exit:
    return res;
}

static amxd_object_t* test_get_envvar_objects(const char* ctr_id) {
    amxd_object_t* ctr_obj = testhelper_get_ctr(ctr_id);
    assert_non_null(ctr_obj);
    return amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
}

static amxd_object_t* test_get_envvar_oci_objects(const char* ctr_id) {
    amxd_object_t* ctr_obj = testhelper_get_ctr(ctr_id);
    assert_non_null(ctr_obj);
    return amxd_object_findf(ctr_obj, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI);
}

static bool test_ctr_contains_envvar(const char* ctr_id, const char* key, const char* value) {
    bool res = false;
    amxd_object_t* envvars = test_get_envvar_objects(ctr_id);
    amxd_object_for_each(instance, it_env, envvars) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_env, amxd_object_t, it);
        char* inst_key = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, NULL);
        char* inst_value = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, NULL);

        printf("Key: %s Value: %s \n", inst_key, inst_value);
        if((!key || (strcmp(key, inst_key) == 0)) &&
           (!value || (strcmp(value, inst_value) == 0))) {
            res = true;
            free(inst_key);
            free(inst_value);
            break;
        }
        free(inst_key);
        free(inst_value);
    }
    return res;
}

static bool test_ctr_contains_envvar_oci(const char* ctr_id, const char* key, const char* value) {
    bool res = false;
    amxd_object_t* envvars = test_get_envvar_oci_objects(ctr_id);
    amxd_object_for_each(instance, it_env, envvars) {
        amxd_object_t* instance = amxc_llist_it_get_data(it_env, amxd_object_t, it);
        char* inst_key = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_KEY, NULL);
        char* inst_value = amxd_object_get_cstring_t(instance, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE, NULL);

        printf("Key: %s Value: %s \n", inst_key, inst_value);
        if((!key || (strcmp(key, inst_key) == 0)) &&
           (!value || (strcmp(value, inst_value) == 0))) {
            res = true;
            free(inst_key);
            free(inst_value);
            break;
        }
        free(inst_key);
        free(inst_value);
    }
    return res;
}

int test_cthulhu_ev_setup(UNUSED void** state) {
    int res = test_cthulhu_setup();
    testhelper_cthulhu_create_sandbox(sb_name, true);
    return res;
}

int test_cthulhu_ev_teardown(UNUSED void** state) {
    testhelper_cthulhu_stop_sandbox(sb_name);
    return test_cthulhu_teardown();
}

void test_cthulhu_envvar_invalid(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_llist_t* envvars = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    // envvars is not a list
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES, "not_a_list");
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_var_clean(&args);
    // envvar does not contain key
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&envvars), 0);
    assert_int_equal(envvars_add(envvars, NULL, "Test-Value"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES, envvars);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&envvars, variant_list_it_free);
    amxc_var_clean(&args);
    // envvar does not contain value
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&envvars), 0);
    assert_int_equal(envvars_add(envvars, "Test-Key", NULL), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES, envvars);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_unknown_error);
    amxc_llist_delete(&envvars, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cthulhu_envvar_default(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_llist_t* envvars = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);
    assert_int_equal(amxc_llist_new(&envvars), 0);
    assert_int_equal(envvars_add(envvars, "Test-Key1", "Test-Value1"), 0);
    assert_int_equal(envvars_add(envvars, "Test-Key2", "Test-Value2"), 0);
    assert_int_equal(envvars_add(envvars, "Test-Key3", "Test-Value3"), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES, envvars);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".2."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key2", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".2."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value2", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".3."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key3", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".3."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value3", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&envvars, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // assert the added envvars
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key1", "Test-Value1"));
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key2", "Test-Value2"));
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key3", "Test-Value3"));

}

void test_cthulhu_envvar_update(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value1", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".2."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key2", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".2."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value2", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".3."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_KEY, "Test-Key3", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES ".3."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_VALUE, "Test-Value3", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    // assert the previously added envvars
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key1", "Test-Value1"));
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key2", "Test-Value2"));
    assert_true(test_ctr_contains_envvar(ctr_id, "Test-Key3", "Test-Value3"));

}

void test_cthulhu_envvar_remove(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_llist_t* envvars = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    assert_int_equal(amxc_llist_new(&envvars), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES, envvars);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&envvars, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_false(test_ctr_contains_envvar(ctr_id, "Test-Key1", "Test-Value1"));
    assert_false(test_ctr_contains_envvar(ctr_id, "Test-Key2", "Test-Value2"));
    assert_false(test_ctr_contains_envvar(ctr_id, "Test-Key3", "Test-Value3"));

    testhelper_remove_ctr(ctr_id, true);
}

void test_cthulhu_envvar_oci_default(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_llist_t* envvars = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_SANDBOX, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_AUTOSTART, false);
    set_autorestart(&args, false);

    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_CREATE, &args, &ret), amxd_status_deferred);
    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_KEY, "PATH", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE, "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&envvars, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // assert the oci added envvars
    assert_true(test_ctr_contains_envvar_oci(ctr_id, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"));

}

void test_cthulhu_envvar_oci_update(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";

    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.15");
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_STATUS, CTHULHU_CONTAINER_STATUS_STOPPED, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_KEY, "PATH", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI ".1."CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI_VALUE, "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin", false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    // assert the previously added envvars
    assert_true(test_ctr_contains_envvar_oci(ctr_id, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"));

}

void test_cthulhu_envvar_oci_remove(UNUSED void** state) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;
    const char* ctr_id = "ctr_envvar";
    amxc_llist_t* envvars = NULL;
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLE, "alpine");
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_BUNDLEVERSION, "3.14");
    assert_int_equal(amxc_llist_new(&envvars), 0);

    amxc_var_add_new_key_amxc_llist_t(&args, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES_OCI, envvars);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_UPDATE, &args, &ret), amxd_status_ok);

    expected_sig_init(&expected_sig, "cthulhu:ctr:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_llist_delete(&envvars, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_false(test_ctr_contains_envvar(ctr_id, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"));

    testhelper_remove_ctr(ctr_id, true);
}
