/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>


#include "cthulhu_priv.h"
#include "test_resources.h"
#include "expected_signal.h"
#include "test_setup.h"



#define ME "test"

static const char* sb_name = "sb_res";

int test_cthulhu_res_setup (UNUSED void** state) {
    LOG_ENTRY
    return test_cthulhu_setup();
    LOG_EXIT
}

int test_cthulhu_res_teardown (UNUSED void** state) {
    LOG_ENTRY
    return test_cthulhu_teardown();
    LOG_EXIT
}

static void _test_cthulhu_res_create_sb (UNUSED void** state, bool with_disk) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, CTHULHU_SANDBOX_ID, sb_name);
    if(with_disk) {
        amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_DISKSPACE, 2000);
    }
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_CPU, 50);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_MEMORY, 60);
    amxc_var_add_new_key_bool(&args, CTHULHU_SANDBOX_ENABLE, false);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    if(with_disk) {
        expected_sig_add_match_int32_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_DISKSPACE, 2000, false);
    }
    expected_sig_add_match_int32_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_CPU, 50, false);
    expected_sig_add_match_int32_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_MEMORY, 60, false);

    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

static int test_cthulhu_add_device(amxd_object_t* sb_obj, const char* const device, const char* const permission,
                                   const char* const type, int major, int minor, const char* access, bool create) {
    int res = -1;
    amxd_object_t* devices_template = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_DEVICES);
    when_null_log(devices_template, exit);

    amxc_var_t params;
    amxd_object_t* device_obj = NULL;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_DEVICES_DEVICE, device);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_DEVICES_PERMISSION, permission);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_DEVICES_TYPE, type);
    amxc_var_add_new_key_int32_t(&params, CTHULHU_SANDBOX_DEVICES_MAJOR, major);
    amxc_var_add_new_key_int32_t(&params, CTHULHU_SANDBOX_DEVICES_MINOR, minor);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_DEVICES_ACCESS, access);
    amxc_var_add_new_key_bool(&params, CTHULHU_SANDBOX_DEVICES_CREATE, create);

    amxd_status_t s = amxd_object_add_instance(&device_obj, devices_template, NULL, 0, &params);
    assert_int_equal(s, amxd_status_ok);
    amxc_var_clean(&params);

    res = 0;
exit:
    return res;
}

void test_cthulhu_res_create_sb (UNUSED void** state) {
    LOG_ENTRY
    _test_cthulhu_res_create_sb(state, true);

    testhelper_cthulhu_remove_sandbox(sb_name);
    LOG_EXIT
}

void test_cthulhu_res_disk_limits (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_DISKSPACE, 100);
    amxc_var_add_new_key_bool(&args, CTHULHU_SANDBOX_ENABLE, false);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_unknown_error);

    expected_sig_init(&expected_sig, "cthulhu:error", false, false);
    expected_sig_add_match_int32_t(&expected_sig, "information.type", 7004, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_res_cpu_limits (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_CPU, 101);
    amxc_var_add_new_key_bool(&args, CTHULHU_SANDBOX_ENABLE, false);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_unknown_error);

    expected_sig_init(&expected_sig, "cthulhu:error", false, false);
    expected_sig_add_match_int32_t(&expected_sig, "information.type", 7004, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}

void test_cthulhu_res_get_stats(UNUSED void** state) {
    LOG_ENTRY
    cthulhu_resource_stats_t cthulhu_stats;

    _test_cthulhu_res_create_sb(state, false);
    testhelper_cthulhu_start_sandbox(sb_name);
    cthulhu_stats.total = -1;
    cthulhu_stats.free = -1;
    cthulhu_stats.used = -1;

    cthulhu_sb_diskspace_resources_get(sb_name, &cthulhu_stats);
    printf("total %ld free %ld used %ld\n", cthulhu_stats.total, cthulhu_stats.free, cthulhu_stats.used);
    cthulhu_cgroup_memory_resources_get(sb_name, &cthulhu_stats);
    printf("total %ld free %ld used %ld\n", cthulhu_stats.total, cthulhu_stats.free, cthulhu_stats.used);
    assert_int_equal(cthulhu_stats.total, 60);
    assert_int_equal(cthulhu_stats.free, 27);
    assert_int_equal(cthulhu_stats.used, 33);

    testhelper_cthulhu_remove_sandbox(sb_name);
    LOG_EXIT
}


void test_cthulhu_res_devices(UNUSED void** state) {
    LOG_ENTRY
    cthulhu_resource_stats_t cthulhu_stats;

    _test_cthulhu_res_create_sb(state, false);
    amxd_object_t* sb_obj = cthulhu_sb_get(sb_name);
    assert_non_null(sb_obj);
    test_cthulhu_add_device(sb_obj, "a", "Deny", "c", -1, -1, "rwm", false);
    test_cthulhu_add_device(sb_obj, "/dev/null", "Allow", "c", 1, 3, "rwm", false);

    testhelper_cthulhu_start_sandbox(sb_name);


    testhelper_cthulhu_remove_sandbox(sb_name);
    LOG_EXIT
}


void test_cthulhu_res_modify (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    _test_cthulhu_res_create_sb(state, true);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_DISKSPACE, 3000);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_CPU, 20);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_MEMORY, 4000);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_MODIFY, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "dm:object-changed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "object", "Cthulhu.Sandbox.Instances.cpe-sb_res.", false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_DISKSPACE ".from", 2000, false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_DISKSPACE ".to", 3000, false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_CPU ".from", 50, false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_CPU ".to", 20, false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_MEMORY ".from", 60, false);
    expected_sig_add_match_int32_t(&expected_sig, "parameters."CTHULHU_SANDBOX_MEMORY ".to", 4000, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    testhelper_cthulhu_remove_sandbox(sb_name);
    LOG_EXIT
}

void test_cthulhu_res_modify_disk_smaller (UNUSED void** state) {
    LOG_ENTRY
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    _test_cthulhu_res_create_sb(state, true);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_new_key_int32_t(&args, CTHULHU_SANDBOX_DISKSPACE, 1500);
    expected_sig_reset_received();
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_MODIFY, &args, &ret), amxd_status_unknown_error);
    expected_sig_init(&expected_sig, "cthulhu:error", false, false);
    expected_sig_add_match_int32_t(&expected_sig, "information.type", 7004, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);

    sig_read_all();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    LOG_EXIT
}
