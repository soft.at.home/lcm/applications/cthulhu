/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>

#include <lcm/amxc_data.h>

#include "cthulhu_priv.h"
#include "wrap_amxm.h"
#include "test_setup.h"

#define UNUSED __attribute__((unused))

#define ME "test"

static amxc_htable_t* containers;

typedef struct _ctr_info_with_it {
    cthulhu_ctr_info_t* ctr_info;
    amxc_llist_it_t lit;        /**< Linked list iterator, can be used to store the variant in a linked list */
    amxc_htable_it_t hit;       /**< Hash table iterator, can be used to store the variant in a hash table */
} ctr_info_with_it_t;

static void container_htable_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    ctr_info_with_it_t* ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    cthulhu_ctr_info_delete(&(ctr_info_wrapper->ctr_info));
    free(ctr_info_wrapper);
}

void wrap_amxm_init() {
    amxc_htable_new(&containers, 5);
}

void wrap_amxm_cleanup() {
    amxc_htable_delete(&containers, container_htable_it_free);
}

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);
int __wrap_amxm_so_close(amxm_shared_object_t** so);
int __wrap_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);
int __wrap_amxm_so_has_function(amxm_shared_object_t* const so,
                                const char* const module_name,
                                const char* const func_name);

int __real_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so) {
    if(strcmp(shared_object_name, "packager") == 0) {
        int rv = (int) mock();
        if(rv == 0) {
            *so = (amxm_shared_object_t*) calloc(1, sizeof(amxm_shared_object_t));
        } else {
            *so = NULL;
        }
        return rv;
    }
    return __real_amxm_so_open(so, shared_object_name, path_to_so);
}

amxm_module_t* __real_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);

amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name) {
    if(strcmp(module_name, "mod_cthulhu") == 0) {
        amxm_module_t* mod = mock_ptr_type(amxm_module_t*);
        return mod;
    }
    return __real_amxm_so_get_module(shared_object, module_name);
}

int __wrap_amxm_so_close(amxm_shared_object_t** so) {
    if(*so != NULL) {
        free(*so);
        *so = NULL;
    }

    return 0;
}

static int container_init(amxc_var_t* args) {
    int res = -1;
    amxo_parser_t* parser = (amxo_parser_t*) GET_DATA(args, CTHULHU_BACKEND_CMD_INIT_PARAM_PARSER);
    ASSERT_NOT_NULL(parser, goto exit);

    res = 0;
exit:
    return res;
}

static int container_information(UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(ret, CTHULHU_INFO_BACKENDNAME, "lxc");
    amxc_var_add_new_key_bool(ret, CTHULHU_INFO_BUNDLES_SUPPORTED, false);
    amxc_var_add_new_key_bool(ret, CTHULHU_INFO_OVERLAYFSONCREATE, false);

    return 0;
}

static void notif_changed(ctr_info_with_it_t* ctr_info_wrapper) {
    amxc_var_t notif;
    amxc_var_init(&notif);
    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_cthulhu_ctr_info_t(&notif, "data", ctr_info_wrapper->ctr_info);
    cthulhu_backend_changed_handler(CTHULHU_BACKEND_NOTIF_CHANGED, &notif, NULL);

    amxc_var_clean(&notif);
}

static int container_create(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    config = amxc_var_get_const_cthulhu_config_t(args);
    if(!config) {
        SAH_TRACE_ERROR("Config argument is not of type cthulhu_config_t");
        goto exit;
    }
    if(amxc_htable_get(containers, config->id)) {
        SAH_TRACE_ERROR("container already exists");
        goto exit;
    }
    ctr_info_wrapper = calloc(1, sizeof(ctr_info_with_it_t));
    if(cthulhu_ctr_info_new(&(ctr_info_wrapper->ctr_info))) {
        SAH_TRACE_ERROR("Could not init ctr_info");
        free(ctr_info_wrapper);
        goto exit;
    }
    cthulhu_ctr_info_t* ctr_info = ctr_info_wrapper->ctr_info;
    ctr_info->ctr_id = strdup(config->id);
    ctr_info->bundle = strdup(config->bundle_name);
    ctr_info->bundle_version = strdup(config->bundle_version);
    ctr_info->sb_id = strdup(config->sandbox);
    ctr_info->status = cthulhu_ctr_status_stopped;
    amxc_htable_insert(containers, config->id, &ctr_info_wrapper->hit);

    res = 0;
exit:
    return res;
}

static int container_remove(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_take(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    amxc_htable_it_clean(it, container_htable_it_free);

    res = 0;
exit:
    return res;
}

static int container_start(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_get(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    ctr_info_wrapper->ctr_info->status = cthulhu_ctr_status_running;
    cthulhu_ctr_info_itf_t* itf = NULL;
    cthulhu_ctr_info_itf_new(&itf);
    itf->name = strdup("eth0");
    amxc_llist_add_string(&itf->ips, "192.168.1.1");
    amxc_llist_add_string(&itf->ips, "192.168.1.2");
    amxc_llist_append(&ctr_info_wrapper->ctr_info->interfaces, &itf->it);
    cthulhu_ctr_info_itf_new(&itf);
    itf->name = strdup("eth1");
    amxc_llist_add_string(&itf->ips, "192.168.2.1");
    amxc_llist_append(&ctr_info_wrapper->ctr_info->interfaces, &itf->it);
    cthulhu_ctr_info_itf_t* inst = NULL;
    amxc_llist_for_each(it, (&ctr_info_wrapper->ctr_info->interfaces)) {
        inst = amxc_llist_it_get_data(it, cthulhu_ctr_info_itf_t, it);
    }
    notif_changed(ctr_info_wrapper);

    res = 0;
exit:
    return res;
}

static int container_stop(amxc_var_t* args) {
    int res = -1;
    const cthulhu_config_t* config = NULL;
    ctr_info_with_it_t* ctr_info_wrapper = NULL;
    amxc_htable_it_t* it = NULL;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_get(containers, ctr_id);
    if(!it) {
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }
    ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
    ctr_info_wrapper->ctr_info->status = cthulhu_ctr_status_stopped;
    amxc_llist_clean(&ctr_info_wrapper->ctr_info->interfaces, cthulhu_interfaces_list_it_free);

    notif_changed(ctr_info_wrapper);

    res = 0;
exit:
    return res;
}

static int container_list(UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_htable_for_each(it, containers) {
        ctr_info_with_it_t* ctr_info_wrapper = amxc_htable_it_get_data(it, ctr_info_with_it_t, hit);
        // printf("ADD TO LIST: %s STATE: %s\n", ctr_info_wrapper->ctr_info->ctr_id,
        //        cthulhu_ctr_status_to_string(ctr_info_wrapper->ctr_info->status));

        cthulhu_ctr_info_itf_t* inst = NULL;
        amxc_llist_for_each(it, (&ctr_info_wrapper->ctr_info->interfaces)) {
            inst = amxc_llist_it_get_data(it, cthulhu_ctr_info_itf_t, it);
        }
        cthulhu_ctr_info_t* cpy = NULL;
        cthulhu_ctr_info_new(&cpy);
        cthulhu_ctr_info_copy(cpy, ctr_info_wrapper->ctr_info);
        amxc_var_add_new_cthulhu_ctr_info_t(ret, cpy);
    }
    return 0;
}

static int container_execute(amxc_var_t* args, amxc_var_t* ret) {
    int retval = 2;
    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    char** cmd_arguments = NULL;
    size_t args_size = 0;
    amxc_htable_it_t* it = NULL;
    uint32_t timeout = 10;
    if(string_is_empty(ctr_id)) {
        SAH_TRACE_ERROR("ContainerId is not set");
        goto exit;
    }
    it = amxc_htable_get(containers, ctr_id);
    if(!it) {
        amxc_var_set(cstring_t, ret, "CONTAINER_NOT_FOUND");
        SAH_TRACE_ERROR("Container does not exist");
        goto exit;
    }

    const char* command = GET_CHAR(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_COMMAND);
    amxc_var_t* arguments = GETP_ARG(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_ARGUMENTS);
    if(!GET_ARG(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT)) {
        SAH_TRACE_WARNING("Command timeout not provided. Default value (%d) will be used", timeout);
    } else {
        timeout = GET_UINT32(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT);
    }

    // Extract arguments
    const amxc_llist_t* arglist = amxc_var_constcast(amxc_llist_t, arguments);
    if(arglist) {
        args_size = amxc_llist_size(arglist);
        if(args_size) {
            int i = 0;
            cmd_arguments = (char**) malloc((args_size) * sizeof(char*));
            amxc_var_t* inst = NULL;
            amxc_llist_for_each(it, arglist) {
                inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
                cmd_arguments[i] = amxc_var_dyncast(cstring_t, inst);
                i++;
            }
        }
    }

    retval = 1;
    if(strcmp(command, "/bin/sleep") == 0) {
        unsigned int sleeptime = 0;
        for(int i = 0; i < (int) args_size; i++) {
            sleeptime += atoi(cmd_arguments[i]);
        }
        if(timeout < sleeptime) {
            sleep(timeout);
            SAH_TRACE_WARNING("Command execution timeout");
            amxc_var_set(cstring_t, ret, "CONTAINER_CMD_TIMEOUT");
            retval = 1;
        } else {
            sleep(sleeptime);
            SAH_TRACE_INFO("Command execution succeeded");
            amxc_var_set(cstring_t, ret, "CONTAINER_CMD_OK");
            retval = 0;
        }
    } else if(strcmp(command, "/bin/hang") == 0) {
        SAH_TRACE_INFO("simulate hang (will wait for timeout)");
        sleep(timeout);
        amxc_var_set(cstring_t, ret, "CONTAINER_CMD_TIMEOUT");
    } else {
        SAH_TRACE_WARNING("Unkown command");
        amxc_var_set(cstring_t, ret, "CONTAINER_CMD_KO");
    }

    // clear command arguments
    for(int i = 0; i < (int) args_size; i++) {
        free(cmd_arguments[i]);
    }
    free(cmd_arguments);

exit:
    return retval;
}

int __real_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret);

int __wrap_amxm_so_execute_function(amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    // printf("Module_name %s fname %s\n", module_name, func_name);
    if(strcmp(module_name, "mod_cthulhu") == 0) {
        if(strcmp(func_name, "init") == 0) {
            return container_init(args);
        }
        if(strcmp(func_name, "information") == 0) {
            return container_information(args, ret);
        }
        if(strcmp(func_name, "list") == 0) {
            return container_list(args, ret);
        }
        if(strcmp(func_name, "version") == 0) {
            return 0;
        }
        if(strcmp(func_name, "create") == 0) {
            return container_create(args);
        }
        if(strcmp(func_name, "remove") == 0) {
            return container_remove(args);
        }
        if(strcmp(func_name, "start") == 0) {
            return container_start(args);
        }
        if(strcmp(func_name, "stop") == 0) {
            return container_stop(args);
        }
        if(strcmp(func_name, "execute") == 0) {
            return container_execute(args, ret);
        }
        printf("Module_name %s fname %s\n", module_name, func_name);
        int rv = (int) mock();

        return rv;
    }
    return __real_amxm_so_execute_function(so, module_name, func_name, args, ret);
}

int __real_amxm_so_has_function(amxm_shared_object_t* const so,
                                const char* const module_name,
                                const char* const func_name);

int __wrap_amxm_so_has_function(amxm_shared_object_t* const so,
                                const char* const module_name,
                                const char* const func_name) {
    // printf("Module_name %s fname %s\n", module_name, func_name);
    if(strcmp(module_name, "mod_cthulhu") == 0) {
        if((strcmp(func_name, "init") == 0) ||
           (strcmp(func_name, "information") == 0) ||
           (strcmp(func_name, "list") == 0) ||
           (strcmp(func_name, "version") == 0) ||
           (strcmp(func_name, "create") == 0) ||
           (strcmp(func_name, "remove") == 0) ||
           (strcmp(func_name, "start") == 0) ||
           (strcmp(func_name, "stop") == 0) ||
           (strcmp(func_name, "execute") == 0)) {
            return 1;
        }
        printf("Module_name %s fname %s\n", module_name, func_name);
        int rv = (int) mock();

        return rv;
    }
    return __real_amxm_so_has_function(so, module_name, func_name);
}
