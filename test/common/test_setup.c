/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <debug/sahtrace.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include "cthulhu_priv.h"

#include "wrap_amxm.h"
#include "expected_signal.h"
#include "test_setup.h"

#define ME "test"

static amxm_module_t dummy;
static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_definition = "../../odl/cthulhu_definition.odl";
static const char* odl_config = "../config/cthulhu_config.odl";

amxd_dm_t* test_get_dm(void) {
    return &dm;
}

amxo_parser_t* test_get_parser(void) {
    return &parser;
}

void ignore_alarm(UNUSED int signum);
void ignore_alarm(UNUSED int signum) {
}

int test_cthulhu_init_dm() {
    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    return 0;
}

static int amxrt_parse_odl_extensions(amxo_parser_t* parser, amxd_object_t* root) {
    int retval = 0;

    const char* ext_dir = GET_CHAR(&parser->config, "extensions-dir");
    amxc_string_t opt_include;

    amxc_string_init(&opt_include, 0);
    ASSERT_STR_NOT_EMPTY(ext_dir, goto exit);

    amxc_string_setf(&opt_include, "#include '%s';", ext_dir);

    retval = amxo_parser_parse_string(parser, amxc_string_get(&opt_include, 0), root);

exit:
    amxc_string_clean(&opt_include);
    return retval;
}

int test_cthulhu_setup_wo_dm() {
    amxd_object_t* root_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* cthulhu = NULL;

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_parser_parse_file(&parser, "../config/cthulhu.odl", root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU "." CTHULHU_CMD_LOADBACKEND, AMXO_FUNC(_Cthulhu_loadBackend)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_CREATE, AMXO_FUNC(_Container_create)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_START, AMXO_FUNC(_Container_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_STOP, AMXO_FUNC(_Container_stop)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_REMOVE, AMXO_FUNC(_Container_remove)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_UPDATE, AMXO_FUNC(_Container_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_MODIFY, AMXO_FUNC(_Container_modify)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_LS, AMXO_FUNC(_Container_ls)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_RESTART, AMXO_FUNC(_Container_restart)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_COMMAND, AMXO_FUNC(_Container_command)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_CONTAINER "."CTHULHU_CMD_CTR_EXECUTE, AMXO_FUNC(_Container_execute)), 0);

    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_CREATE, AMXO_FUNC(_Sandbox_create)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_MODIFY, AMXO_FUNC(_Sandbox_modify)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_START, AMXO_FUNC(_Sandbox_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_STOP, AMXO_FUNC(_Sandbox_stop)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_RESTART, AMXO_FUNC(_Sandbox_restart)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_REMOVE, AMXO_FUNC(_Sandbox_remove)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_LS, AMXO_FUNC(_Sandbox_ls)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_SB_EXEC, AMXO_FUNC(_Sandbox_exec)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, CTHULHU_DM_SANDBOX "."CTHULHU_CMD_COMMAND, AMXO_FUNC(_Sandbox_command)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);

    // parse extensions
    amxrt_parse_odl_extensions(&parser, root_obj);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, &dummy);

    _cthulhu_main(0, &dm, &parser);
    _odl_loaded(NULL, NULL, NULL);

    signal(SIGALRM, ignore_alarm);

    expected_sig_start_listening(&dm);
    expected_sig_reset_received();

    wrap_amxm_init();

    return 0;
}


int test_cthulhu_setup() {
    sahTraceOpen("test", TRACE_TYPE_STDOUT);
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "all");
    test_cthulhu_init_dm();
    test_cthulhu_setup_wo_dm();
    return 0;
}

int test_cthulhu_teardown() {
    // process lingering signals and timers
    expected_sig_eat(5);

    expected_sig_reset_received();

    _cthulhu_main(1, &dm, &parser);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    wrap_amxm_cleanup();

    return 0;
}

void testhelper_cthulhu_create_sandbox(const char* sb_name, bool enabled) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    expected_sig_reset_received();
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, sb_name);
    amxc_var_add_key(bool, &args, CTHULHU_SANDBOX_ENABLE, enabled);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_CREATE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:created", true, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    expected_sig_clean(&expected_sig);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", true, false);
    if(enabled) {
        expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    }
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, sb_name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    sleep(1);
}

void testhelper_cthulhu_start_sandbox(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    expected_sig_reset_received();
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_START, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Up", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void testhelper_cthulhu_stop_sandbox(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    expected_sig_reset_received();
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_STOP, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:updated", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_STATUS, "Disabled", false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void testhelper_cthulhu_remove_sandbox(const char* name) {
    amxd_object_t* dm_sandbox = amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_sandbox);

    expected_sig_reset_received();
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_SANDBOX_ID, name);
    assert_int_equal(amxd_object_invoke_function(dm_sandbox, CTHULHU_CMD_SB_REMOVE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:sb:removed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_SANDBOX_ID, name, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);
    sig_read_all();
    sleep(1);
    amxp_timers_calculate();
    amxp_timers_check();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void testhelper_remove_ctr(const char* ctr_id, bool remove_data) {
    amxd_object_t* dm_ctr = amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER);
    amxc_var_t args;
    amxc_var_t ret;
    expected_signal_t expected_sig;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dm_ctr);

    expected_sig_reset_received();
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, CTHULHU_CONTAINER_ID, ctr_id);
    amxc_var_add_key(bool, &args, CTHULHU_CONTAINER_REMOVEDATA, remove_data);
    assert_int_equal(amxd_object_invoke_function(dm_ctr, CTHULHU_CMD_CTR_REMOVE, &args, &ret), amxd_status_ok);
    expected_sig_init(&expected_sig, "cthulhu:ctr:removed", false, false);
    expected_sig_add_match_cstring_t(&expected_sig, "dm_obj."CTHULHU_CONTAINER_ID, ctr_id, false);
    assert_int_equal(expected_sig_wait(&expected_sig, 5), 0);

    sig_read_all();

    expected_sig_clean(&expected_sig);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

amxd_object_t* testhelper_get_ctr(const char* ctr_id) {
    return amxd_dm_findf(test_get_dm(), CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID " == '%s'].", ctr_id);
}

amxd_object_t* testhelper_get_sb(const char* sb_id) {
    return amxd_dm_findf(test_get_dm(), CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID " == '%s'].", sb_id);
}

int test_load_odl(const char* odl_file) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);
    return amxo_parser_parse_file(&parser, odl_file, root_obj);
}

void set_autorestart(amxc_var_t* args, bool enable) {
    amxc_var_t* ar_settings = NULL;
    ar_settings = amxc_var_add_new_key(args, CTHULHU_CONTAINER_AUTORESTART);
    amxc_var_set_type(ar_settings, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_bool(ar_settings, CTHULHU_DM_CTR_AR_ENABLE, enable);
    amxc_var_add_new_key_int32_t(ar_settings, CTHULHU_DM_CTR_AR_MIN_INT, 10);
    amxc_var_add_new_key_int32_t(ar_settings, CTHULHU_DM_CTR_AR_MAX_INT, 60);
    amxc_var_add_new_key_int32_t(ar_settings, CTHULHU_DM_CTR_AR_RETRY_INT, 2000);
}
