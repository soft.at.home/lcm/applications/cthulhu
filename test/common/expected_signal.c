/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "cthulhu_priv.h"
#include "expected_signal.h"
#include "test_setup.h"
#include <time.h>
#include <amxd/amxd_dm.h>

#define ME "test"

static expected_signal_t* expected_sig = NULL;
static bool expected_sig_found = false;
static bool expected_sig_exclusive_error = false;

static amxc_llist_t received_signals;

typedef struct _match_data {
    amxc_htable_it_t hit; /**< Hash table iterator, can be used to store the variant in a hash table */
    amxc_var_t var;
    bool exclusive;       /* if exclusive is set, then this match is only allowed if the signal name and the other matches also match */
} match_data_t;

typedef struct _saved_sig {
    char* sig_name;
    amxc_var_t data;
    amxc_llist_it_t it;
} saved_sig_t;

static int saved_sig_new (saved_sig_t** sig, const char* sig_name, const amxc_var_t* data) {
    int retval = -1;
    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_EQUAL(*sig, NULL, goto exit);
    ASSERT_NOT_NULL(sig_name, goto exit);
    *sig = (saved_sig_t*) calloc(1, sizeof(saved_sig_t));
    ASSERT_NOT_NULL(*sig, goto exit);
    (*sig)->sig_name = strdup(sig_name);
    amxc_var_copy(&((*sig)->data), data);

    retval = 0;
exit:
    return retval;
}

static void saved_sig_delete(saved_sig_t** sig) {
    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_NOT_NULL(*sig, goto exit);
    amxc_llist_it_take(&(*sig)->it);
    free((*sig)->sig_name);
    amxc_var_clean(&((*sig)->data));
    free(*sig);
    *sig = NULL;
exit:
    return;
}

static int match_data_new(match_data_t** match_data) {
    int retval = -1;
    ASSERT_NOT_NULL(match_data, goto exit);

    *match_data = (match_data_t*) calloc(1, sizeof(match_data_t));
    ASSERT_NOT_NULL(*match_data, goto exit);
    ASSERT_SUCCESS(amxc_htable_it_init(&(*match_data)->hit), goto exit)

    retval = amxc_var_init(&(*match_data)->var);

exit:
    return retval;
}

static void match_data_delete(match_data_t** match_data) {
    ASSERT_NOT_NULL(match_data, goto exit);

    if(*match_data != NULL) {
        amxc_var_clean(&(*match_data)->var);
        amxc_htable_it_clean(&((*match_data)->hit), NULL);
    }

    free(*match_data);
    *match_data = NULL;

exit:
    return;
}

static void match_data_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    match_data_t* match_data = amxc_htable_it_get_data(it, match_data_t, hit);
    match_data_delete(&match_data);
}

static bool var_contains_data(const amxc_var_t* const data, const char* const path, const amxc_var_t* const match) {
    bool res = false;
    int comp_res = 0;
    const amxc_var_t* var = amxc_var_get_path(data, path, AMXC_VAR_FLAG_DEFAULT);
    ASSERT_NOT_NULL(var, goto exit);
    ASSERT_SUCCESS(amxc_var_compare(var, match, &comp_res), goto exit);
    res = !comp_res;
exit:
    return res;

}

static void find_matches(const amxc_var_t* const data,
                         const amxc_htable_t* const matches,
                         int* totalfound,
                         int* totalnotfound,
                         int* exclusivesfound,
                         bool print) {
    amxc_htable_for_each(it, matches) {
        match_data_t* match_data = amxc_htable_it_get_data(it, match_data_t, hit);
        const char* path = amxc_htable_it_get_key(it);
        if(var_contains_data(data, path, &match_data->var)) {
            *totalfound += 1;
            if(match_data->exclusive) {
                *exclusivesfound += 1;
            }
            if(print) {
                printf("%smatch found [path %s]:\n", match_data->exclusive ? "exclusive " : "", path);
                amxc_var_dump(&match_data->var, 1);
            }
        } else {
            *totalnotfound += 1;
            if(print) {
                printf("%smatch not found [path %s]:\n", match_data->exclusive ? "exclusive " : "", path);
                amxc_var_dump(&match_data->var, 1);
            }

        }
    }

}

/** check if a saved sig matches the expected sig */
static void check_saved_sig(saved_sig_t* saved_sig) {
    int totalfound = 0;
    int totalnotfound = 0;
    int exclusivesfound = 0;
    ASSERT_NOT_NULL(expected_sig, goto exit);

    // early abort if it is the wrong signal. uncomment to print all signals
    if(!expected_sig->print_all && expected_sig->signame && (strcmp(saved_sig->sig_name, expected_sig->signame) != 0)) {
        goto exit;
    }
    if(expected_sig->print_matching_signal) {
        printf("found %d notfound %d exclusives %d\n", totalfound, totalnotfound, exclusivesfound);
        amxc_var_dump(&saved_sig->data, 1);
    }
    find_matches(&saved_sig->data, &expected_sig->matches, &totalfound, &totalnotfound, &exclusivesfound, expected_sig->print_matching_signal);
    if(expected_sig->print_matching_signal) {
        printf("Matches found: %d notfound: %d exclusives: %d\n", totalfound, totalnotfound, exclusivesfound);
    }

    if(expected_sig->signame && (strcmp(saved_sig->sig_name, expected_sig->signame) != 0)) {
        if(exclusivesfound) {
            expected_sig_exclusive_error = true;
            if(expected_sig->print_matching_signal) {
                printf("Exclusive error in Sig: %s\n", saved_sig->sig_name);
                amxc_var_dump(&saved_sig->data, 1);
            }
        }
        goto exit;
    }

    if(totalnotfound == 0) {
        expected_sig_found = true;
    } else if(exclusivesfound) {
        printf("Exclusive error in Sig: %s\n", saved_sig->sig_name);
        expected_sig_exclusive_error = true;
    }
exit:
    return;
}

static void expected_sig_received(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    saved_sig_t* sig = NULL;
    const char* path = GET_CHAR(data, "path");
    printf("Received sig: %s\t%s\n", sig_name, path);
    saved_sig_new(&sig, sig_name, data);
    ASSERT_NOT_NULL(sig, goto exit);
    amxc_llist_append(&received_signals, &sig->it);
    check_saved_sig(sig);

exit:
    return;
}

static void check_cached_sigs(void) {
    saved_sig_t* sig = NULL;
    amxc_llist_for_each(it, &received_signals) {
        saved_sig_t* sig = amxc_llist_it_get_data(it, saved_sig_t, it);
        check_saved_sig(sig);
    }
}

void sig_read_all(void) {
    while(amxp_signal_read() == 0) {
    }
}

int expected_sig_init(expected_signal_t* sig, const char* const signame, bool print_matching_signal, bool print_all) {
    int res = -1;
    ASSERT_NOT_NULL(sig, goto exit);

    sig->print_matching_signal = print_matching_signal;
    sig->print_all = print_all;
    if(signame) {
        sig->signame = strdup(signame);
    } else {
        sig->signame = NULL;
    }
    res = amxc_htable_init(&(sig->matches), 5);
exit:
    return res;

}
void expected_sig_clean(expected_signal_t* sig) {
    ASSERT_NOT_NULL(sig, goto exit);
    free(sig->signame);
    amxc_htable_clean(&(sig->matches), match_data_it_free);
exit:
    return;
}

int expected_sig_add_match_bool(expected_signal_t* sig, const char* const key, bool val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_SUCCESS(match_data_new(&match_data), goto exit);
    ASSERT_SUCCESS(amxc_var_set_bool(&match_data->var, val), goto error);
    ASSERT_SUCCESS(amxc_htable_insert(&sig->matches, key, &match_data->hit), goto error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_add_match_int32_t(expected_signal_t* sig, const char* const key, int32_t val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_SUCCESS(match_data_new(&match_data), goto exit);
    ASSERT_SUCCESS(amxc_var_set_int32_t(&match_data->var, val), goto error);
    ASSERT_SUCCESS(amxc_htable_insert(&sig->matches, key, &match_data->hit), goto error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_add_match_uint32_t(expected_signal_t* sig, const char* const key, uint32_t val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_SUCCESS(match_data_new(&match_data), goto exit);
    ASSERT_SUCCESS(amxc_var_set_uint32_t(&match_data->var, val), goto error);
    ASSERT_SUCCESS(amxc_htable_insert(&sig->matches, key, &match_data->hit), goto error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

int expected_sig_add_match_cstring_t(expected_signal_t* sig, const char* const key, const char* const val, bool exclusive) {
    int res = -1;
    match_data_t* match_data = NULL;

    ASSERT_NOT_NULL(sig, goto exit);
    ASSERT_NOT_NULL(key, goto exit);
    ASSERT_NOT_NULL(val, goto exit);
    ASSERT_SUCCESS(match_data_new(&match_data), goto exit);
    ASSERT_SUCCESS(amxc_var_set_cstring_t(&match_data->var, val), goto error);
    ASSERT_SUCCESS(amxc_htable_insert(&sig->matches, key, &match_data->hit), goto error);
    match_data->exclusive = exclusive;

    res = 0;
    goto exit;
error:
    match_data_delete(&match_data);
exit:
    return res;
}

static void trigger_lcm_worker(void) {
    lcm_worker_t* worker = cthulhu_get_worker();
    process_callback_handler(worker->main_fd, worker);
}

static uint64_t get_timestamp_ms(void) {
    struct timespec tms;
    uint64_t ms = 0;
    if(clock_gettime(CLOCK_REALTIME, &tms)) {
        SAH_TRACEZ_ERROR(ME, "Failed to get timestamp");
        return 0;
    }

    ms = tms.tv_sec * 1000;
    ms += tms.tv_nsec / 1000000;
    return ms;
}

#define DEBUG_SIG

int expected_sig_wait(expected_signal_t* const sig, int timeout) {
    int res = -1;
    printf("Wait for sig: %s\n", sig->signame);
    expected_sig = sig;
    expected_sig_found = false;
    expected_sig_exclusive_error = false;

    check_cached_sigs();
    if(expected_sig_found && !expected_sig_exclusive_error) {
        printf("Found sig in cache\n");
        res = 0;
        goto exit;
    }

    trigger_lcm_worker();


    int ms_per_loop = 100;
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = ms_per_loop * 1000000;

    uint64_t end_time = get_timestamp_ms() + (timeout * 1000);
#ifdef DEBUG_SIG
    setbuf(stdout, NULL);
#endif
    int cnt = 0;
    while(get_timestamp_ms() < end_time) {
        while(!expected_sig_found &&
              !expected_sig_exclusive_error && (
                  amxp_signal_read() == 0) &&
              (get_timestamp_ms() < end_time)) {
#ifdef DEBUG_SIG
            printf("*");
#endif
            trigger_lcm_worker();
        }
        if(expected_sig_exclusive_error) {
            goto exit;
        }
        if(expected_sig_found) {
            res = 0;
            goto exit;
        }
#ifdef DEBUG_SIG
        printf(".");
#endif
        cnt++;
        if(cnt == 5) {
#ifdef DEBUG_SIG
            printf("*");
#endif
            trigger_lcm_worker();
            cnt = 0;
        }
        nanosleep(&ts, NULL);
        trigger_lcm_worker();
        amxp_timers_calculate();
        amxp_timers_check();
    }
    if(expected_sig_found && !expected_sig_exclusive_error) {
        res = 0;
        goto exit;
    }
    printf("!!! Not found\n");
exit:
#ifdef DEBUG_SIG
    printf("\n");
#endif
    expected_sig = NULL;
    return res;
}

int expected_sig_start_listening(amxd_dm_t* dm) {
    int res = -1;
    amxc_llist_init(&received_signals);
    if(amxp_slot_connect(&dm->sigmngr, "*", NULL, expected_sig_received, NULL) != 0) {
        printf("Could not connect slot\n");
        goto exit;
    }
    res = 0;
exit:
    return res;
}

static void saved_sig_it_free(amxc_llist_it_t* it) {
    saved_sig_t* sig = amxc_llist_it_get_data(it, saved_sig_t, it);
    saved_sig_delete(&sig);
}

void expected_sig_reset_received(void) {
    sig_read_all();
    amxc_llist_clean(&received_signals, saved_sig_it_free);
    printf("Reset received signals\n");
}

void expected_sig_stop_listening(amxd_dm_t* dm) {
    int res = -1;
    amxp_slot_disconnect(&dm->sigmngr, "*", expected_sig_received);
    amxc_llist_clean(&received_signals, saved_sig_it_free);
}

void expected_sig_eat(int wait_time) {
    expected_signal_t expected_sig;
    expected_sig_init(&expected_sig, "eat_signals", false, false);
    expected_sig_wait(&expected_sig, wait_time);
    expected_sig_clean(&expected_sig);
}
